// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: yellow; icon-glyph: magic;
const md5 = string => {
  const safeAdd = (x, y) => {
    let lsw = (x & 0xFFFF) + (y & 0xFFFF);
    return (((x >> 16) + (y >> 16) + (lsw >> 16)) << 16) | (lsw & 0xFFFF)
  };
  const bitRotateLeft = (num, cnt) => (num << cnt) | (num >>> (32 - cnt));
  const md5cmn = (q, a, b, x, s, t) => safeAdd(bitRotateLeft(safeAdd(safeAdd(a, q), safeAdd(x, t)), s), b),
    md5ff = (a, b, c, d, x, s, t) => md5cmn((b & c) | ((~b) & d), a, b, x, s, t),
    md5gg = (a, b, c, d, x, s, t) => md5cmn((b & d) | (c & (~d)), a, b, x, s, t),
    md5hh = (a, b, c, d, x, s, t) => md5cmn(b ^ c ^ d, a, b, x, s, t),
    md5ii = (a, b, c, d, x, s, t) => md5cmn(c ^ (b | (~d)), a, b, x, s, t);
  const firstChunk = (chunks, x, i) => {
      let [a, b, c, d] = chunks;
      a = md5ff(a, b, c, d, x[i + 0], 7, -680876936);
      d = md5ff(d, a, b, c, x[i + 1], 12, -389564586);
      c = md5ff(c, d, a, b, x[i + 2], 17, 606105819);
      b = md5ff(b, c, d, a, x[i + 3], 22, -1044525330);

      a = md5ff(a, b, c, d, x[i + 4], 7, -176418897);
      d = md5ff(d, a, b, c, x[i + 5], 12, 1200080426);
      c = md5ff(c, d, a, b, x[i + 6], 17, -1473231341);
      b = md5ff(b, c, d, a, x[i + 7], 22, -45705983);

      a = md5ff(a, b, c, d, x[i + 8], 7, 1770035416);
      d = md5ff(d, a, b, c, x[i + 9], 12, -1958414417);
      c = md5ff(c, d, a, b, x[i + 10], 17, -42063);
      b = md5ff(b, c, d, a, x[i + 11], 22, -1990404162);

      a = md5ff(a, b, c, d, x[i + 12], 7, 1804603682);
      d = md5ff(d, a, b, c, x[i + 13], 12, -40341101);
      c = md5ff(c, d, a, b, x[i + 14], 17, -1502002290);
      b = md5ff(b, c, d, a, x[i + 15], 22, 1236535329);

      return [a, b, c, d]
    },
    secondChunk = (chunks, x, i) => {
      let [a, b, c, d] = chunks;
      a = md5gg(a, b, c, d, x[i + 1], 5, -165796510);
      d = md5gg(d, a, b, c, x[i + 6], 9, -1069501632);
      c = md5gg(c, d, a, b, x[i + 11], 14, 643717713);
      b = md5gg(b, c, d, a, x[i], 20, -373897302);

      a = md5gg(a, b, c, d, x[i + 5], 5, -701558691);
      d = md5gg(d, a, b, c, x[i + 10], 9, 38016083);
      c = md5gg(c, d, a, b, x[i + 15], 14, -660478335);
      b = md5gg(b, c, d, a, x[i + 4], 20, -405537848);

      a = md5gg(a, b, c, d, x[i + 9], 5, 568446438);
      d = md5gg(d, a, b, c, x[i + 14], 9, -1019803690);
      c = md5gg(c, d, a, b, x[i + 3], 14, -187363961);
      b = md5gg(b, c, d, a, x[i + 8], 20, 1163531501);

      a = md5gg(a, b, c, d, x[i + 13], 5, -1444681467);
      d = md5gg(d, a, b, c, x[i + 2], 9, -51403784);
      c = md5gg(c, d, a, b, x[i + 7], 14, 1735328473);
      b = md5gg(b, c, d, a, x[i + 12], 20, -1926607734);

      return [a, b, c, d]
    },
    thirdChunk = (chunks, x, i) => {
      let [a, b, c, d] = chunks;
      a = md5hh(a, b, c, d, x[i + 5], 4, -378558);
      d = md5hh(d, a, b, c, x[i + 8], 11, -2022574463);
      c = md5hh(c, d, a, b, x[i + 11], 16, 1839030562);
      b = md5hh(b, c, d, a, x[i + 14], 23, -35309556);

      a = md5hh(a, b, c, d, x[i + 1], 4, -1530992060);
      d = md5hh(d, a, b, c, x[i + 4], 11, 1272893353);
      c = md5hh(c, d, a, b, x[i + 7], 16, -155497632);
      b = md5hh(b, c, d, a, x[i + 10], 23, -1094730640);

      a = md5hh(a, b, c, d, x[i + 13], 4, 681279174);
      d = md5hh(d, a, b, c, x[i], 11, -358537222);
      c = md5hh(c, d, a, b, x[i + 3], 16, -722521979);
      b = md5hh(b, c, d, a, x[i + 6], 23, 76029189);

      a = md5hh(a, b, c, d, x[i + 9], 4, -640364487);
      d = md5hh(d, a, b, c, x[i + 12], 11, -421815835);
      c = md5hh(c, d, a, b, x[i + 15], 16, 530742520);
      b = md5hh(b, c, d, a, x[i + 2], 23, -995338651);

      return [a, b, c, d]
    },
    fourthChunk = (chunks, x, i) => {
      let [a, b, c, d] = chunks;
      a = md5ii(a, b, c, d, x[i], 6, -198630844);
      d = md5ii(d, a, b, c, x[i + 7], 10, 1126891415);
      c = md5ii(c, d, a, b, x[i + 14], 15, -1416354905);
      b = md5ii(b, c, d, a, x[i + 5], 21, -57434055);

      a = md5ii(a, b, c, d, x[i + 12], 6, 1700485571);
      d = md5ii(d, a, b, c, x[i + 3], 10, -1894986606);
      c = md5ii(c, d, a, b, x[i + 10], 15, -1051523);
      b = md5ii(b, c, d, a, x[i + 1], 21, -2054922799);

      a = md5ii(a, b, c, d, x[i + 8], 6, 1873313359);
      d = md5ii(d, a, b, c, x[i + 15], 10, -30611744);
      c = md5ii(c, d, a, b, x[i + 6], 15, -1560198380);
      b = md5ii(b, c, d, a, x[i + 13], 21, 1309151649);

      a = md5ii(a, b, c, d, x[i + 4], 6, -145523070);
      d = md5ii(d, a, b, c, x[i + 11], 10, -1120210379);
      c = md5ii(c, d, a, b, x[i + 2], 15, 718787259);
      b = md5ii(b, c, d, a, x[i + 9], 21, -343485551);
      return [a, b, c, d]
    };
  const binlMD5 = (x, len) => {
    /* append padding */
    x[len >> 5] |= 0x80 << (len % 32);
    x[(((len + 64) >>> 9) << 4) + 14] = len;
    let commands = [firstChunk, secondChunk, thirdChunk, fourthChunk],
      initialChunks = [
        1732584193,
        -271733879,
        -1732584194,
        271733878
      ];
    return Array.from({length: Math.floor(x.length / 16) + 1}, (v, i) => i * 16)
      .reduce((chunks, i) => commands
        .reduce((newChunks, apply) => apply(newChunks, x, i), chunks.slice())
        .map((chunk, index) => safeAdd(chunk, chunks[index])), initialChunks)

  };
  const binl2rstr = input => Array(input.length * 4).fill(8).reduce((output, k, i) => output + String.fromCharCode((input[(i * k) >> 5] >>> ((i * k) % 32)) & 0xFF), '');
  const rstr2binl = input => Array.from(input).map(i => i.charCodeAt(0)).reduce((output, cc, i) => {
    let resp = output.slice();
    resp[(i * 8) >> 5] |= (cc & 0xFF) << ((i * 8) % 32);
    return resp
  }, []);
  const rstrMD5 = string => binl2rstr(binlMD5(rstr2binl(string), string.length * 8));
  const rstr2hex = input => {
    const hexTab = (pos) => '0123456789abcdef'.charAt(pos);
    return Array.from(input).map(c => c.charCodeAt(0)).reduce((output, x) => output + hexTab((x >>> 4) & 0x0F) + hexTab(x & 0x0F), '')
  };
  const str2rstrUTF8 = unicodeString => {
    if (typeof unicodeString !== 'string') throw new TypeError('parameter ‘unicodeString’ is not a string')
    const cc = c => c.charCodeAt(0);
    return unicodeString
      .replace(/[\u0080-\u07ff]/g,  // U+0080 - U+07FF => 2 bytes 110yyyyy, 10zzzzzz
        c => String.fromCharCode(0xc0 | cc(c) >> 6, 0x80 | cc(c) & 0x3f))
      .replace(/[\u0800-\uffff]/g,  // U+0800 - U+FFFF => 3 bytes 1110xxxx, 10yyyyyy, 10zzzzzz
        c => String.fromCharCode(0xe0 | cc(c) >> 12, 0x80 | cc(c) >> 6 & 0x3F, 0x80 | cc(c) & 0x3f))
  };
  const rawMD5 = s => rstrMD5(str2rstrUTF8(s));
  const hexMD5 = s => rstr2hex(rawMD5(s));
  return hexMD5(string)
};

class Core {
  constructor(arg = '') {
    this.arg = arg;
    this._actions = {};
    this.init();
  }

  /**
   * 初始化配置
   * @param {string} widgetFamily
   */
  init(widgetFamily = config.widgetFamily) {
    // 组件大小：small,medium,large
    this.widgetFamily = widgetFamily;
    // 系统设置的key，这里分为三个类型：
    // 1. 全局
    // 2. 不同尺寸的小组件
    // 3. 不同尺寸+小组件自定义的参数
    // 当没有key2时，获取key1，没有key1获取全局key的设置
    // this.SETTING_KEY = md5(Script.name()+'@'+this.widgetFamily+'@'+this.arg)
    // this.SETTING_KEY1 = md5(Script.name()+'@'+this.widgetFamily)
    this.SETTING_KEY = md5(Script.name());
    // 插件设置
    this.settings = this.getSettings();
  }

  /**
   * 注册点击操作菜单
   * @param {string} name 操作函数名
   * @param {Function} func 点击后执行的函数
   */
  registerAction(name, func) {
    this._actions[name] = func.bind(this);
  }

  /**
   * 设置字体
   * @param {WidgetText} widget
   * @param size
   * @param { 'regular' || 'bold' } type
   */
  setFontFamilyStyle(widget, size, type = 'regular') {
    const regularFont = this.settings['regularFont'] || 'PingFangSC-Regular ';
    const boldFont = this.settings['boldFont'] || 'PingFangSC-Semibold';

    widget.font = new Font(type === 'regular' ? regularFont : boldFont, size);
  }

  /**
   * 获取当前插件的设置
   * @param {boolean} json 是否为json格式
   * @param key
   */
  getSettings(json = true, key = this.SETTING_KEY) {
    let result = json ? {} : '';
    let cache = '';
    if (Keychain.contains(key)) {
      cache = Keychain.get(key);
    }
    if (json) {
      try {
        result = JSON.parse(cache);
      } catch (error) {
        // throw new Error('JSON 数据解析失败' + error)
      }
    } else {
      result = cache;
    }

    return result
  }

  /**
   * 新增 Stack 布局
   * @param {WidgetStack | ListWidget} stack 节点信息
   * @param {'horizontal' | 'vertical'} layout 布局类型
   * @returns {WidgetStack}
   */
  addStackTo(stack, layout) {
    const newStack = stack.addStack();
    layout === 'horizontal' ? newStack.layoutHorizontally() : newStack.layoutVertically();
    return newStack
  }

  /**
   * 时间格式化
   * @param date
   * @param format
   * @return {string}
   */
  formatDate(date = new Date(), format = 'MM-dd HH:mm') {
    const formatter = new DateFormatter();
    formatter.dateFormat = format;
    const updateDate = new Date(date);
    return formatter.string(updateDate)
  }

  /**
   * 生成操作回调URL，点击后执行本脚本，并触发相应操作
   * @param {string} name 操作的名称
   * @param {string} data 传递的数据
   */
  actionUrl(name = '', data = '') {
    let u = URLScheme.forRunningScript();
    let q = `act=${encodeURIComponent(name)}&data=${encodeURIComponent(data)}&__arg=${encodeURIComponent(this.arg)}&__size=${this.widgetFamily}`;
    let result = '';
    if (u.includes('run?')) {
      result = `${u}&${q}`;
    } else {
      result = `${u}?${q}`;
    }
    return result
  }

  /**
   * HTTP 请求接口
   * @param {Object} options request 选项配置
   * @returns {Promise<JSON | String>}
   */
  async http(options) {
    const url = options?.url || url;
    const method = options?.method || 'GET';
    const headers = options?.headers || {};
    const body = options?.body || '';
    const json = options?.json || true;

    let response = new Request(url);
    response.method = method;
    response.headers = headers;
    if (method === 'POST' || method === 'post') response.body = body;
    return (json ? response.loadJSON() : response.loadString())
  }

  /**
   * 弹出一个通知
   * @param {string} title 通知标题
   * @param {string} body 通知内容
   * @param {string} url 点击后打开的URL
   * @param {Object} opts
   * @returns {Promise<void>}
   */
  async notify(title, body = '', url = undefined, sound = 'piano_success',opts = {}) {
    try {
      let n = new Notification();
      n = Object.assign(n, opts);
      n.title = title;
      n.body = body;
      n.sound = sound;
      if (url) n.openURL = url;
      return await n.schedule()
    } catch (error) {
      console.warn(error);
    }
  }

  /**
   * 存储当前设置
   * @param {boolean} notify 是否通知提示
   */
  async saveSettings(notify = true) {
    const result = (typeof this.settings === 'object') ? JSON.stringify(this.settings) : String(this.settings);
    Keychain.set(this.SETTING_KEY, result);
    if (notify) await this.notify('设置成功', '桌面组件稍后将自动刷新');
  }

  /**
   * 获取用户车辆照片
   * @returns {Promise<Image|*>}
   */
  async getMyCarPhoto(photo) {
    if (!this.settings['myCarPhotoUrl']){
      console.log('message')
    let myCarPhoto = await this.getImageByUrl('https://gitlab.com/j8468/ggfv/-/raw/main/img/st.png');
    
//     if (this.settings['myCarPhoto']) myCarPhoto = await FileManager.local().readImage(this.settings['myCarPhoto']);
    return myCarPhoto
    }else{
    let myCarPhoto = await this.getImageByUrl(photo);
    return myCarPhoto
    }
  }

  /**
   * 获取LOGO照片
   * @returns {Promise<Image|*>}
   */
  async getMyCarLogo(logo) {
    let myCarLogo = await this.getImageByUrl(logo);
    if (this.settings['myCarLogo']) myCarLogo = await FileManager.local().readImage(this.settings['myCarLogo']);
    return myCarLogo
  }
  async talk() {

    const webView = new WebView();
    await webView.loadHTML (`<div id="" style="position: fixed;bottom: 0px;left: 50%;transform: translateX(-50%);"><center><h1>长按下发图片选择“存储到照片”，然后打开微信扫一扫添加。</h1></center><hr><img id="sourceImg"src="https://gitlab.com/j8468/ggfv/-/raw/main/img/me.JPG" /></div>`);
    await webView.present()
  }

  /**
   * 预览组件
   * @param {Widget} Widget
   * @return {Promise<void>}
   */
  async actionPreview(Widget) {
    const alert = new Alert();
    alert.title = '预览组件';
    alert.message = '用于调试和测试组件样式';

    const menuList = [{
      name: 'Small',
      text: '小尺寸'
    }, {
      name: 'Medium',
      text: '中尺寸'
    }, {
      name: 'Large',
      text: '大尺寸'
    }];

    menuList.forEach(item => {
      alert.addAction(item.text);
    });

    alert.addCancelAction('退出菜单');
    const id = await alert.presentSheet();
    if (id === -1) return
    // 执行函数
    const widget = new Widget(args.widgetParameter || '');
    widget.widgetFamily = (menuList[id].name).toLowerCase();
    const w = await widget.render();
    await w['present' + menuList[id].name]();
  }
}

/**
 * 根据百分比输出 hex 颜色
 * @param {number} pct
 * @returns {Color}
 */

/**
 * 一维数组转换多维数组
 * @param arr
 * @param num
 * @returns {*[]}
 */
const format2Array = (arr, num) => {
  const  pages = [];
  arr.forEach((item, index) => {
    const page = Math.floor(index / num);
    if (!pages[page]) {
      pages[page] = [];
    }
    pages[page].push(item);
  });
  return pages
};

class UIRender extends Core {
  constructor(args = '') {
    super(args);

    // 默认背景色
    this.lightDefaultBackgroundColorGradient = ['#ffffff', '#dbefff'];
    this.darkDefaultBackgroundColorGradient = ['#414345', '#232526'];

    this.myCarPhotoUrl = '';
    this.myCarLogoUrl = '';
    this.logoWidth = 0;
    this.logoHeight = 0;

    this.defaultMyOne = '';
    this.locationBorderRadius = 15;
    this.locationMapZoom = 12;
  }

  /**
   * 成功色调
   * @param alpha
   * @returns {Color}
   */
  successColor = (alpha = 1) => new Color('#67C23A', alpha)
  carpz = (alpha = 1) => new Color('#4296FB', alpha)
  carbk = (alpha = 1) => new Color('#FFFFFF', alpha)
  
  
  
  /**
   * 警告色调
   * @param alpha
   * @returns {Color}
   */
  warningColor = (alpha = 1) => new Color('#E6A23C', alpha)

  /**
   * 危险色调
   * @param alpha
   * @returns {Color}
   */
  dangerColor = (alpha = 1) => new Color('#F56C6C', alpha)

  /**
   * 将图像裁剪到指定的 rect 中
   * @param img
   * @param rect
   * @returns {Image}
   */
  cropImage(img, rect) {
    const draw = new DrawContext();
    draw.size = new Size(rect.width, rect.height);

    draw.drawImageAtPoint(img, new Point(-rect.x, -rect.y));
    return draw.getImage()
  }

  /**
   * 手机分辨率
   * @returns Object
   */
  phoneSizes() {
    return {
      '2778': {
        small: 510,
        medium: 1092,
        large: 1146,
        left: 96,
        right: 678,
        top: 246,
        middle: 882,
        bottom: 1518
      },

      // 12 and 12 Pro
      '2532': {
        small: 474,
        medium: 1014,
        large: 1062,
        left: 78,
        right: 618,
        top: 231,
        middle: 819,
        bottom: 1407
      },

      // 11 Pro Max, XS Max
      '2688': {
        small: 507,
        medium: 1080,
        large: 1137,
        left: 81,
        right: 654,
        top: 228,
        middle: 858,
        bottom: 1488
      },

      // 11, XR
      '1792': {
        small: 338,
        medium: 720,
        large: 758,
        left: 54,
        right: 436,
        top: 160,
        middle: 580,
        bottom: 1000
      },

      // 11 Pro, XS, X
      '2436': {
        small: 465,
        medium: 987,
        large: 1035,
        left: 69,
        right: 591,
        top: 213,
        middle: 783,
        bottom: 1353
      },

      // Plus phones
      '2208': {
        small: 471,
        medium: 1044,
        large: 1071,
        left: 99,
        right: 672,
        top: 114,
        middle: 696,
        bottom: 1278
      },

      // SE2 and 6/6S/7/8
      '1334': {
        small: 296,
        medium: 642,
        large: 648,
        left: 54,
        right: 400,
        top: 60,
        middle: 412,
        bottom: 764
      },

      // SE1
      '1136': {
        small: 282,
        medium: 584,
        large: 622,
        left: 30,
        right: 332,
        top: 59,
        middle: 399,
        bottom: 399
      },

      // 11 and XR in Display Zoom mode
      '1624': {
        small: 310,
        medium: 658,
        large: 690,
        left: 46,
        right: 394,
        top: 142,
        middle: 522,
        bottom: 902
      },
            // 15 pro 
      '2556': {
        small: 474,
        medium: 1014,
        large: 1062,
        left: 82,
        right: 622,
        top: 270,
        middle: 858,
        bottom: 1446
      },
         // 14 Pro Max
      "2796": { 
        small: 510,
        medium: 1092,
        large: 1146, 
        left: 99, 
        right: 681, 
        top: 282, 
        middle: 918, 
        bottom: 1554 
      },

      // Plus in Display Zoom mode
      '2001': {
        small: 444,
        medium: 963,
        large: 972,
        left: 81,
        right: 600,
        top: 90,
        middle: 618,
        bottom: 1146
      }
    }
  }

  /**
   * 获取车辆地址位置静态图片
   * @param {Object} location 位置
   * @param {boolean} debug 开启日志输出
   * @return {string}
   */
  getCarAddressImage(location, debug = false) {
    const longitude = location?.longitude || this.settings['longitude'] || this.settings['phoneLongitude'];
    const latitude = location?.latitude || this.settings['latitude'] || this.settings['phoneLatitude'];

    const aMapKey = this.settings['aMapKey']?.trim() || 'c078fb16379c25bc0aad8633d82cf1dd';
    const size = this.settings['largeMapType'] ? '500*280' : '100*60';
    const aMapUrl = `https://restapi.amap.com/v3/staticmap?key=${aMapKey}&markers=mid,0xFF0000,0:${longitude},${latitude}&size=${size}&scale=1&zoom=${this.getLocationMapZoom()}&traffic=1`;
    if (debug) {
      console.log('位置图片请求地址：');
      console.log(aMapUrl);
    }
    return aMapUrl
  }

  /**
   * 正常锁车风格
   * @returns {boolean}
   */
  getLockSuccessStyle() {
    return this.settings['lockSuccessStyle'] === 'successColor'
  }

  /**
   * logo 填充
   * @returns {boolean}
   */
  getLogoHasTint() {
    return this.settings['logoTintType'] ? this.settings['logoTintType'] === 'fontColor' : true
  }

  /**
   * 大组件弧度
   * @returns {number}
   */
  getLocationBorderRadius() {
    return parseInt(this.settings['locationBorderRadius'], 10) || this.locationBorderRadius
  }

  /**
   * 地图缩放比例
   * @returns {number|number}
   */
  getLocationMapZoom() {
    return parseInt(this.settings['locationMapZoom'], 10) || this.locationMapZoom
  }

  /**
   * 获取 logo 大小
   * @param {'width' || 'height'} type
   */
  getLogoSize(type) {
    if (type === 'width') return parseInt(this.settings['logoWidth'], 10) || this.logoWidth
    if (type === 'height') return parseInt(this.settings['logoHeight'], 10) || this.logoHeight
  }

  /**
   * 动态设置组件字体或者图片颜色
   * @param {WidgetText || WidgetImage || WidgetStack} widget
   * @param {'textColor' || 'tintColor' || 'borderColor' || 'backgroundColor'} type
   * @param {number} alpha
   */
  setWidgetNodeColor(widget, type = 'textColor', alpha = 1) {
    const widgetFamily = this.widgetFamily === 'small' ? 'Small' : this.widgetFamily === 'medium' ? 'Medium' : 'Large';
    if (this.settings['backgroundPhoto' + widgetFamily]) {
      const textColor = this.settings['backgroundImageTextColor'] || '#ffffff';
      widget[type] = new Color(textColor, alpha);
    } else {
      const lightTextColor = this.settings['lightTextColor'] || '#000000';
      const darkTextColor = this.settings['darkTextColor'] || '#ffffff';
      widget[type] = Color.dynamic(new Color(lightTextColor, alpha), new Color(darkTextColor, alpha));
//       canvas.setTextColor(Color.dynamic(new Color(lightTextColor, 1), new Color(darkTextColor, 1)));
    }
  }


xgys(gh) {
 
      const lightTextColor = this.settings['lightTextColor'] || '#000000';
      const darkTextColor = this.settings['darkTextColor'] || '#ffffff';
      gh.setTextColor(Color.dynamic(new Color(lightTextColor, 1), new Color(darkTextColor, 1)));
  
  }

  /**
   * 给图片加一层半透明遮罩
   * @param {Image} img 要处理的图片
   * @param {string} color 遮罩背景颜色
   * @param {number} opacity 透明度
   * @returns {Promise<Image>}
   */
  async shadowImage(img, color = '#000000', opacity = 0.7) {
    let ctx = new DrawContext();
    // 获取图片的尺寸
    ctx.size = img.size;

    ctx.drawImageInRect(img, new Rect(0, 0, img.size['width'], img.size['height']));
    ctx.setFillColor(new Color(color, opacity));
    ctx.fillRect(new Rect(0, 0, img.size['width'], img.size['height']));

    return ctx.getImage()
  }

  /**
   * Alert 弹窗封装
   * @param title
   * @param message
   * @param options
   * @returns {Promise<number>}
   */
  async generateAlert(title = '提示', message, options) {
    const alert = new Alert();
    alert.title = title;
    alert.message = message;
    for (const option of options) {
      alert.addAction(option);
    }
    return await alert.presentAlert()
  }

  /**
   * 组件声明
   * @returns {Promise<number>}
   */
  async actionStatementSettings(message) {
    const alert = new Alert();
    alert.title = ' 组件声明';
    alert.message = message;
    alert.addAction('同意');
    alert.addCancelAction('不同意');
    return await alert.presentAlert()
  }

  /**
   * SFSymbol 图标
   * @param sfSymbolName
   * @returns {Promise<Image>}
   */
async getSFSymbolImage(sfSymbolName) {
//     return SFSymbol.named(sfSymbolName).image;
    return await this.getImageByUrl(`https://gitlab.com/j8468/ggfv/-/raw/main/icon/${sfSymbolName}.png`)
  } 
  /**
   * 动态设置组件背景色
   * @param {ListWidget || WidgetStack} widget
   * @param {'Small' || 'Medium' || 'Large'} widgetFamily
   */
  async setWidgetDynamicBackground(widget, widgetFamily) {
    if (this.settings['backgroundPhoto' + widgetFamily]) {
      widget.backgroundImage = await FileManager.local().readImage(this.settings['backgroundPhoto' + widgetFamily]);
    } else {
      const bgColor = new LinearGradient();
      const lightBgColors = this.settings['lightBgColors'] ? this.settings['lightBgColors'].split(',') : this.lightDefaultBackgroundColorGradient;
      const darkBgColors = this.settings['darkBgColors'] ? this.settings['darkBgColors'].split(',') : this.darkDefaultBackgroundColorGradient;
      const colorArr = [];
      lightBgColors.forEach((color, index) => {
        const dynamicColor = Color.dynamic(new Color(lightBgColors[index], 1), new Color(darkBgColors[index], 1));
        colorArr.push(dynamicColor);
      });
      bgColor.colors = colorArr;
      bgColor.locations = this.settings['bgColorsLocations'] ? this.settings['bgColorsLocations'].split(',').map(i => parseFloat(i)) : [0.0, 1.0];
      widget.backgroundGradient = bgColor;
    }
  }

  /**
   * 下载额外的主题文件
   * @returns {Promise<void>}
   */
  async actionDownloadThemes() {
    const FILE_MGR = FileManager[module.filename.includes('Documents/iCloud~') ? 'iCloud' : 'local']();

    const request = new Request('https://cdn.jsdelivr.net/gh/JaxsonWang/Scriptable-VW@latest/build/themes.json');
    const response = await request.loadJSON();
    const themes = response['themes'];

    const alert = new Alert();
    alert.title = '下载主题';
    alert.message = '点击下载你喜欢的主题，并且在桌面引入主题风格即可';

    themes.forEach(item => {
      alert.addAction(item.name);
    });

    alert.addCancelAction('退出菜单');
    const id = await alert.presentSheet();
    if (id === -1) return

    await this.notify('正在下载主题中...');
    const REMOTE_REQ = new Request(themes[id]?.download);
    const REMOTE_RES = await REMOTE_REQ.load();
    FILE_MGR.write(FILE_MGR.joinPath(FILE_MGR.documentsDirectory(), themes[id]?.fileName), REMOTE_RES);

    await this.notify(`${themes[id]?.name} 主题下载完毕，快去使用吧！`);
  }

  /**
   * 调试日志
   */
  async actionDebug() {
    const alert = new Alert();
    alert.title = '组件调试日志';
    alert.message = '用于调试一些奇怪的问题，配合开发者调试数据';

    const menuList = [{
      name: 'setTrackingLog',
      text: `${this.settings['trackingLogEnabled'] ? '开启' : '关闭'}追踪日志`
    }, {
      name: 'viewTrackingLog',
      text: '查阅追踪日志'
    }, {
      name: 'clearTrackingLog',
      text: '清除追踪日志'
    }, {
      name: 'viewErrorLog',
      text: '查阅报错日志'
    }, {
      name: 'clearErrorLog',
      text: '清除报错日志'
    }];

    menuList.forEach(item => {
      alert.addAction(item.text);
    });

    alert.addCancelAction('取消设置');
    const id = await alert.presentSheet();
    if (id === -1) return
    await this[menuList[id].name]();
  }

  /**
   * 开启日志追踪
   * @returns {Promise<void>}
   */
  async setTrackingLog() {
    const alert = new Alert();
    alert.title = '是否开启数据更新日志追踪';
    alert.message = this.settings['trackingLogEnabled'] ? '当前日志追踪状态已开启' : '当前日志追踪状态已关闭';
    alert.addAction('开启');
    alert.addCancelAction('关闭');

    const id = await alert.presentAlert();
    this.settings['trackingLogEnabled'] = id !== -1;
    await this.saveSettings(false);
    return await this.actionDebug()
  }

  /**
   * 查阅日志
   * @returns {Promise<void>}
   */
  async viewTrackingLog() {
    console.log('数据更新日志：');
    console.log(this.settings['debug_bootstrap_date_time']);

    const alert = new Alert();
    alert.title = '查阅跟踪日志';
    alert.message = this.settings['debug_bootstrap_date_time'] || '暂无日志';
    alert.addAction('关闭');
    await alert.presentAlert();
    return await this.actionDebug()
  }

  /**
   * 清除日志
   * @returns {Promise<void>}
   */
  async clearTrackingLog() {
    this.settings['debug_bootstrap_date_time'] = undefined;
    await this.saveSettings(false);
    return await this.actionDebug()
  }

  /**
   * 查阅错误日志
   * @return {Promise<void>}
   */
  async viewErrorLog() {
    console.log('错误日志：');
    console.log(this.settings['error_bootstrap_date_time'] || '暂无日志');

    const alert = new Alert();
    alert.title = '查阅错误日志';
    alert.message = this.settings['error_bootstrap_date_time'] || '暂无日志';
    alert.addAction('关闭');
    await alert.presentAlert();
    return await this.actionDebug()
  }

  /**
   * 清除错误日志
   * @return {Promise<void>}
   */
  async clearErrorLog() {
    this.settings['error_bootstrap_date_time'] = undefined;
    await this.saveSettings(false);
    return await this.actionDebug()
  }

  /**
   * 写入错误日志
   * @param data
   * @param error
   * @return {Promise<void>}
   */
  async writeErrorLog(data, error) {
    const type = Object.prototype.toString.call(data);
    let log = data;
    if (type === '[object Object]' || type === '[object Array]') {
      log = JSON.stringify(log);
    }
    this.settings['error_bootstrap_date_time'] = this.formatDate(new Date(), '\nyyyy年MM月dd日 HH:mm:ss 错误日志：\n') + ' - ' + error + log;
    await this.saveSettings(false);
  }

/**
   * 获取远程图片内容
   * @param {string} url 图片地址
   * @param {boolean} useCache 是否使用缓存（请求失败时获取本地缓存）
   */
  async getImageByUrl(url, useCache = true) {
    const cacheKey = md5(url);
    const cacheFile = FileManager.local().joinPath(FileManager.local().temporaryDirectory(), cacheKey);
    // 判断是否有缓存
    if (useCache && FileManager.local().fileExists(cacheFile)) {
      return Image.fromFile(cacheFile)
    }
    try {
      const req = new Request(url);
      const img = await req.loadImage();
      // 存储到缓存
      FileManager.local().writeImage(cacheFile, img);
      return img
    } catch (e) {
      // 没有缓存+失败情况下，返回自定义的绘制图片（红色背景）
      let ctx = new DrawContext();
      ctx.size = new Size(100, 100);
      ctx.setFillColor(Color.red());
      ctx.fillRect(new Rect(0, 0, 100, 100));
      return ctx.getImage()
    }
  }

async htt(
    icon = 'square.grid.2x2',
    color = '#e8e8e8',
    cornerWidth = 42){
    try {
      let sf = SFSymbol.named(icon);
      if (sf == null) {
        sf = SFSymbol.named('scribble');
      }
      sf.applyFont(Font.mediumSystemFont(30));
      const imgData = Data.fromPNG(sf.image).toBase64String();
      const html = `
    <img id="sourceImg" src="data:image/png;base64,${imgData}" />
    <img id="silhouetteImg" src="" />
    <canvas id="mainCanvas" />
    `
      const js = `
    var canvas = document.createElement("canvas");
    var sourceImg = document.getElementById("sourceImg");
    var silhouetteImg = document.getElementById("silhouetteImg");
    var ctx = canvas.getContext('2d');
    var size = sourceImg.width > sourceImg.height ? sourceImg.width : sourceImg.height;
    canvas.width = size;
    canvas.height = size;
    ctx.drawImage(sourceImg, (canvas.width - sourceImg.width) / 2, (canvas.height - sourceImg.height) / 2);
    var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var pix = imgData.data;
    //convert the image into a silhouette
    for (var i=0, n = pix.length; i < n; i+= 4){
      //set red to 0
      pix[i] = 255;
      //set green to 0
      pix[i+1] = 255;
      //set blue to 0
      pix[i+2] = 255;
      //retain the alpha value
      pix[i+3] = pix[i+3];
    }
    ctx.putImageData(imgData,0,0);
    silhouetteImg.src = canvas.toDataURL();
    output=canvas.toDataURL()
    `

      let wv = new WebView();
      await wv.loadHTML(html);
      const base64Image = await wv.evaluateJavaScript(js);
      const iconImage = await new Request(base64Image).loadImage();
      const size = new Size(160, 160);
      const ctx = new DrawContext();
      ctx.opaque = false;
      ctx.respectScreenScale = true;
      ctx.size = size;
      
      const path = new Path();
      const rect = new Rect(0, 0, size.width, size.width);

      path.addRoundedRect(rect, cornerWidth, cornerWidth);
      path.closeSubpath();
      ctx.setFillColor(new Color(color));
      ctx.addPath(path);
      ctx.fillPath();
      const rate = 36;
      const iw = size.width - rate;
      const x = (size.width - iw) / 2;
      ctx.drawImageInRect(iconImage, new Rect(x, x, iw, iw));
//       console.log(iconImage);
      return ctx.getImage();
      
    } catch (error) {
      console.error(error);
    }
  }


async getFont(fontName, fontSize) {
    if (fontName == 'SF UI Display') {
      return Font.systemFont(fontSize);
    }
    if (fontName == 'SF UI Display Bold') {
      return Font.semiboldSystemFont(fontSize);
    }
    if (fontName == 'monoSpaced') {
      return Font.regularMonospacedSystemFont(fontSize);
    }
    return new Font(fontName, fontSize);
  }
   
 async getImageSize(imageWidth, imageHeight, canvasWidth, canvasHeight, resizeRate = 0.85) {
    let a = imageWidth;
    let b = imageHeight;

    if (a > canvasWidth || b > canvasHeight) {
        if (resizeRate >= 1) {
            resizeRate = 0.99;
        }
        a *= resizeRate;
        b *= resizeRate;
        return this.getImageSize(a, b, canvasWidth, canvasHeight);
    }

    return {width: a, height: b};
  }
  async tyyy(icon,hex){
  return await this.drawTableIcon(icon,parseInt('0x' + hex.slice(1, 3)),parseInt('0x' + hex.slice(3, 5)),parseInt('0x' + hex.slice(5,7)));
 console.warn(hex);
};
async drawTableIcon (icon = 'square.grid.2x2',x=104,y=135,z=209) {
    
    let sfi;
    try{
      sfi = SFSymbol.named(icon);
      sfi.applyFont(Font.mediumSystemFont(30));
      var imgData = Data.fromPNG(sfi.image).toBase64String();
    } catch(e) {
      var imgData = Data.fromPNG(icon).toBase64String();
    }
    
    const html = `
    <img id="sourceImg" src="data:image/png;base64,${imgData}" />
    <img id="silhouetteImg" src="" />
    <canvas id="mainCanvas" />`;
    const js = `
    var canvas = document.createElement("canvas");
    var sourceImg = document.getElementById("sourceImg");
    var silhouetteImg = document.getElementById("silhouetteImg");
    var ctx = canvas.getContext('2d');
    var size = sourceImg.width > sourceImg.height ? sourceImg.width : sourceImg.height;
    canvas.width = size;
    canvas.height = size;
    ctx.drawImage(sourceImg, (canvas.width - sourceImg.width) / 2, (canvas.height - sourceImg.height) / 2);
    var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var pix = imgData.data;
    // 将图像转换为剪影
    for (var i=0, n = pix.length; i < n; i+= 4){
      //set red to 0 设置为红色到0
      pix[i] = ${x};
      //set green to 0 设置绿色到0
      pix[i+1] = ${y};
      //set blue to 0 设置为蓝色到0
      pix[i+2] = ${z};
      //retain the alpha value 保留阿尔法值
      pix[i+3] = pix[i+3];
    }
    ctx.putImageData(imgData,0,0);
    silhouetteImg.src = canvas.toDataURL();
    output=canvas.toDataURL()`;
    let wv = new WebView();
    await wv.loadHTML(html);
    const base64Image = await wv.evaluateJavaScript(js);
    const iconImage = await new Request(base64Image).loadImage();
    const image = iconImage;
    return image;
  };
  async setCustomAction(title, desc, opt, flag = true, notify = true) {
    const a = new Alert();
    a.title = title;
    a.message = !desc ? '' : '\n'+desc;
    Object.keys(opt).forEach((key) => {
      flag ? a.addTextField(opt[key], this.settings[key]) : a.addTextField(opt[key], this.djgSettings[key]);
    });
    a.addAction('确定');
    a.addCancelAction('取消');
    const id = await a.presentAlert();
    if (id === -1) return false;
    const data = {};
    Object.keys(opt).forEach(async (key, index) => {
      let temp = a.textFieldValue(index);
      data[key] = temp.replace(' ', '');
    });
    flag ? this.settings = { ...this.settings, ...data } : this.djgSettings = { ...this.djgSettings, ...data };
    this.saveSettings(notify, flag);
    return true;
  };



  /**
   * 偏好设置
   * @returns {Promise<void>}
   */
  async actionPreferenceSettings() {
    const aa=new UITable()
     aa.showSeparators=true;

      const menuList = [
      {
        name: 'setMyCarName',
        text: '自定义车辆名称',
        icon: 'person.text.rectangle.fill',
        color:'#ff4800'
      },
      {
        name: 'setMyCarModelName',
        text: '自定义车辆功率',
        icon: 'tag',
        color:'#6c00ff'
      },
      /*
{
        name: 'setMyCarPhoto',
        text: '车辆照片',
        icon: 'wand.and.stars',
        color:'#4cc9f0'
      },
*/
      {
        name: 'setMyCarLogo',
        text: 'LOGO照片',
        icon: 'gobackward',
        color:'#7743db'
      },
      {
        name: 'setMyOne',
        text: '自定义一言',
        icon: 'gobackward.minus',
        color:'#27e1c1'
      },
      {
        name: 'setAMapKey',
        text: '设置车辆位置',
        icon: 'car',
        color:'#4cc9f0'
      },
      {
        name: 'setLocationFormat',
        text: '位置信息格式',
        icon: 'eyes',
        color:'#6c00ff'
      },
      {
        name: 'setShowType',
        text: '信息描述风格',
        icon: 'key',
        color:'#ff4800'
      },
    ];

    const header = new UITableRow();
    const heading = header.addText('个性化设置');
    heading.titleFont = Font.mediumSystemFont(17);
    heading.centerAligned();
    aa.addRow(header);
    
      for(let item of menuList){
      let row2 = new UITableRow();
      row2.dismissOnSelect = false;
      let img2 = row2.addImage(await this.htt(item.icon,item.color));
      img2.widthWeight = 100;
      let rowtext2 = row2.addText(item.text);   
      rowtext2.widthWeight = 400;
      let valText2 = row2.addText('>');
      valText2.titleColor =Color.blue();
      valText2.widthWeight = 500;
      valText2.rightAligned();
      row2.onSelect = async () => {   
         eval('this.'+item.name+'()')
      }
      aa.addRow(row2);
      }
      aa.present();    
    
  }





  /**
   * 界面微调
   * @returns {Promise<void>}
   */
  async actionUIRenderSettings() {
    const aa=new UITable()
    aa.showSeparators=true;
    const menuList = [
      {
        name: 'showyh',
        text: '切换油耗数据显示',
        icon: 'gobackward',
        color:'#2e8b57'
      },
      {
        name: 'setBackgroundConfig',
        text: '自定义组件背景',
        icon: 'key',
        color:'#66cd00'
      },
      {
        name: 'setFontFamily',
        text: '设置字体风格',
        icon: 'eyes',
        color:'#6a63b8'
      },
      {
        name: 'setLockSuccessStyle',
        text: '锁车提示风格',
        icon: 'car',
        color:'#30c758'
      },
      {
        name: 'setLargeLocationBorderRadius',
        text: '大组件边界弧度',
        icon: 'gobackward.minus',
        color:'#7743db'
      },
      {
        name: 'setLargeMapType',
        text: '大组件地图风格',
        icon: 'gobackward',
        color:'#4cc9f0'
      },
      {
        name: 'setMapZoom',
        text: '设置地图缩放',
        icon: 'wand.and.stars',
        color:'#6c00ff'
      },
      {
        name: 'showPlate',
        text: '设置车辆牌照',
        icon: 'tag',
        color:'#ff4800'
      },
      {
        name: 'showPlate1',
        text: '牌照显示风格',
        icon: 'car',
        color:'#4cc9f0'
      },
      {
        name: 'showOil',
        text: '设置机油显示',
        icon: 'square.and.pencil',
        color:'#27e1c1'
      },
      {
        name: 'showzhzt',
        text: '切换中组件主题',
        icon: 'theatermasks.circle',
        color:'#6887d1'
      },
      
    ];
    const header = new UITableRow();
    const heading = header.addText('界面调整');
    heading.titleFont = Font.mediumSystemFont(17);
    heading.centerAligned();
    aa.addRow(header);
    
      for(let item of menuList){
      let row2 = new UITableRow();
      row2.dismissOnSelect = false;
      let img2 = row2.addImage(await this.htt(item.icon,item.color));
      img2.widthWeight = 100;
      let rowtext2 = row2.addText(item.text);   
      rowtext2.widthWeight = 600;
      let valText2 = row2.addText('>');
      valText2.titleColor =Color.blue();
      valText2.widthWeight = 500;
      valText2.rightAligned();
      row2.onSelect = async () => {   
         eval('this.'+item.name+'()')
      }
      aa.addRow(row2);
      }
      aa.present(); 

  }

  /**
   * 自定义车辆名称
   * @returns {Promise<void>}
   */
  async setMyCarName() {
    const alert = new Alert();
    alert.title = '车辆名称';
    alert.message = '如果您不喜欢系统返回的名称可以自己定义名称';
    alert.addTextField('请输入自定义名称', this.settings['myCarName'] || this.settings['seriesName']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return ;
    this.settings['myCarName'] = alert.textFieldValue(0) || this.settings['seriesName'];
    await this.saveSettings();
  }

  /**
   * 自定义车辆功率
   * @returns {Promise<void>}
   */
  async setMyCarModelName() {
    const alert = new Alert();
    alert.title = '车辆功率';
    alert.message = '根据车辆实际情况可自定义功率类型，不填为系统默认';
    alert.addTextField('请输入自定义功率', this.settings['myCarModelName'] || this.settings['carModelName']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['myCarModelName'] = alert.textFieldValue(0) || this.settings['carModelName'];
    await this.saveSettings();
  }

  /**
   * 自定义车辆图片
   * @returns {Promise<void>}
   */
  async setMyCarPhoto() {
    const alert = new Alert();
    alert.title = '车辆图片';
    alert.message = '请在相册选择您最喜欢的车辆图片以便展示到小组件上，最好是全透明背景PNG图。';
    alert.addAction('选择照片');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    try {
      const image = await Photos.fromLibrary();
      const imagePath = FileManager.local().joinPath(FileManager.local().documentsDirectory(), `myCarPhoto_${this.SETTING_KEY}`);
      await FileManager.local().writeImage(imagePath, image);
      this.settings['myCarPhoto'] = imagePath;
      await this.saveSettings();
    } catch (error) {
      // 取消图片会异常 暂时不用管
    }
  }

  /**
   * 自定义 LOGO
   * @returns {Promise<void>}
   */
  async setMyCarLogo() {
    const alert = new Alert();
    alert.title = 'LOGO 图片';
    alert.message = '请在相册选择 LOGO 图片以便展示到小组件上，最好是全透明背景PNG图。';
    alert.addAction('选择照片');
    alert.addAction('恢复默认logo');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
//     console.warn(id);
    if(id === 1){
      this.settings['myCarLogo'] = '';
      await this.saveSettings();
      return 
    }
    // 选择图片
    try {
      const image = await Photos.fromLibrary();
      const imagePath = FileManager.local().joinPath(FileManager.local().documentsDirectory(), `myCarLogo_${this.SETTING_KEY}`);
      await FileManager.local().writeImage(imagePath, image);
      this.settings['myCarLogo'] = imagePath;
      await this.saveSettings();
    } catch (error) {
      // 取消图片会异常 暂时不用管
    }
    // 设置图片颜色
    const message = '请选择是否需要图片颜色填充？\n' +
      '原彩色：保持图片颜色\n' +
      '字体色：和字体颜色统一';
    const sizes = ['原彩色', '字体色'];
    const size = await this.generateAlert('提示', message, sizes);
    if (size === 1) {
      this.settings['logoTintType'] = 'fontColor';
      await this.saveSettings();
      return 
    }
    this.settings['logoTintType'] = 'default';
    await this.saveSettings();
  }

  /**
   * 设置LOGO图片大小
   * @returns {Promise<void>}
   */
  async setMyCarLogoSize() {
    const alert = new Alert();
    alert.title = '设置 LOGO 大小';
    alert.message = `不填为默认，默认图片宽度为 ${this.logoWidth} 高度为 ${this.logoHeight}`;

    alert.addTextField('logo 宽度', this.settings['logoWidth']);
    alert.addTextField('logo 高度', this.settings['logoHeight']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    const logoWidth = alert.textFieldValue(0) || this.logoWidth;
    const logoHeight = alert.textFieldValue(1) || this.logoHeight;

    this.settings['logoWidth'] = logoWidth;
    this.settings['logoHeight'] = logoHeight;
    await this.saveSettings();
  }

  /**
   * 自定义组件背景
   * @returns {Promise<void>}
   */
  async setBackgroundConfig() {
    const alert = new Alert();
    alert.title = '自定义组件背景';
    alert.message = '颜色背景和图片背景共同存存在时，图片背景设置优先级更高，将会加载图片背景\n' +
      '只有清除组件背景图片时候颜色背景才能生效！';

    const menuList = [{
      name: 'setColorBackground',
      text: '设置颜色背景',
      icon: '🖍'
    }, {
      name: 'setImageBackground',
      text: '设置图片背景',
      icon: '🏞'
    }, {
      name: 'actionUIRenderSettings',
      text: '返回上一级',
      icon: '👈'
    }];

    menuList.forEach(item => {
      alert.addAction(item.icon + ' ' + item.text);
    });

    alert.addCancelAction('取消设置');
    const id = await alert.presentSheet();
    if (id === -1) return
    await this[menuList[id].name]();
  }

  /**
   * 设置组件颜色背景
   * @returns {Promise<void>}
   */
  async setColorBackground() {
    const alert = new Alert();
    alert.title = '自定义颜色背景';
    alert.message = '系统浅色模式适用于白天情景\n' +
      '系统深色模式适用于晚上情景\n' +
      '请根据自己的偏好进行设置，请确保您的手机「设置 - 显示与亮度」外观「自动」选项已打开\n' +
      '颜色列表只写一个为纯色背景，多个则是渐变背景，格式如下：' +
      '「#fff」或者「#333,#666,#999」\n' +
      '位置列表规格如下：「0.0, 1.0」请填写 0.0 到 1.0 范围内，根据值选项渲染渐变效果不同\n' +
      '使用英文逗号分隔，颜色值可以不限制填写，全部为空则不启用该功能';

    alert.addTextField('浅色背景颜色列表', this.settings['lightBgColors']);
    alert.addTextField('浅色字体颜色', this.settings['lightTextColor']);
    alert.addTextField('深色背景颜色列表', this.settings['darkBgColors']);
    alert.addTextField('深色字体颜色', this.settings['darkTextColor']);
    alert.addTextField('渐变位置列表值', this.settings['bgColorsLocations']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return await this.setBackgroundConfig()
    const lightBgColors = alert.textFieldValue(0);
    const lightTextColor = alert.textFieldValue(1);
    const darkBgColors = alert.textFieldValue(2);
    const darkTextColor = alert.textFieldValue(3);
    const bgColorsLocations = alert.textFieldValue(4);

    if (lightBgColors.split(',').length !== darkBgColors.split(',').length) return this.setColorBackground()

    this.settings['lightBgColors'] = lightBgColors;
    this.settings['lightTextColor'] = lightTextColor;
    this.settings['darkBgColors'] = darkBgColors;
    this.settings['darkTextColor'] = darkTextColor;
    this.settings['bgColorsLocations'] = bgColorsLocations;
    await this.saveSettings();
    return await this.setBackgroundConfig()
  }

  /**
   * 设置组件图片背景
   * @returns {Promise<void>}
   */
  async setImageBackground() {
    const alert = new Alert();
    alert.title = '自定义图片背景';
    alert.message = '目前自定义图片背景可以设置下列俩种场景\n' +
      '透明背景：因为组件限制无法实现，目前使用桌面图片裁剪实现所谓对透明组件，设置之前需要先对桌面壁纸进行裁剪哦，请选择「裁剪壁纸」菜单进行获取透明背景图片\n' +
      '图片背景：选择您最喜欢的图片作为背景';

    const menuList = [{
      name: 'setTransparentBackground',
      text: '透明壁纸',
      icon: '🌅'
    }, {
      name: 'setPhotoBackground',
      text: '自选图片',
      icon: '🌄'
    }, {
      name: 'setColorBackgroundTextColor',
      text: '字体颜色',
      icon: '✍️'
    }, {
      name: 'removeImageBackground',
      text: '还原背景',
      icon: '🪣'
    }, {
      name: 'setBackgroundConfig',
      text: '返回上一级',
      icon: '👈'
    }];

    menuList.forEach(item => {
      alert.addAction(item.icon + ' ' + item.text);
    });

    alert.addCancelAction('取消设置');
    const id = await alert.presentSheet();
    if (id === -1) return
    await this[menuList[id].name]();
  }

  /**
   * 透明（剪裁）壁纸
   * @returns {Promise<void>}
   */
  async setTransparentBackground() {
    let message = '开始之前，请转到主屏幕并进入桌面编辑模式，滚动到最右边的空页面，然后截图！';
    const exitOptions = ['前去截图', '继续'];
    const shouldExit = await this.generateAlert('提示', message, exitOptions);
    if (!shouldExit) return

    // Get screenshot and determine phone size.
    try {
      const img = await Photos.fromLibrary();
      const height = img.size.height;
      const phone = this.phoneSizes()[height];
      if (!phone) {
        message = '您选择的照片好像不是正确的截图，或者您的机型暂时不支持。';
        await this.generateAlert('提示', message, ['OK']);
        return await this.setImageBackground()
      }

      // Prompt for widget size and position.
      message = '您创建组件的是什么规格？';
      const sizes = ['小组件', '中组件', '大组件'];
      const _sizes = ['Small', 'Medium', 'Large'];
      const size = await this.generateAlert('提示', message, sizes);
      const widgetSize = _sizes[size];

      message = '在桌面上组件存在什么位置？';
      message += (height === 1136 ? ' （备注：当前设备只支持两行小组件，所以下边选项中的「中间」和「底部」的选项是一致的）' : '');

      // Determine image crop based on phone size.
      const crop = {w: '', h: '', x: '', y: ''};
      let positions = '';
      let _positions = '';
      let position = '';
      switch (widgetSize) {
        case 'Small':
          crop.w = phone.small;
          crop.h = phone.small;
          positions = ['Top left', 'Top right', 'Middle left', 'Middle right', 'Bottom left', 'Bottom right'];
          _positions = ['左上角', '右上角', '中间左', '中间右', '左下角', '右下角'];
          position = await this.generateAlert('提示', message, _positions);

          // Convert the two words into two keys for the phone size dictionary.
          const keys = positions[position].toLowerCase().split(' ');
          crop.y = phone[keys[0]];
          crop.x = phone[keys[1]];
          break
        case 'Medium':
          crop.w = phone.medium;
          crop.h = phone.small;

          // Medium and large widgets have a fixed x-value.
          crop.x = phone.left;
          positions = ['Top', 'Middle', 'Bottom'];
          _positions = ['顶部', '中部', '底部'];
          position = await this.generateAlert('提示', message, _positions);
          const key = positions[position].toLowerCase();
          crop.y = phone[key];
          break
        case 'Large':
          crop.w = phone.medium;
          crop.h = phone.large;
          crop.x = phone.left;
          positions = ['Top', 'Bottom'];
          _positions = ['顶部', '底部'];
          position = await this.generateAlert('提示', message, _positions);

          // Large widgets at the bottom have the 'middle' y-value.
          crop.y = position ? phone.middle : phone.top;
          break
      }

      // Crop image and finalize the widget.
      const imgCrop = this.cropImage(img, new Rect(crop.x, crop.y, crop.w, crop.h));

      const imagePath = FileManager.local().joinPath(FileManager.local().documentsDirectory(), `backgroundPhoto${widgetSize}_${this.SETTING_KEY}`);
      await FileManager.local().writeImage(imagePath, imgCrop);
      this.settings['backgroundPhoto' + widgetSize] = imagePath;
      await this.saveSettings();
      await this.setImageBackground();
    } catch (error) {
      // 取消图片会异常 暂时不用管
      console.error(error);
    }
  }

  /**
   * 自选图片
   * @returns {Promise<void>}
   */
  async setPhotoBackground() {
    try {
      let message = '您创建组件的是什么规格？';
      const sizes = ['小组件', '中组件', '大组件'];
      const _sizes = ['Small', 'Medium', 'Large'];
      const size = await this.generateAlert('提示', message, sizes);
      const widgetSize = _sizes[size];

      const image = await Photos.fromLibrary();
      const imagePath = FileManager.local().joinPath(FileManager.local().documentsDirectory(), `backgroundPhoto${widgetSize}_${this.SETTING_KEY}`);
      await FileManager.local().writeImage(imagePath, image);
      this.settings['backgroundPhoto' + widgetSize] = imagePath;
      await this.saveSettings();
      await this.setImageBackground();
    } catch (error) {
      // 取消图片会异常 暂时不用管
    }
  }

  /**
   * 设置图片背景下的字体颜色
   * @return {Promise<void>}
   */
  async setColorBackgroundTextColor() {
    const alert = new Alert();
    alert.title = '字体颜色';
    alert.message = '仅在设置图片背景情境下进行对字体颜色更改。字体颜色规格：#ffffff';
    alert.addTextField('请输入字体颜色值', this.settings['backgroundImageTextColor']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return await this.setImageBackground()
    this.settings['backgroundImageTextColor'] = alert.textFieldValue(0);
    await this.saveSettings();

    return await this.setImageBackground()
  }

  /**
   * 移除背景图片
   * @return {Promise<void>}
   */
  async removeImageBackground() {
    this.settings['backgroundPhotoSmall'] = undefined;
    this.settings['backgroundPhotoMedium'] = undefined;
    this.settings['backgroundPhotoLarge'] = undefined;
    await this.saveSettings();
    await this.setImageBackground();
  }

  /**
   * 输入一言
   * @returns {Promise<void>}
   */
  async setMyOne() {
    const alert = new Alert();
    alert.title = '输入一言';
    alert.message = `请输入一言，将会在桌面展示语句，不填则显示 「${this.defaultMyOne}」`;
    alert.addTextField('请输入一言', this.settings['myOne']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['myOne'] = alert.textFieldValue(0);
    await this.saveSettings();
  }

  /**
   * 设置显示风格
   * @returns {Promise<void>}
   */
  async setShowType() {
    const message = '设置组件信息根据你的选择进行展示？';
    const menus = ['图标描述', '文字描述'];
    // 默认 显示图标描述
    this.settings['showType'] = Boolean(await this.generateAlert('组件信息描述', message, menus));
    await this.saveSettings();
    return 
  }

  /**
   * 设置车辆位置
   * @returns {Promise<void>}
   */
  async setAMapKey() {
    const alert = new Alert();
    alert.title = '设置车辆位置';
    alert.message = '请输入组件所需要的高德地图密钥，用于车辆逆地理编码以及地图资源\n如不填写则关闭地址显示';
    alert.addTextField('高德地图密钥', this.settings['aMapKey']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['aMapKey'] = alert.textFieldValue(0);
    await this.saveSettings();
  }
  /**
   * 设置qd
   * @returns {Promise<void>}
   */
  async setqd() {
    const alert = new Alert();
    alert.title = '设置微店签到';
    alert.message = '请输入组件所需要的微店签到密钥，用于微店每日签到\n如不填写则关闭签到';
    alert.addTextField('微店签到密钥', this.settings['qdkey']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['qdkey'] = alert.textFieldValue(0);
    await this.saveSettings();
  }
// 车辆图片链接
async setpic() {
    const alert = new Alert();
    alert.title = '设置车辆图片';
    alert.message = '请输入车辆图片链接';
    alert.addTextField('远程图片链接', this.settings['myCarPhotoUrl']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['myCarPhotoUrl'] = alert.textFieldValue(0);
    await this.saveSettings();
  }
  /**
   * 位置信息格式
   * @returns {Promise<void>}
   */
  async setLocationFormat() {
    const alert = new Alert();
    alert.title = '位置信息格式';
    alert.message = '请输入组件所需要的位置信息格式，格式如下【国|省|市|区|乡镇|街道|社区|建筑】\n如不填写则默认显示标准位置信息';
    alert.addTextField('位置信息格式', this.settings['locationFormat']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['locationFormat'] = alert.textFieldValue(0);
    await this.saveSettings();
  }

  /**
   * 车牌显示
   * @returns {Promise<void>}
   */
  async showPlate1() {
    const title = '车牌显示风格切换';
    const message = this.settings['showPlate'] ? '当前显示为文字车牌' : '当前显示为仿真车牌';
    const menus = ['仿真车牌', '文字车牌'];
    this.settings['showPlate'] = Boolean(await this.generateAlert(title, message, menus));
    await this.saveSettings();
  }

  /**
   * 机油显示
   * @returns {Promise<void>}
   */
  async showOil() {
    const title = '是否显示机油数据';
    const message = (this.settings['showOil'] ? '当前机油显示状态已开启' : '当前机油显示状态已关闭') + '，机油数据仅供参考，长时间停车会造成机油数据不准确，请悉知！';
    const menus = ['关闭显示', '开启显示'];
    this.settings['showOil'] = Boolean(await this.generateAlert(title, message, menus));
    await this.saveSettings();
  }
  /**
   * 油耗数据切换显示
   * @returns {Promise<void>}
   */
  async showyh() {
    const title = '是否切换油耗数据';
    const message = (this.settings['showyh'] ? '当前显示为上次行程数据\n' : '当前显示为加油后数据\n') + '切换后稍等自动刷新！';
    const menus = ['切换加油后数据', '切换上次行程数据'];
    this.settings['showyh'] = Boolean(await this.generateAlert(title, message, menus));
    await this.saveSettings();
  }
  /**
   * 主题切换显示
   * @returns {Promise<void>}
   */
  async showzhzt() {
    const title = '是否切换主题';
    const message = (this.settings['zhzt'] ? '当前为[主题1]\n' : '当前为[初始主题]\n') + '切换后稍等自动刷新！';
    const menus = ['切换初始主题', '切换主题1'];
    this.settings['zhzt'] = Boolean(await this.generateAlert(title, message, menus));
    await this.saveSettings();
  }
  /**
   * 设置字体风格
   * @returns {Promise<void>}
   */
  async setFontFamily() {
    const alert = new Alert();
    alert.title = '设置字体风格';
    alert.message = '目前默认是「PingFang SC」并且只有标准体和粗体，请到 http://iosfonts.com 选择您喜欢的字体风格吧';
    alert.addTextField('标准字体', this.settings['regularFont']);
    alert.addTextField('粗体', this.settings['boldFont']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    const regularFont = alert.textFieldValue(0);
    const boldFont = alert.textFieldValue(1);

    this.settings['regularFont'] = regularFont;
    this.settings['boldFont'] = boldFont;
    await this.saveSettings();
  }

  /**
   * 设置锁车风格
   * @returns {Promise<void>}
   */
  async setLockSuccessStyle() {
    const message = '用于设置锁车提示风格，可以设置绿色或者字体色俩种风格';
    const sizes = ['绿色', '字体色'];
    const size = await this.generateAlert('提示', message, sizes);
    if (size === 1) {
      this.settings['lockSuccessStyle'] = 'fontColor';
      await this.saveSettings();
    }
    this.settings['lockSuccessStyle'] = 'successColor';
    await this.saveSettings();
  }

  /**
   * 设置大组件位置边界弧度
   * @returns {Promise<void>}
   */
  async setLargeLocationBorderRadius() {
    const alert = new Alert();
    alert.title = '设置弧度';
    alert.message = `大组件下方长方形弧度设置，默认是 ${this.locationBorderRadius}，请输入数字类型。`;
    alert.addTextField('弧度大小', this.settings['locationBorderRadius']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['locationBorderRadius'] = alert.textFieldValue(0);
    await this.saveSettings();
  }

  /**
   * 设置大组件地图展示风格
   * @returns {Promise<void>}
   */
  async setLargeMapType() {
    const message = '用于大组件展示地图风格';
    const menus = ['默认', '全地图'];
    this.settings['largeMapType'] = Boolean(await this.generateAlert('提示', message, menus));
    await this.saveSettings();
  }

  /**
   * 设置大组件地图缩放
   * @returns {Promise<void>}
   */
  async setMapZoom() {
    const alert = new Alert();
    alert.title = '设置缩放比例';
    alert.message = `大组件下方地图缩放数字越小缩放越大，范围在（1 ~ 17），默认是 ${this.locationMapZoom}，请输入数字类型。`;
    alert.addTextField('缩放大小', this.settings['locationMapZoom']);
    alert.addAction('确定');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return 
    this.settings['locationMapZoom'] = alert.textFieldValue(0);
    await this.saveSettings();
  }

  /**
   * 刷新数据
   */
  async actionRefreshData() {
    const alert = new Alert();
    alert.title = '刷新数据';
    alert.message = '如果发现数据延迟，选择对应函数获取最新数据，同样也是获取日志分享给开发者使用。';

    const menuList = [{
      name: 'getData',
      text: '组件数据'
    }, {
      name: 'handleLoginRequest',
      text: '用户信息数据'
    }, {
      name: 'getVehiclesStatus',
      text: '当前车辆状态数据'
    }, {
      name: 'getVehiclesPosition',
      text: '车辆经纬度数据'
    }, {
      name: 'getCarAddressInfo',
      text: '车辆位置数据'
    }];

    menuList.forEach(item => {
      alert.addAction(item.text);
    });

    alert.addCancelAction('退出菜单');
    const id = await alert.presentSheet();
    if (id === -1) return
    // 执行函数
    await this[menuList[id].name](true);
  }

  /**
   * 重置登出
   */
  async actionLogOut() {
    const alert = new Alert();
    alert.title = '退出账号';
    alert.message = '您所登录的账号包括缓存本地的数据将全部删除，请慎重操作。';
    alert.addAction('登出');
    alert.addCancelAction('取消');

    const id = await alert.presentAlert();
    if (id === -1) return
    if (Keychain.contains(this.SETTING_KEY)) Keychain.remove(this.SETTING_KEY);
    await this.notify('登出成功', '敏感信息已全部删除');
    Safari.open('scriptable:///run?scriptName=' + encodeURIComponent(Script.name()));
  }

  /**
   * 检查更新
   */
  async checkUpdate(jsonName, txt = true) {
    const fileName = Script.name() + '.js';
    const FILE_MGR = FileManager[module.filename.includes('Documents/iCloud~') ? 'iCloud' : 'local']();
    const request = new Request(`https://gitlab.com/j8468/ggfv/-/raw/main/${jsonName}.json`);
    const response = await request.loadJSON();
    console.log(`远程版本：${response?.version}`);
    if (response?.version === this.version){
      if (txt) {
        return this.notify('无需更新', '远程版本一致，暂无更新')
      } else {
        return
      }
    }
    console.log('发现新的版本');
    const log = response?.changelog.join('\n');
    const alert = new Alert();
    alert.title = '更新提示';
    alert.message = `是否需要升级到${response?.version.toString()}版本\n\r${log}`;
    alert.addAction('更新');
    alert.addCancelAction('取消');
    const id = await alert.presentAlert();
    if (id === -1) return
    await this.notify('正在更新中...');
    const REMOTE_REQ = new Request(response?.download);
    const REMOTE_RES = await REMOTE_REQ.load();
    FILE_MGR.write(FILE_MGR.joinPath(FILE_MGR.documentsDirectory(), fileName), REMOTE_RES);

    await this.notify(' 桌面组件更新完毕！');
      Safari.open('scriptable:///run?scriptName=' + encodeURIComponent(Script.name()));
 
  }

  /**
   * 传送给 Siri 快捷指令车辆信息数据
   * @returns {Object}
   */
  async siriShortcutData() {
    return await this.getData()
  }

  /**
   * 获取车辆地理位置信息
   * @param {Object} location 经纬度
   * @param {boolean} debug 开启日志输出
   * @return {Promise<{customAddress, completeAddress}|{customAddress: *, completeAddress: *}>}
   */
  async getCarAddressInfo(location, debug = false) {
    const longitude = location?.longitude || this.settings['longitude'] || this.settings['longitude'];
    const latitude = location?.latitude || this.settings['latitude'] || this.settings['latitude'];

    // 经纬度异常判断
    if (longitude === undefined || latitude === undefined) {
      return {
        customAddress: '暂无位置信息',
        completeAddress: '暂无位置信息'
      }
    }

    const aMapKey = this.settings['aMapKey']?.trim() || 'c078fb16379c25bc0aad8633d82cf1dd';
    const options = {
      url: `https://restapi.amap.com/v3/geocode/regeo?key=${aMapKey}&location=${longitude},${latitude}&radius=1000&extensions=base&batch=false&roadlevel=0`,
      method: 'GET'
    };
    try {
      const response = await this.http(options);
      if (response.status === '1') {
        const addressComponent = response.regeocode.addressComponent;
        let customAddress = '';
        const format = this.settings['locationFormat']?.split('|')?.map(item => {
          switch (item) {
            case '国':
              item = 'country';
              break
            case '省':
              item = 'province';
              break
            case '市':
              item = 'city';
              break
            case '区':
              item = 'district';
              break
            case '乡镇':
              item = 'township';
              break
            case '社区':
              item = 'neighborhood';
              break
            case '街道':
              item = 'streetNumber';
              break
            case '建筑':
              item = 'building';
              break
          }
          return item
        });
        if (Array.isArray(format)) {
          format.forEach(item => {
            if (item === 'neighborhood') {
              customAddress += (addressComponent[item].name || '');
            } else if (item === 'building') {
              customAddress += (addressComponent[item].name || '');
            } else if (item === 'streetNumber') {
              customAddress += ((addressComponent[item].street || '') + (addressComponent[item].number || ''));
            } else {
              customAddress += (addressComponent[item] || '');
            }
          });
        }
        const completeAddress = response.regeocode.formatted_address || '暂无位置信息';
        this.settings['customAddress'] = customAddress;
        this.settings['completeAddress'] = completeAddress;
        await this.saveSettings(false);
        console.log('获取车辆地理位置信息成功');
        if (debug) {
          console.log('当前车辆地理位置：');
          console.log('自定义地址：' + customAddress);
          console.log('详细地址：' + completeAddress);
          console.log('车辆地理位置返回数据：');
          console.log(response);
        }
        return {
          customAddress,
          completeAddress
        }
      } else {
        console.error('获取车辆位置失败，请检查高德地图 key 是否填写正常');
        await this.notify('获取车辆地理位置失败', '请在【个性设置-设置车辆位置】里填写你的高德地图key');
        this.settings['customAddress'] = '暂无位置信息';
        this.settings['completeAddress'] = '暂无位置信息';
        return {
          customAddress: this.settings['customAddress'],
          completeAddress: this.settings['completeAddress']
        }
      }
    } catch (error) {
      await this.notify('请求失败', '提示：' + error);
      console.error(error);
      this.settings['customAddress'] = '暂无位置信息';
      this.settings['completeAddress'] = '暂无位置信息';
      return {
        customAddress: this.settings['customAddress'],
        completeAddress: this.settings['completeAddress']
      }
    }
  }

  /**
   * 渲染函数，函数名固定
   * 可以根据 this.widgetFamily 来判断小组件尺寸，以返回不同大小的内容
   * @returns {Promise<ListWidget>}
   */
  async render() {
    if (this.settings['isLogin']) {
      const data = await this.getData();
      switch (this.widgetFamily) {
        case 'large':
          return await this.renderLarge(data)
        case 'medium':
        if(this.settings['zhzt']){     
          return await this.renderMedium1(data)
          }else{   
          return await this.renderMedium(data)
          }
        default:
          return await this.renderSmall(data)
      }
    } else {
      return await this.renderEmpty()
    }
  }


 async renderMedium(data) {
  const ee = new ListWidget();
  await this.setWidgetDynamicBackground(ee, 'Medium');
    const rrt = await this.getMyCarPhoto(this.settings['myCarPhotoUrl']);
    const qq = await this.getImageByUrl('http://j8468.gitlab.io/img/qqq.png')
    const vw = await this.getImageByUrl('https://gitlab.com/j8468/ggfv/-/raw/main/img/vw_logo.png');
//     const rrt =await this.getImageByUrl('https://j8468.gitlab.io/img/23bai.png');
//     const rrt = await this.getImageByUrl('https://gitlab.com/j8468/ggfv/-/raw/main/img/st.png')
    let canvas = new DrawContext();
    let kuan = Device.screenResolution().width/2;
    console.warn(rrt);
    console.log(Device.screenSize())
    canvas.size = new Size(378, 158);
    if (this.settings['backgroundPhoto']) {
      console.log('有背景图片');
      const textColor = this.settings['backgroundImageTextColor'] || '#ffffff';
      canvas.setTextColor(new Color(textColor, alpha));
    } else {
      console.log('无背景图片')
      const lightTextColor = this.settings['lightTextColor'] || '#000000';
      const darkTextColor = this.settings['darkTextColor'] || '#ffffff';
//       await this.xgys(canvas);
      canvas.setTextColor(Color.dynamic(new Color(lightTextColor, 1), new Color(darkTextColor, 1)));
      var ttbb = Color.dynamic(new Color(lightTextColor, 1), new Color(darkTextColor, 1)).hex;
      await this.drawTableIcon('clock.arrow.2.circlepath',46,139,87)
//       widget[type] = Color.dynamic(new Color(lightTextColor, alpha), new Color(darkTextColor, alpha));
    }
    canvas.setFont(Font.systemFont(158*0.33))
    canvas.opaque = false;
    canvas.respectScreenScale = true;

const qhh = await this.getImageSize(qq.size.width,qq.size.height , 287*0.5, 154*0.5, 1.4)
const vwlogo = await this.getImageSize(vw.size.width,vw.size.height,378*0.07,378*0.07,1.2)
const hh =await this.getImageSize(rrt.size.width, rrt.size.height , 378*0.55, 158*0.55, 1.2)
 console.warn(hh)
// canvas.drawTextInRect('FVW', new Rect(378 * 0.59, 158*0.21 , 378 * 0.5, 158/2));
// canvas.drawImageInRect(qq, new Rect(378*0.5, 30, Math.round(qhh.width), Math.round(qhh.height)))
canvas.drawImageInRect(rrt, new Rect(378-Math.round(hh.width),158-158*0.19-Math.round(hh.height),Math.round(hh.width) , Math.round(hh.height)))
canvas.drawImageInRect(await this.tyyy(vw,'#' + ttbb), new Rect(378-Math.round(vwlogo.width),0,Math.round(vwlogo.width) , Math.round(vwlogo.height)))



canvas.setFont(Font.systemFont(158*0.13))
 canvas.drawTextInRect(data.seriesName, new Rect(0, 0, 378 * 0.5, 158*0.2));// 
canvas.setFont(Font.systemFont(12))
canvas.drawTextInRect(data.carModelName, new Rect(0, 158*0.18, 378 * 0.5, 30));
canvas.setFont(Font.systemFont(13))
canvas.drawImageInRect(await this.tyyy('fuelpump','#' + ttbb), new Rect(0, 158*0.3, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect(data.fuelRange+'km/'+data.fuelLevel+'%', new Rect(378 * 0.053, 158*0.3, 378 * 0.24, 158/2));
canvas.drawImageInRect(await this.tyyy('gauge','#' + ttbb), new Rect(378*0.28, 158*0.3, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect(data.gl+'km/'+data.sj+'分', new Rect(378 * 0.333, 158*0.3, 378 * 0.27, 158/2));



canvas.drawImageInRect(await this.tyyy('car','#' + ttbb), new Rect(0, 158*0.45, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect(data.mileage+'km', new Rect(378 * 0.053, 158*0.45, 378 * 0.24, 158/2));
canvas.drawImageInRect(await this.tyyy('car.circle','#' + ttbb), new Rect(378*0.28, 158*0.45, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect(data.yh+' L', new Rect(378 * 0.333, 158*0.45, 378 * 0.24, 158/2));


canvas.drawImageInRect(await this.tyyy('lock.rotation','#' + ttbb), new Rect(0, 158*0.6, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect(`${this.formatDate(data.updateTimeStamp, 'MM-dd HH:mm')}`, new Rect(378 * 0.053, 158*0.6, 378 * 0.24, 158/2));
canvas.drawImageInRect(await this.tyyy('gearshape.circle','#' + ttbb), new Rect(378*0.28, 158*0.6, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect(data.sd+'km/h', new Rect(378 * 0.333, 158*0.6, 378 * 0.24, 158/2));

canvas.drawImageInRect(await this.tyyy('clock.arrow.2.circlepath','#' + ttbb), new Rect(0, 158*0.75, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect('数据更新于'+this.formatDate(new Date().valueOf(), 'MM-dd HH:mm'), new Rect(378 * 0.05, 158*0.75, 378 * 0.5, 158/2));
// canvas.drawImageInRect(await this.getSFSymbolImage('fuelpump'), new Rect(378*0.25, 158*0.75, 378 * 0.045, 378 * 0.045))
// canvas.drawTextInRect('18km/L', new Rect(378 * 0.3, 158*0.75, 378 * 0.24, 158/2));

const doorStatus = data.doorStatus;
const windowStatus = data.windowStatus;
const doorAndWindowNormal = doorStatus.concat(windowStatus).length !== 0;
canvas.setFont(Font.systemFont(14))
canvas.setTextAlignedRight()
if(data.isLocked){
canvas.drawImageInRect(await this.drawTableIcon('lock.shield.fill',50,205,50), new Rect(378*0.8, 158*0.17, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect('车辆锁定', new Rect(378 * 0.82, 158*0.17, 378 * 0.18, 158/2));
}else{
canvas.drawImageInRect(await this.drawTableIcon('lock.shield.fill',255,0,0), new Rect(378*0.8, 158*0.17, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect('没有锁车', new Rect(378 * 0.82, 158*0.17, 378 * 0.18, 158/2)); 
await this.notify('注意！车辆没有锁车');
}
if(!doorAndWindowNormal){
canvas.drawImageInRect(await this.drawTableIcon('lock.shield.fill',50,205,50), new Rect(378*0.57, 158*0.17, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect('门窗关闭', new Rect(378 * 0.59, 158*0.17, 378 * 0.18, 158/2));
}else{
canvas.drawImageInRect(await this.drawTableIcon('lock.shield.fill',255,165,0), new Rect(378*0.57, 158*0.17, 378 * 0.05, 378 * 0.05))
canvas.drawTextInRect('未关门窗', new Rect(378 * 0.59, 158*0.17, 378 * 0.18, 158/2));
await this.notify('注意！车辆门窗没有关好');
}
canvas.setFont(Font.systemFont(12))
canvas.setTextAlignedLeft()
canvas.drawImageInRect(await this.drawTableIcon('location',50,205,50), new Rect(0, 158*0.9, 378 * 0.045, 378 * 0.045))
canvas.drawTextInRect(data.completeAddress, new Rect(378 * 0.05, 158*0.9, 378*0.8, 158/2));
canvas.setTextAlignedRight()
canvas.drawTextInRect(data.outdoorTemperature+'℃' + (data.showqd?data.qdsj?' '+data.jf+'积分':'+KEY失效':''), new Rect(0, 158*0.9, 378, 158/2));
if(data.carPlateNo){
if(data.showPlate){
canvas.setFont(Font.systemFont(16))
// canvas.drawImageInRect(await this.getSFSymbolImage('fuelpump'), new Rect(0, 158*0.9, 378 * 0.045, 378 * 0.045))
canvas.drawTextInRect(data.carPlateNo, new Rect(378 * 0.6, 2, 120, 33));
}
if (!data.showPlate) {
  const y = 3;
const c = 83
const k = 23
const xx = 378 * 0.7
//       ctx.opaque = false;
//       ctx.respectScreenScale = true;
//       ctx.size = size;
      const path = new Path();
      const rect = new Rect(xx, 0, c, k);
      path.addRoundedRect(rect, y, y);
      path.closeSubpath();
      canvas.setFillColor(Color.blue());
      canvas.addPath(path);
      canvas.fillPath();
      
      
      const path1 = new Path();
      const rect1 = new Rect(xx+2, 2, c-4, k-4);
      path1.addRoundedRect(rect1, y, y);
      path1.closeSubpath();
      canvas.setFillColor(Color.white());
      canvas.addPath(path1);      
      canvas.fillPath();
      
      
      const path2 = new Path();
      const rect2 = new Rect(xx+3, 3, c-6, k-6);
      path2.addRoundedRect(rect2, y, y);
      path2.closeSubpath();
      canvas.setFillColor(Color.blue());
      canvas.addPath(path2);      
      canvas.fillPath();
      
canvas.setFont(Font.systemFont(14))
canvas.setTextAlignedCenter()
canvas.setTextColor(Color.white())
canvas.drawTextInRect(data.carPlateNo, new Rect(xx+3,3, c-6, k-6));
//   canvas.setFillColor(Color.white());
//   canvas.fillEllipse(new Rect(xx+c*0.25, 1, 4, 4))
//   canvas.fillEllipse(new Rect(xx+c*0.75, 1, 4, 4))
//   canvas.fillEllipse(new Rect(xx+c*0.25, k-4, 4, 4))
//   canvas.fillEllipse(new Rect(xx+c*0.75, k-4, 4, 4))
  
}
}
// canvas.setFillColor(Color.blue())
// canvas.fillRect(new Rect(373, 156, 40, 40))



//                         console.warn(canvas);
               const eee = await canvas.getImage();
  // 创建一个显示元素列表的小部件
  // 显示元素列表的小部件。将小部件传递给 Script.setWidget() 将其显示在您的主屏幕上。
  // 请注意，小部件会定期刷新，小部件刷新的速率很大程度上取决于操作系统。
  // 另请注意，在小部件中运行脚本时存在内存限制。当使用太多内存时，小部件将崩溃并且无法正确呈现。

  const eq = ee.addStack();
  // 添加文本物件
//   const text = ee.addImage(Image.fromData(imageData));
//   const text = ee.addText(String(time));
  const gg = eq.addImage(eee);
  // 设置字体颜色
//   text.textColor = new Color("#000000");
  // 设置字体大小
//   text.font = Font.boldSystemFont(36);
  // 设置文字对齐方式
//   text.centerAlignText();
  // 新建线性渐变物件
  // 设置部件
  return ee;
    }





  /**
   * 渲染小尺寸组件
   * @param data
   * @returns {Promise<ListWidget>}
   */
  async renderSmall(data) {
    try {
      const widget = new ListWidget();
      await this.setWidgetDynamicBackground(widget, 'Small');
      widget.setPadding(0, 0, 0, 0);

      const doorStatus = data.doorStatus;
      const windowStatus = data.windowStatus;
      const doorAndWindowNormal = doorStatus.concat(windowStatus).length !== 0;
      const isLocked = data.isLocked;

      const containerStack = this.addStackTo(widget, 'vertical');
      // 续航/燃料信息
       const carInfoStack = this.addStackTo(containerStack, 'horizontal');
      
//       carInfoStack.centerAlignContent();
      const carInfoTextStack = this.addStackTo(carInfoStack, 'horizontal');
       carInfoTextStack.centerAlignContent();
        const carInfoImage = carInfoTextStack.addImage(await this.tyyy('fuelpump.fill','#ffffff'));
        carInfoImage.imageSize = new Size(15, 15);
        carInfoImage.tintColor = Color.white()
//         carInfoTextStack.setPadding(0, 0, 5, 0);
      const enduranceText = carInfoTextStack.addText(`${data.fuelRange}km`);

       this.setFontFamilyStyle(enduranceText, 14, 'bold');
//       this.setWidgetNodeColor(enduranceText, 'textColor');
//       enduranceText.textColor = Color.white();
      if (
        data.fuelLevel && data.fuelLevel <= 20 ||
        data.socLevel && data.socLevel <= 20
      ) {
        enduranceText.textColor = this.dangerColor();
      }
      if (data.fuelLevel) {
        carInfoTextStack.spacing = 4;
        const fuelStack = this.addStackTo(carInfoTextStack, 'horizontal');
        fuelStack.setPadding(0, 0, 0, 0);
        const fuelText = fuelStack.addText(`${data.fuelLevel}%`);
//         fuelStack.addSpacer();
//         const logoStack = this.addStackTo(carInfoTextStack, 'vertical');
//       logoStack.centerAlignContent();
//       const carLogo = await this.getMyCarLogo(this.myCarLogoUrl);
//       const carLogoImage = logoStack.addImage(carLogo);
//       carLogoImage.tintColor = Color.white()
//       carLogoImage.imageSize = new Size(14, 14);
        this.setFontFamilyStyle(fuelText, 14, 'bold');
//      this.setWidgetNodeColor(fuelText, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText.textColor = this.dangerColor();
        }
      }
      if (data.socLevel) {
        carInfoTextStack.spacing = 4;
        const fuelStack = this.addStackTo(carInfoTextStack, 'horizontal');
        fuelStack.setPadding(0, 0, data.fuelLevel ? 3 : 2, 0);
        const fuelText = fuelStack.addText(data.socLevel + '%');
        this.setFontFamilyStyle(fuelText, data.fuelLevel ? 10 : 12, 'regular');
//         this.setWidgetNodeColor(fuelText, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText.textColor = this.dangerColor();
        }
      }
       carInfoStack.addSpacer();
       containerStack.spacing = 0;
      
      const statusMainStack = this.addStackTo(containerStack, 'horizontal');
//       statusMainStack.addSpacer();
      const statusStack = this.addStackTo(statusMainStack, 'horizontal');
      statusStack.centerAlignContent();
      let icon = await this.getSFSymbolImage('lock.fill');
      if (doorAndWindowNormal) icon = await this.getSFSymbolImage('exclamationmark.shield.fill');
      if (!isLocked) icon = await this.tyyy('lock.open.fill','#ffffff');
      const statusImage = statusStack.addImage(icon);
      statusImage.imageSize = new Size(14, 14);
      statusImage.tintColor = Color.white()
//       if (this.getLockSuccessStyle()) statusImage.tintColor = this.successColor();
//       else this.setWidgetNodeColor(statusImage, 'tintColor');
//       if (doorAndWindowNormal) statusImage.tintColor = this.warningColor();
//       if (!isLocked) statusImage.tintColor = this.dangerColor();
      statusStack.spacing = 4;

      const infoStack = this.addStackTo(statusStack, 'horizontal');
       let status = '车辆已锁定';
      if (doorAndWindowNormal) status = '门窗未锁定';
      if (!isLocked) status = '未锁车';
//       infoStack.setPadding(0, 0, 2, 0);
      const statusText = infoStack.addText(status);
      this.setFontFamilyStyle(statusText, 14, 'bold');
      statusMainStack.addSpacer();
//       if (this.getLockSuccessStyle()) statusText.textColor = this.successColor();
//       else this.setWidgetNodeColor(statusText, 'textColor');
//       if (doorAndWindowNormal) statusText.textColor = this.warningColor();
//       if (!isLocked) statusText.textColor = this.dangerColor();
      
      /*
      const carPhotoStack = this.addStackTo(containerStack, 'horizontal');
      carPhotoStack.addSpacer();
      const carPhoto = await this.getMyCarPhoto(this.myCarPhotoUrl);
      const inContainerImage = carPhotoStack.addImage(carPhoto);
      carPhotoStack.addSpacer();
      inContainerImage.centerAlignImage();
*/
       const updateTimeStack = this.addStackTo(containerStack, 'horizontal');
       updateTimeStack.centerAlignContent();

        const carInfoImage1 = updateTimeStack.addImage(await this.tyyy('clock.arrow.2.circlepath','#ffffff'));
        carInfoImage1.imageSize = new Size(16, 16);
//         carInfoImage1.tintColor = Color.gray()
      const updateTimeText = updateTimeStack.addText(`上锁:${this.formatDate(data.updateTimeStamp, 'MM-dd HH:mm')}`);
      updateTimeText.spacing = 4;
      this.setFontFamilyStyle(updateTimeText, 14, 'bold');
      updateTimeText.textColor=Color.gray()
//       this.setWidgetNodeColor(updateTimeText, 'textColor');
//       updateTimeStack.addSpacer();
      
//       statusStack.centerAlignContent();
//        statusStack.setPadding(5, 10, 5, 10);
//       statusStack.cornerRadius = 10;
//       statusStack.borderWidth = 2;
//       if (this.getLockSuccessStyle()) statusStack.backgroundColor = this.successColor(0.25);
//       else this.setWidgetNodeColor(statusStack, 'backgroundColor', 0.25);
//       if (doorAndWindowNormal) statusStack.backgroundColor = this.warningColor(0.25);
//       if (!isLocked) statusStack.backgroundColor = this.dangerColor(0.25);
//       if (this.getLockSuccessStyle()) statusStack.borderColor = this.successColor(0.5);
//       else this.setWidgetNodeColor(statusStack, 'borderColor', 0.5);
//       if (doorAndWindowNormal) statusStack.borderColor = this.warningColor(0.5);
//       if (!isLocked) statusStack.borderColor = this.dangerColor(0.5);
// 
      

      return widget
    } catch (error) {
      await this.writeErrorLog(data, error);
      throw error
    }
  }

  /**
   * 渲染中尺寸组件
   * @param data
   * @returns {Promise<ListWidget>}
   */
  async renderMedium1(data) {
    try {
      const widget = new ListWidget();
      await this.setWidgetDynamicBackground(widget, 'Medium');
      widget.setPadding(10, 13, 10, 13);
      // region logoStack
      const rowHeader = this.addStackTo(widget, 'horizontal');
      rowHeader.setPadding(0, 0, 0, 0);
      rowHeader.topAlignContent();
      // 车辆名称
      const nameStack = this.addStackTo(rowHeader, 'vertical');
      const carText = nameStack.addText(data.seriesName);
      this.setFontFamilyStyle(carText, 18, 'bold');
      this.setWidgetNodeColor(carText, 'textColor');
      // 2.0 140KW B9 40TFSI S-line
      const powerText = nameStack.addText(data.carModelName);
        powerText.minimumScaleFactor = 0.8;
      this.setFontFamilyStyle(powerText, 10, 'regular');
      this.setWidgetNodeColor(powerText, 'textColor');
      rowHeader.addSpacer();
      const headerRightStack = this.addStackTo(rowHeader, 'vertical');
      headerRightStack.centerAlignContent();
      const baseInfoStack = this.addStackTo(headerRightStack, 'horizontal');
      baseInfoStack.addSpacer();
      baseInfoStack.setPadding(4, 0, 0, 0);
      baseInfoStack.centerAlignContent();
      // 车牌显示
const plateNoStack3 = this.addStackTo(baseInfoStack, 'vertical');
const plateNoText3 = plateNoStack3.addText(this.formatDate(new Date().valueOf(), 'HH:mm') + ' 刷新');
       plateNoText3.minimumScaleFactor = 0.7;
         this.setFontFamilyStyle(plateNoText3, 12);
        this.setWidgetNodeColor(plateNoText3, 'textColor');
        baseInfoStack.spacing = 5;
        if(data.carPlateNo){
        if (!data.showPlate) {
        const plateNoStack1 = this.addStackTo(baseInfoStack, 'vertical');
        plateNoStack1.setPadding(1,1,1, 1);
        plateNoStack1.backgroundColor = this.carpz();
        plateNoStack1.cornerRadius = 2;
        // 牌照边框  
        const plateNoStack = this.addStackTo(plateNoStack1, 'vertical');
        plateNoStack.centerAlignContent();
        plateNoStack.setPadding(0, 2,0, 2);
        plateNoStack.borderColor = this.carbk();
//         plateNoStack.size = new Size(0, 0);
        plateNoStack.backgroundColor = this.carpz();
        plateNoStack.cornerRadius = 2;
        plateNoStack.borderWidth = 2;
        
        plateNoStack.centerAlignContent();
        const plateNoText = plateNoStack.addText(data.carPlateNo);
        this.setFontFamilyStyle(plateNoText, 10, 'bold');
        this.setWidgetNodeColor(plateNoText, 'textColor');
        plateNoText.minimumScaleFactor = 0.7;
        plateNoText.textColor = this.carbk();
        baseInfoStack.spacing = 5;
      } else{
        const plateNoStack = this.addStackTo(baseInfoStack, 'vertical');
        plateNoStack.centerAlignContent();
        const plateNoText = plateNoStack.addText(data.carPlateNo);
        this.setFontFamilyStyle(plateNoText, 12, 'regular');
        this.setWidgetNodeColor(plateNoText, 'textColor');
        plateNoText.minimumScaleFactor = 0.7;
//         plateNoText.textColor = this.carbk();
        baseInfoStack.spacing = 5;
      }
      }
      const logoStack = this.addStackTo(baseInfoStack, 'vertical');
      logoStack.centerAlignContent();
      const carLogo = await this.getMyCarLogo(this.myCarLogoUrl);
      const carLogoImage = logoStack.addImage(carLogo);
      carLogoImage.imageSize = new Size(this.getLogoSize('width'), this.getLogoSize('height'));
      if (this.getLogoHasTint()) this.setWidgetNodeColor(carLogoImage, 'tintColor');

      headerRightStack.spacing = 4;
      const statusStack = this.addStackTo(headerRightStack, 'horizontal');
      statusStack.centerAlignContent();
      statusStack.addSpacer();
      const carLockStack = this.addStackTo(statusStack, 'horizontal');
      carLockStack.centerAlignContent();
      // 门窗状态
      const doorStatus = data.doorStatus;
      const windowStatus = data.windowStatus;
      const doorAndWindowNormal = doorStatus.concat(windowStatus).length !== 0;
      // const doorAndWindowNormal = true
      if (doorAndWindowNormal) {
        const carInfoText = carLockStack.addText('门窗未关');
        this.setFontFamilyStyle(carInfoText, 12);
        this.setWidgetNodeColor(carInfoText, 'textColor');
        const carDoorImage = carLockStack.addImage(await this.getSFSymbolImage('xmark.shield.fill'));
        carDoorImage.imageSize = new Size(14, 14);
        carDoorImage.tintColor = this.warningColor();
        await this.notify('车门窗没有关好');
      }else{
        const carInfoText = carLockStack.addText('门窗关闭');
        carInfoText.minimumScaleFactor = 0.7;
        this.setFontFamilyStyle(carInfoText, 12);
        this.setWidgetNodeColor(carInfoText, 'textColor');
        const carDoorImage = carLockStack.addImage(await this.getSFSymbolImage('lock.shield.fill'));
        carDoorImage.imageSize = new Size(14, 14);
        carDoorImage.tintColor = this.successColor();
      }
      
      carLockStack.spacing = 5;
      // 锁车状态
      if (data.isLocked) {
        const carInfoText = carLockStack.addText('车辆已锁');
        carInfoText.minimumScaleFactor = 0.7;
        this.setFontFamilyStyle(carInfoText, 12);
        this.setWidgetNodeColor(carInfoText, 'textColor');
      } else {
        const carInfoText = carLockStack.addText('未锁车');
        carInfoText.minimumScaleFactor = 0.7;
        this.setFontFamilyStyle(carInfoText, 12);
        this.setWidgetNodeColor(carInfoText, 'textColor');
      }
      const carLockImage = carLockStack.addImage(await this.getSFSymbolImage('lock.shield.fill'));
      carLockImage.imageSize = new Size(14, 14);
      carLockImage.tintColor = data.isLocked ? this.successColor():  this.dangerColor();
      data.isLocked ? this.successColor():await this.notify('没有锁车');
      
       
      // endregion
      // region mainStack
      const mainStack = this.addStackTo(widget, 'horizontal');
      mainStack.setPadding(5, 0, 0, 0);
      mainStack.centerAlignContent();
       //mainStack.borderColor = this.carbk();
       //mainStack.cornerRadius = 3;
       //mainStack.borderWidth = 2;
      // region 状态信息展示
      const rowLeftStack = this.addStackTo(mainStack, 'vertical');
      //rowLeftStack.borderColor = this.carbk();
      //rowLeftStack.cornerRadius = 3;
      //rowLeftStack.borderWidth = 2;
      // 续航/燃料信息
      const carInfoStack = this.addStackTo(rowLeftStack, 'horizontal');
      carInfoStack.centerAlignContent();
      const carInfoImageStack = this.addStackTo(carInfoStack, 'vertical');
      carInfoImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const carInfoText = carInfoImageStack.addText('续航:');
        this.setFontFamilyStyle(carInfoText, 12);
        this.setWidgetNodeColor(carInfoText, 'textColor');
      } else {
        const carInfoImage = carInfoImageStack.addImage(await this.getSFSymbolImage('fuelpump'));
        carInfoImage.imageSize = new Size(14, 14);
        this.setWidgetNodeColor(carInfoImage, 'tintColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          carInfoImage.tintColor = this.dangerColor();
        }
      }
      carInfoStack.addSpacer(5);
      const carInfoTextStack = this.addStackTo(carInfoStack, 'horizontal');
      carInfoTextStack.bottomAlignContent();
      const enduranceText = carInfoTextStack.addText(`${data.fuelRange}Km`);
      this.setFontFamilyStyle(enduranceText, 11, 'bold');
      this.setWidgetNodeColor(enduranceText, 'textColor');
      if (
        data.fuelLevel && data.fuelLevel <= 20 ||
        data.socLevel && data.socLevel <= 20
      ) {
        enduranceText.textColor = this.dangerColor();
      }
      if (data.fuelLevel) {
        carInfoTextStack.spacing = 2;
        const fuelStack = this.addStackTo(carInfoStack, 'horizontal');
        fuelStack.bottomAlignContent();
        fuelStack.setPadding(0, 0, 0, 0);
        const fuelText = fuelStack.addText(` ${data.fuelLevel}%`);
        this.setFontFamilyStyle(fuelText, 12, 'regular');
        this.setWidgetNodeColor(fuelText, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText.textColor = this.dangerColor();
        }
      }
      if (data.socLevel) {
        //carInfoTextStack.spacing = 2;
        const fuelStack = this.addStackTo(carInfoTextStack, 'horizontal');
        fuelStack.setPadding(0, 0, 0, 0);
        const fuelText = fuelStack.addText(data.socLevel + '%');
        this.setFontFamilyStyle(fuelText, data.fuelLevel ? 8 : 12, 'regular');
        this.setWidgetNodeColor(fuelText, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText.textColor = this.dangerColor();
        }
      }

      rowLeftStack.spacing = 2;
      // 总里程
      const mileageStack = this.addStackTo(rowLeftStack, 'horizontal');
      mileageStack.setPadding(0, 0, 0, 0);
      mileageStack.centerAlignContent();
      const mileageImageStack = this.addStackTo(mileageStack, 'vertical');
      mileageImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const mileageText = mileageImageStack.addText('里程:');
        this.setFontFamilyStyle(mileageText, 12);
        this.setWidgetNodeColor(mileageText, 'textColor');
      } else {
        const mileageImage = mileageImageStack.addImage(await this.getSFSymbolImage('car'));
        mileageImage.imageSize = new Size(14, 14);
        this.setWidgetNodeColor(mileageImage, 'tintColor');
      }
      mileageStack.addSpacer(5);
      const mileageTextStack = this.addStackTo(mileageStack, 'horizontal');
      mileageTextStack.setPadding(0, 0, 0, 0);
      mileageTextStack.bottomAlignContent();
      const mileageText = mileageTextStack.addText(data.mileage + 'km');
      this.setFontFamilyStyle(mileageText, 11, 'bold');
      this.setWidgetNodeColor(mileageText, 'textColor');

      rowLeftStack.spacing =2;


      const dateTimeStack1 = this.addStackTo(rowLeftStack, 'horizontal');
      dateTimeStack1.centerAlignContent();
      const dateTimeImageStack1 = this.addStackTo(dateTimeStack1, 'vertical');
      dateTimeImageStack1.bottomAlignContent();
      if (this.settings['showType']) {
        const dateTimeText = dateTimeImageStack1.addText('驻车:');
        this.setFontFamilyStyle(dateTimeText, 12);
        this.setWidgetNodeColor(dateTimeText, 'textColor');
      } else {
      const dateTimeImage1 = dateTimeImageStack1.addImage(await this.getSFSymbolImage('hand.tap'));
      dateTimeImage1.imageSize = new Size(15, 15);
      this.setWidgetNodeColor(dateTimeImage1, 'tintColor');
     }
      dateTimeStack1.addSpacer(5);
      const dateTimeTextStack1 = this.addStackTo(dateTimeStack1, 'horizontal');
      dateTimeTextStack1.bottomAlignContent();
      const dateTimeText1 = dateTimeTextStack1.addText(data.parkingBrakeActive ? '手刹已激活' :'未拉手刹');
      dateTimeTextStack1.setPadding(0, 0, 0, 0);
      this.setFontFamilyStyle(dateTimeText1, 11, 'bold');
      this.setWidgetNodeColor(dateTimeText1, 'textColor');


      rowLeftStack.spacing =2;



      // 更新日期
      const dateTimeStack = this.addStackTo(rowLeftStack, 'horizontal');
      dateTimeStack.centerAlignContent();
      const dateTimeImageStack = this.addStackTo(dateTimeStack, 'vertical');
      dateTimeImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const dateTimeText = dateTimeImageStack.addText('更新:');
        this.setFontFamilyStyle(dateTimeText, 12);
        this.setWidgetNodeColor(dateTimeText, 'textColor');
      } else {
        const dateTimeImage = dateTimeImageStack.addImage(await this.getSFSymbolImage('arrow.clockwise.icloud'));
        dateTimeImage.imageSize = new Size(15, 15);
        this.setWidgetNodeColor(dateTimeImage, 'tintColor');
      }
      dateTimeStack.addSpacer(5);
      const dateTimeTextStack = this.addStackTo(dateTimeStack, 'horizontal');
      dateTimeTextStack.bottomAlignContent();
      const dateTimeText = dateTimeTextStack.addText(this.formatDate(data.updateTimeStamp, 'MM-dd HH:mm'));
      dateTimeTextStack.setPadding(0, 0, 0, 0);
      this.setFontFamilyStyle(dateTimeText, 11, 'bold');
      this.setWidgetNodeColor(dateTimeText, 'textColor');



    //第二行

      const rowLeftStack11 = this.addStackTo(mainStack, 'vertical');
      rowLeftStack11.setPadding(0, 5, 0, 0);
      //rowLeftStack11.borderColor = this.carbk();
      //rowLeftStack11.cornerRadius = 3;
      //rowLeftStack11.borderWidth = 2;
      // 续航/燃料信息1
      const carInfoStack11 = this.addStackTo(rowLeftStack11, 'horizontal');
      carInfoStack11.centerAlignContent();
      const carInfoImageStack11 = this.addStackTo(carInfoStack11, 'vertical');
      carInfoImageStack11.bottomAlignContent();

        const carInfoImage11 = carInfoImageStack11.addImage(await this.getSFSymbolImage('gauge'));
        carInfoImage11.imageSize = new Size(14, 14);
        this.setWidgetNodeColor(carInfoImage11, 'tintColor');


      carInfoStack11.addSpacer(5);
      const carInfoTextStack11 = this.addStackTo(carInfoStack11, 'horizontal');
      carInfoTextStack11.bottomAlignContent();
      const enduranceText11 = carInfoTextStack11.addText(`${data.yh} L`);
      this.setFontFamilyStyle(enduranceText11, 11, 'bold');
      this.setWidgetNodeColor(enduranceText11, 'textColor');

      rowLeftStack11.spacing = 2;


      // 续航/燃料信息2
      const carInfoStack22 = this.addStackTo(rowLeftStack11, 'horizontal');
      carInfoStack22.centerAlignContent();
      const carInfoImageStack22 = this.addStackTo(carInfoStack22, 'vertical');
      carInfoImageStack22.bottomAlignContent();

        const carInfoImage22 = carInfoImageStack22.addImage(await this.getSFSymbolImage('car.circle'));
        carInfoImage22.imageSize = new Size(14, 14);
        this.setWidgetNodeColor(carInfoImage22, 'tintColor');


      carInfoStack22.addSpacer(5);
      const carInfoTextStack22 = this.addStackTo(carInfoStack22, 'horizontal');
      carInfoTextStack22.bottomAlignContent();
      const enduranceText22 = carInfoTextStack22.addText(`${data.gl} Km`);
      this.setFontFamilyStyle(enduranceText22, 11, 'bold');
      this.setWidgetNodeColor(enduranceText22, 'textColor');


      rowLeftStack11.spacing = 2;

      // 续航/燃料信息3
      const carInfoStack33 = this.addStackTo(rowLeftStack11, 'horizontal');
      carInfoStack33.centerAlignContent();
      const carInfoImageStack33 = this.addStackTo(carInfoStack33, 'vertical');
      carInfoImageStack33.bottomAlignContent();

        const carInfoImage33 = carInfoImageStack33.addImage(await this.getSFSymbolImage('gearshape.circle'));
        carInfoImage33.imageSize = new Size(14, 14);
        this.setWidgetNodeColor(carInfoImage33, 'tintColor');


      carInfoStack33.addSpacer(5);
      const carInfoTextStack33 = this.addStackTo(carInfoStack33, 'horizontal');
      carInfoTextStack33.bottomAlignContent();
      const enduranceText33 = carInfoTextStack33.addText(`${data.sd} Km/h`);
      this.setFontFamilyStyle(enduranceText33, 11, 'bold');
      this.setWidgetNodeColor(enduranceText33, 'textColor');
      rowLeftStack11.spacing = 2;
      // 续航/燃料信息4
      const carInfoStack44 = this.addStackTo(rowLeftStack11, 'horizontal');
      carInfoStack44.centerAlignContent();
      const carInfoImageStack44 = this.addStackTo(carInfoStack44, 'vertical');
      carInfoImageStack44.bottomAlignContent();

        const carInfoImage44 = carInfoImageStack44.addImage(await this.getSFSymbolImage('stopwatch'));
        carInfoImage44.imageSize = new Size(14, 14);
        this.setWidgetNodeColor(carInfoImage44, 'tintColor');


      carInfoStack44.addSpacer(5);
      const carInfoTextStack44 = this.addStackTo(carInfoStack44, 'horizontal');
      carInfoTextStack44.bottomAlignContent();
      const enduranceText44 = carInfoTextStack44.addText(`${data.sj} min`);
      this.setFontFamilyStyle(enduranceText44, 11, 'bold');
      this.setWidgetNodeColor(enduranceText44, 'textColor');



      // endregion
      mainStack.addSpacer();
      // region 右侧车辆图片
      const rowRightStack = this.addStackTo(mainStack, 'vertical');

      rowRightStack.setPadding(0, 5, 0, 0);
      //rowRightStack.borderColor = this.carbk();
      //rowRightStack.cornerRadius = 3;
      //rowRightStack.borderWidth = 2;
      const carPhoto = await this.getMyCarPhoto(this.settings['myCarPhotoUrl']);
      const carPhotoStack = rowRightStack.addImage(carPhoto);
      carPhotoStack.centerAlignImage();
      // endregion
      // endregion
      const footTextData = data.showLocation ? data.showLocationFormat ? data.customAddress : data.completeAddress : data.myOne;
      const footerStack = this.addStackTo(widget, 'horizontal');

      //footerStack.borderColor = this.carbk();
      //footerStack.cornerRadius = 3;
      //footerStack.borderWidth = 2;
      footerStack.setPadding(5, 0, 0, 0);

      const footerStack11 = footerStack.addImage(await this.getSFSymbolImage('location'));
      footerStack11.imageSize = new Size(11, 11);
      footerStack11.tintColor = this.successColor();
      footerStack.addSpacer(3);
      const footerText = footerStack.addText(footTextData);
      data.showLocation?footerText.url = `iosamap://viewMap?sourceApplication=VW&poiname=车辆位置&lat=${data.latitude}&lon=${data.longitude}&dev=0`:'';
      footerText.minimumScaleFactor = 0.7;
      this.setFontFamilyStyle(footerText, 11, 'regular');
      this.setWidgetNodeColor(footerText, 'textColor');
      footerStack.addSpacer();
      const footerStack22 = footerStack.addImage(await this.getSFSymbolImage('thermometer'));
      footerStack22.imageSize = new Size(11, 11);
      footerStack22.tintColor = this.successColor();
      const footerText1 = footerStack.addText(data.outdoorTemperature+'°C');
      this.setFontFamilyStyle(footerText1, 11, 'regular');
      this.setWidgetNodeColor(footerText1, 'textColor');
      if (data.showqd) {
        console.log(data.qdxx)
        if(data.qdxx != '此手机号今天已经签到' && data.qdsj){
        await this.notify('大众微店签到成功');
        }
        footerStack.addSpacer(3);
        const footerStack33 = footerStack.addImage(await this.getSFSymbolImage('gift'));
        footerStack33.imageSize = new Size(11, 11);
        data.qdsj ? footerStack33.tintColor = this.successColor():footerStack33.tintColor = this.dangerColor();
     footerStack.addSpacer(3);
    const footerText2 = footerStack.addText(data.jf?data.jf+'分':'KEY失效');
    footerText2.minimumScaleFactor = 0.7;
      this.setFontFamilyStyle(footerText2, 11, 'regular');
      this.setWidgetNodeColor(footerText2, 'textColor');
       } 

      return widget
    } catch (error) {
      await this.writeErrorLog(data, error);
      throw error
    }
  }

  /**
   * 渲染大尺寸组件
   * @param data
   * @returns {Promise<ListWidget>}
   */
  async renderLarge(data) {
    try {
      const widget = new ListWidget();
      await this.setWidgetDynamicBackground(widget, 'Large');

      widget.setPadding(15, 15, 15, 15);
      // region headerStack
      const rowHeader = this.addStackTo(widget, 'horizontal');
      rowHeader.setPadding(0, 0, 10, 0);
      rowHeader.topAlignContent();
      // 顶部左侧
      const headerLeftStack = this.addStackTo(rowHeader, 'vertical');
      // 车辆名称
      const nameStack = this.addStackTo(headerLeftStack, 'vertical');
      const carText = nameStack.addText(data.seriesName);
      this.setFontFamilyStyle(carText, 22, 'bold');
      this.setWidgetNodeColor(carText, 'textColor');
      carText.minimumScaleFactor = 0.7;
      // 功率显示
      const powerStack = this.addStackTo(headerLeftStack, 'vertical');
      const powerText = powerStack.addText(data.carModelName);
      this.setFontFamilyStyle(powerText, 14, 'regular');
      this.setWidgetNodeColor(powerText, 'textColor');
      powerText.minimumScaleFactor = 0.7;
      // 俩侧分割
      rowHeader.addSpacer();
      // 顶部右侧
      const logoWidth = this.getLogoSize('width') * 1.5;
      const logoHeight = this.getLogoSize('height') * 1.5;
      const headerRightStackWidth = data.showPlate ? data.carPlateNo ? data.carPlateNo.length * 12 : logoWidth : logoWidth + 10;
      const headerRightStackHeight = data.showPlate ? logoHeight + 25 : logoHeight;
      const headerRightStack = this.addStackTo(rowHeader, 'vertical');
      headerRightStack.size = new Size(headerRightStackWidth, headerRightStackHeight);
      // Logo
      const carLogoStack = this.addStackTo(headerRightStack, 'horizontal');
      carLogoStack.addText('');
      carLogoStack.addSpacer();
      const carLogo = await this.getMyCarLogo(this.myCarLogoUrl);
      const carLogoImage = carLogoStack.addImage(carLogo);
      carLogoImage.imageSize = new Size(logoWidth, logoHeight);
      if (this.getLogoHasTint()) this.setWidgetNodeColor(carLogoImage, 'tintColor');
      headerRightStack.spacing = 5;
      // 车牌信息
      if (data.showPlate) {
        const plateNoStack = this.addStackTo(headerRightStack, 'horizontal');
        plateNoStack.addText('');
        plateNoStack.addSpacer();
        const plateNoText = plateNoStack.addText(data.carPlateNo);
        this.setFontFamilyStyle(plateNoText, 12, 'bold');
        this.setWidgetNodeColor(plateNoText, 'textColor');
      }
      // endregion
      // region mainStack
      const mainStack = this.addStackTo(widget, 'horizontal');
      mainStack.centerAlignContent();
      mainStack.setPadding(0, 0, 0, 0);
      // region 状态信息展示
      const rowLeftStack = this.addStackTo(mainStack, 'vertical');
      // region 续航里程
      const enduranceStack = this.addStackTo(rowLeftStack, 'horizontal');
      enduranceStack.bottomAlignContent();
      const enduranceImageStack = this.addStackTo(enduranceStack, 'vertical');
      enduranceImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const enduranceText = enduranceImageStack.addText('续航里程:');
        this.setFontFamilyStyle(enduranceText, 14);
        this.setWidgetNodeColor(enduranceText, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          enduranceText.textColor = this.dangerColor();
        }
      } else {
        const enduranceImage = enduranceImageStack.addImage(await this.getSFSymbolImage('flag.circle'));
        enduranceImage.imageSize = new Size(18, 18);
        this.setWidgetNodeColor(enduranceImage, 'tintColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          enduranceImage.tintColor = this.dangerColor();
        }
      }
      enduranceStack.addSpacer(5);
      const enduranceTextStack = this.addStackTo(enduranceStack, 'horizontal');
      enduranceTextStack.bottomAlignContent();
      const enduranceText = enduranceTextStack.addText(data.fuelRange + 'km');
      this.setFontFamilyStyle(enduranceText, 14, 'bold');
      this.setWidgetNodeColor(enduranceText, 'textColor');
      if (data.fuelLevel && data.fuelLevel <= 20 || data.socLevel && data.socLevel <= 20) {
        enduranceText.textColor = this.dangerColor();
      }
      // endregion
      rowLeftStack.addSpacer(5);
      // region 燃料信息
      const fuelStack = this.addStackTo(rowLeftStack, 'horizontal');
      fuelStack.bottomAlignContent();
      const fuelImageStack = this.addStackTo(fuelStack, 'vertical');
      fuelImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const fuelText = fuelImageStack.addText('燃料剩余:');
        this.setFontFamilyStyle(fuelText, 14);
        this.setWidgetNodeColor(fuelText, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText.textColor = this.dangerColor();
        }
      } else {
        let fuelIcon = 'fuelpump.circle';
        if (data.socLevel) fuelIcon = 'bolt.circle';
        const fuelImage = fuelImageStack.addImage(await this.getSFSymbolImage(fuelIcon));
        fuelImage.imageSize = new Size(18, 18);
        this.setWidgetNodeColor(fuelImage, 'tintColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelImage.tintColor = this.dangerColor();
        }
      }
      fuelStack.addSpacer(5);
      // 汽油
      const fuelTextStack1 = this.addStackTo(fuelStack, 'horizontal');
      fuelTextStack1.bottomAlignContent();
      if (data.fuelLevel) {
        const fuelText1 = fuelTextStack1.addText(data.fuelLevel + '%');
        this.setFontFamilyStyle(fuelText1, 14, 'regular');
        this.setWidgetNodeColor(fuelText1, 'textColor');
        fuelStack.addSpacer(5);
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText1.textColor = this.dangerColor();
        }
      }
      // 电池
      if (data.socLevel) {
        const fuelTextStack2 = this.addStackTo(fuelStack, 'horizontal');
        fuelTextStack2.bottomAlignContent();
        const fuelText2 = fuelTextStack2.addText(data.socLevel + '%');
        this.setFontFamilyStyle(fuelText2, data.fuelLevel ? 12 : 14, 'regular');
        this.setWidgetNodeColor(fuelText2, 'textColor');
        if (
          data.fuelLevel && data.fuelLevel <= 20 ||
          data.socLevel && data.socLevel <= 20
        ) {
          fuelText2.textColor = this.dangerColor();
        }
      }
      // endregion
      rowLeftStack.addSpacer(5);
      // region 总里程
      const mileageStack = this.addStackTo(rowLeftStack, 'horizontal');
      mileageStack.bottomAlignContent();
      const mileageImageStack = this.addStackTo(mileageStack, 'vertical');
      mileageImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const mileageText = mileageImageStack.addText('行程里程:');
        this.setFontFamilyStyle(mileageText, 14);
        this.setWidgetNodeColor(mileageText, 'textColor');
      } else {
        const mileageImage = mileageImageStack.addImage(await this.getSFSymbolImage('car.circle'));
        mileageImage.imageSize = new Size(18, 18);
        this.setWidgetNodeColor(mileageImage, 'tintColor');
      }
      mileageStack.addSpacer(5);
      const mileageTextStack = this.addStackTo(mileageStack, 'horizontal');
      mileageTextStack.bottomAlignContent();
      const mileageText = mileageTextStack.addText(data.mileage + 'km');
      this.setFontFamilyStyle(mileageText, 14, 'regular');
      this.setWidgetNodeColor(mileageText, 'textColor');
      // endregion
      rowLeftStack.addSpacer(5);
      // region 机油数据
      if (data.oilSupport && data.oilLevel !== '0.0') {
        const oilStack = this.addStackTo(rowLeftStack, 'horizontal');
        oilStack.bottomAlignContent();
        const oilImageStack = this.addStackTo(oilStack, 'vertical');
        oilImageStack.bottomAlignContent();
        if (this.settings['showType']) {
          const oilText = oilImageStack.addText('机油剩余:');
          this.setFontFamilyStyle(oilText, 14);
          this.setWidgetNodeColor(oilText, 'textColor');
        } else {
          const oilImage = oilImageStack.addImage(await this.getSFSymbolImage('drop.circle'));
          oilImage.imageSize = new Size(18, 18);
          if (Number(data.oilLevel) <= 12.5) {
            oilImage.tintColor = this.dangerColor();
          } else {
            this.setWidgetNodeColor(oilImage, 'tintColor');
          }
        }
        oilStack.addSpacer(5);
        const oilTextStack = this.addStackTo(oilStack, 'horizontal');
        oilTextStack.bottomAlignContent();
        const oilText = oilTextStack.addText(data.oilLevel + '%');
        this.setFontFamilyStyle(oilText, 14, 'regular');
        if (Number(data.oilLevel) <= 12.5) {
          oilText.textColor = this.dangerColor();
        } else {
          this.setWidgetNodeColor(oilText, 'textColor');
        }
        rowLeftStack.addSpacer(5);
      }
      // endregion
      // region 锁车状态
      const lockedStack = this.addStackTo(rowLeftStack, 'horizontal');
      lockedStack.bottomAlignContent();
      const lockedImageStack = this.addStackTo(lockedStack, 'vertical');
      lockedImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const lockedText = lockedImageStack.addText('车辆状态:');
        this.setFontFamilyStyle(lockedText, 14);
        this.setWidgetNodeColor(lockedText, 'textColor');
      } else {
        const lockedImage = lockedImageStack.addImage(await this.getSFSymbolImage('lock.circle'));
        lockedImage.imageSize = new Size(18, 18);
        if (this.getLockSuccessStyle()) lockedImage.tintColor = this.successColor();
        else this.setWidgetNodeColor(lockedImage, 'tintColor');
        if (!data.isLocked) lockedImage.tintColor = this.dangerColor();
      }
      lockedStack.addSpacer(5);
      const lockedTextStack = this.addStackTo(lockedStack, 'horizontal');
      lockedTextStack.bottomAlignContent();
      const lockedText = lockedTextStack.addText(data.isLocked ? '已锁车' : '未锁车');
      this.setFontFamilyStyle(lockedText, 14, 'regular');
      if (this.getLockSuccessStyle()) lockedText.textColor = this.successColor();
      else this.setWidgetNodeColor(lockedText, 'textColor');
      if (!data.isLocked) lockedText.textColor = this.dangerColor();
      // endregion
      rowLeftStack.addSpacer(5);
      // region 数据更新日期
      const dateTimeStack = this.addStackTo(rowLeftStack, 'horizontal');
      dateTimeStack.bottomAlignContent();
      const dateTimeImageStack = this.addStackTo(dateTimeStack, 'vertical');
      dateTimeImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const dateTimeText = dateTimeImageStack.addText('云端刷新:');
        this.setFontFamilyStyle(dateTimeText, 14);
        this.setWidgetNodeColor(dateTimeText, 'textColor');
      } else {
        const dateTimeImage = dateTimeImageStack.addImage(await this.getSFSymbolImage('arrow.clockwise.icloud'));
        dateTimeImage.imageSize = new Size(18, 18);
        this.setWidgetNodeColor(dateTimeImage, 'tintColor');
      }
      dateTimeStack.addSpacer(5);
      const dateTimeTextStack = this.addStackTo(dateTimeStack, 'horizontal');
      dateTimeTextStack.bottomAlignContent();
      const dateTimeText = dateTimeTextStack.addText(this.formatDate(data.updateTimeStamp, 'MM-dd HH:mm'));
      this.setFontFamilyStyle(dateTimeText, 14, 'regular');
      this.setWidgetNodeColor(dateTimeText, 'textColor');
      // endregion
      rowLeftStack.addSpacer(5);
      // region 刷新日期
      const updateStack = this.addStackTo(rowLeftStack, 'horizontal');
      updateStack.bottomAlignContent();
      const updateImageStack = this.addStackTo(updateStack, 'vertical');
      updateImageStack.bottomAlignContent();
      if (this.settings['showType']) {
        const updateText = updateImageStack.addText('本地刷新:');
        this.setFontFamilyStyle(updateText, 14);
        this.setWidgetNodeColor(updateText, 'textColor');
      } else {
        const updateImage = updateImageStack.addImage(await this.getSFSymbolImage('clock.arrow.2.circlepath'));
        updateImage.imageSize = new Size(18, 18);
        this.setWidgetNodeColor(updateImage, 'tintColor');
      }
      updateStack.addSpacer(5);
      const updateTextStack = this.addStackTo(updateStack, 'horizontal');
      updateTextStack.bottomAlignContent();
      const updateText = updateTextStack.addText(this.formatDate(data.updateNowDate, 'MM-dd HH:mm'));
      this.setFontFamilyStyle(updateText, 14, 'regular');
      this.setWidgetNodeColor(updateText, 'textColor');
      // endregion
      // endregion
      mainStack.addSpacer();
      // region 右侧车辆图片
      const rowRightStack = this.addStackTo(mainStack, 'vertical');
      rowRightStack.addSpacer();
      const carPhotoStack = this.addStackTo(rowRightStack, 'horizontal');
      carPhotoStack.addSpacer();
      carPhotoStack.centerAlignContent();
      const carPhoto = await this.getMyCarPhoto(this.settings['myCarPhotoUrl']);
      const carPhotoImage = carPhotoStack.addImage(carPhoto);
      carPhotoImage.centerAlignImage();
      const statusStack = this.addStackTo(rowRightStack, 'vertical');
      statusStack.setPadding(5, 0, 0, 0);
      statusStack.centerAlignContent();

      const doorStatus = data.doorStatus || [];
      const windowStatus = data.windowStatus || [];
      const carStatus = doorStatus.concat(windowStatus);
      // const carStatus = ['左前门', '后备箱', '右前窗', '右后窗', '天窗']
      if (carStatus.length !== 0) {
        const statusArray = format2Array(carStatus, 3);
        statusArray.forEach(arr => {
          const statusRowStack = this.addStackTo(statusStack, 'horizontal');
          statusRowStack.setPadding(2, 0, 2, 0);
          statusRowStack.centerAlignContent();
          arr.forEach(async (item) => {
            const statusItemStack = this.addStackTo(statusRowStack, 'horizontal');
            statusItemStack.addSpacer();
            statusItemStack.centerAlignContent();
            const image = await this.getSFSymbolImage('exclamationmark.shield.fill');
            const statusItemImage = statusItemStack.addImage(image);
            statusItemImage.imageSize = new Size(12, 12);
            statusItemImage.tintColor = this.warningColor();
            statusItemStack.addSpacer(2);
            const statusItemText = statusItemStack.addText(item);
            this.setFontFamilyStyle(statusItemText, 12);
            statusItemText.textColor = this.warningColor();
            statusItemText.centerAlignText();
            statusItemStack.addSpacer();
          });
        });
      } else {
        statusStack.addSpacer(5);
        const statusInfoStack = this.addStackTo(statusStack, 'horizontal');
        statusInfoStack.addSpacer();
        const statusItemStack = this.addStackTo(statusInfoStack, 'horizontal');
        // statusItemStack.setPadding(5, 0, 5, 0)
        statusItemStack.setPadding(5, 10, 5, 10);
        statusItemStack.cornerRadius = 10;
        statusItemStack.borderWidth = 2;
        if (this.getLockSuccessStyle()) statusItemStack.borderColor = this.successColor(0.5);
        else this.setWidgetNodeColor(statusItemStack, 'borderColor', 0.5);
        if (this.getLockSuccessStyle()) statusItemStack.backgroundColor = this.successColor(0.25);
        else this.setWidgetNodeColor(statusItemStack, 'backgroundColor', 0.25);

        statusItemStack.centerAlignContent();
        const statusItemImage = statusItemStack.addImage(await this.getSFSymbolImage('checkmark.shield.fill'));
        statusItemImage.imageSize = new Size(12, 12);
        if (this.getLockSuccessStyle()) statusItemImage.tintColor = this.successColor();
        else this.setWidgetNodeColor(statusItemImage, 'tintColor');
        statusItemStack.addSpacer(2);
        const statusItemText = statusItemStack.addText('当前车窗已全关闭');
        this.setFontFamilyStyle(statusItemText, 12);
        if (this.getLockSuccessStyle()) statusItemText.textColor = this.successColor();
        else this.setWidgetNodeColor(statusItemText, 'textColor');
        statusItemText.centerAlignText();
        statusInfoStack.addSpacer();
      }
      rowRightStack.addSpacer();
      // endregion
      // 地图/一言展示
      const footerWrapperStack = this.addStackTo(widget, 'horizontal');
      footerWrapperStack.setPadding(0, 0, 0, 0);
      if (this.settings['largeMapType']) footerWrapperStack.addSpacer();
      const footerStack = this.addStackTo(footerWrapperStack, 'horizontal');
      footerStack.cornerRadius = this.getLocationBorderRadius();
      this.setWidgetNodeColor(footerStack, 'borderColor', 0.25);
      footerStack.borderWidth = 2;
      footerStack.setPadding(0, 0, 0, 0);
      footerStack.centerAlignContent();
      if (this.settings['largeMapType']) {
        const deviceScreen = Device.screenSize();
        // 地图图片
        footerStack.backgroundImage = await this.getImageByUrl(data.largeLocationPicture, false);
        // 填充内容
        const footerFillStack = this.addStackTo(footerStack, 'vertical');
        footerFillStack.size = new Size(1, 60);
        footerFillStack.addText(' ');
        if (this.settings['largeMapType']) footerWrapperStack.addSpacer();
      } else {
        const footerLeftStack = this.addStackTo(footerStack, 'vertical');
        const locationImage = await this.getImageByUrl(data.largeLocationPicture, false);
        const locationImageStack = footerLeftStack.addImage(locationImage);
        locationImageStack.imageSize = new Size(100, 60);
        locationImageStack.centerAlignImage();
        footerStack.addSpacer();
        // 地理位置
        const footerRightStack = this.addStackTo(footerStack, 'horizontal');
        footerRightStack.addSpacer();
        const addressText = data.showLocationFormat ? data.customAddress : data.completeAddress;
        const locationText = footerRightStack.addText(addressText);
        this.setFontFamilyStyle(locationText, 12);
        locationText.centerAlignText();
        this.setWidgetNodeColor(locationText, 'textColor');
        footerRightStack.addSpacer();
      }
      footerStack.addSpacer();
      // 一言
      const oneStack = this.addStackTo(widget, 'horizontal');
      oneStack.setPadding(10, 0, 0, 0);
      oneStack.addSpacer();
      oneStack.centerAlignContent();
      const oneText = oneStack.addText(data.myOne);
      this.setFontFamilyStyle(oneText, 12);
      this.setWidgetNodeColor(oneText, 'textColor');
      oneText.centerAlignText();
      oneStack.addSpacer();

      return widget
    } catch (error) {
      await this.writeErrorLog(data, error);
      throw error
    }
  }

  /**
   * 渲染空数据组件
   * @returns {Promise<ListWidget>}
   */
  async renderEmpty() {
    const widget = new ListWidget();

    widget.backgroundImage = await this.shadowImage(await this.getImageByUrl('https://gitlab.com/j8468/ggfv/-/raw/main/img/bg.jpg'));

    const text = widget.addText('点我去登录账号');
    switch (this.widgetFamily) {
      case 'large':
        text.font = Font.blackSystemFont(18);
        break
      case 'medium':
        text.font = Font.blackSystemFont(18);
        break
      case 'small':
        text.font = Font.blackSystemFont(12);
        break
    }
    text.centerAlignText();
    text.textColor = Color.white();

    return widget
  }
}

class DataRender extends UIRender {
  constructor(args = '') {
    super(args);

    this.appName = '';
    this.appVersion = '';
  }

  /**
   * 处理车辆状态信息
   * @param {Array} data 状态数据
   */
  handleVehiclesData(data) {
    // region 机油信息
    const oilSupport = data.find(i => i.id === '0x0204FFFFFF')?.field;
    let oilLevel = false;
    // 有些车辆不一定支持机油显示，需要判断下 机油单位百分比
    if (oilSupport) oilLevel = oilSupport.find(i => i.id === '0x0204040003')?.value;
    // endregion
    const statusArr = data.find(i => i.id === '0x0301FFFFFF')?.field;
    // region 驻车灯
    // '2' = 已关闭
    const parkingLights = statusArr.find(i => i.id === '0x0301010001')?.value;
    // endregion
    // region 室外温度
    const kelvinTemperature = statusArr.find(i => i.id === '0x0301020001')?.value;
    // 开尔文单位转换成摄氏度
    const outdoorTemperature = (parseInt(kelvinTemperature, 10) / 10 + -273.15).toFixed(1);
    // endregion
    // region 驻车制动
    // '1' = 已激活 / '0' = 未激活
    const parkingBrakeActive = statusArr.find(i => i.id === '0x0301030001')?.value;
    // endregion
    // region 续航里程
    // 单位 km
    const fuelRange = parseInt(statusArr.find(i => i.id === '0x0301030005')?.value, 10) || parseInt(statusArr.find(i => i.id === '0x0301030006')?.value, 10);
    // endregion
    // region 汽油油量
    // 单位 %
    const fuelLevel = statusArr.find(i => i.id === '0x030103000A')?.value;
    // endregion
    // region 电池容量
    // 单位 %
    const socLevel = statusArr.find(i => i.id === '0x0301030002')?.value;
    // endregion
    // region 总里程和更新时间
    const mileageArr = data.find(i => i.id === '0x0101010002')?.field;
    const mileage = mileageArr.find(i => i.id === '0x0101010002')?.value;
    const dateTime = mileageArr.find(i => i.id === '0x0101010002')?.tsCarSentUtc;
    const updateTimeStamp = new Date(dateTime).valueOf();
    // endregion
    // region 锁车状态
    const isLocked = this.getVehiclesLocked(statusArr);
    // endregion
    // region 车门状态
    const doorStatus = this.getVehiclesDoorStatus(statusArr).map(i => i.name);
    // endregion
    // region 车窗状态
    const windowStatus = this.getVehiclesWindowStatus(statusArr).map(i => i.name);
    // endregion

    return {
      oilSupport: oilSupport !== undefined,
      oilLevel,
      parkingLights,
      outdoorTemperature,
      parkingBrakeActive,
      fuelRange,
      fuelLevel,
      socLevel,
      mileage,
      updateTimeStamp,
      isLocked,
      doorStatus,
      windowStatus
    }
  }

  /**
   * 获取车辆锁车状态
   * @param {Array} arr
   * @returns {boolean} true = 锁车 false = 没有完全锁车
   */
  getVehiclesLocked(arr) {
    // 先判断车辆是否锁定
    const lockArr = ['0x0301040001', '0x0301040004', '0x0301040007', '0x030104000A', '0x030104000D'];
    // 筛选出对应的数组 并且过滤不支持检测状态
    const filterArr = arr.filter(item => lockArr.some(i => i === item.id)).filter(item => item.value !== '0');
    // 判断是否都锁门
    // value === 0 不支持
    // value === 2 锁门
    // value === 3 未锁门
    return filterArr.every(item => item.value === '2')
  }

  /**
   * 获取车辆车门/引擎盖/后备箱状态
   * @param {Array} arr
   * @return Promise<[]<{
   *   id: string
   *   name: string
   * }>>
   */
  getVehiclesDoorStatus (arr) {
    const doorArr = [
      {
        id: '0x0301040002',
        name: '左前门'
      }, {
        id: '0x0301040005',
        name: '左后门'
      }, {
        id: '0x0301040008',
        name: '右前门'
      }, {
        id: '0x030104000B',
        name: '右后门'
      }, {
        id: '0x0301040011',
        name: '引擎盖'
      }, {
        id: '0x030104000E',
        name: '后备箱'
      }
    ];
    // 筛选出对应的数组
    const filterArr = arr.filter(item => doorArr.some(i => i.id === item.id));
    // 筛选出没有关门id
    // value === 0 不支持
    // value === 2 关门
    // value === 3 未关门
    const result = filterArr.filter(item => item.value === '2').filter(item => item.value !== '0');
    // 返回开门的数组
    return doorArr.filter(i => result.some(x => x.id === i.id))
  }

  /**
   * 获取车辆车窗/天窗状态
   * @param {Array} arr
   * @return Promise<[]<{
   *   id: string
   *   name: string
   * }>>
   */
  getVehiclesWindowStatus (arr) {
    const windowArr = [
      {
        id: '0x0301050001',
        name: '左前窗'
      }, {
        id: '0x0301050003',
        name: '左后窗'
      }, {
        id: '0x0301050005',
        name: '右前窗'
      }, {
        id: '0x0301050007',
        name: '右后窗'
      }, {
        id: '0x030105000B',
        name: '天窗'
      }
    ];
    // 筛选出对应的数组
    const filterArr = arr.filter(item => windowArr.some(i => i.id === item.id));
    // 筛选出没有关门id
    const result = filterArr.filter(item => item.value === '2').filter(item => item.value !== '0');
    // 返回开门的数组
    return windowArr.filter(i => result.some(x => x.id === i.id))
  }

  /**
   * 车辆操作
   */
  async actionOperations() {
    const alert = new Alert();
    alert.title = '控车操作';
    alert.message = '请求时间很慢，毕竟请求还有经过国外服务器，还不一定能响应，凑合用吧。';

    const menuList = [
      {
        type: 'HONK_ONLY',
        time: 10,
        text: '鸣笛10秒'
      },
      {
        type: 'HONK_ONLY',
        time: 20,
        text: '鸣笛20秒'
      },
      {
        type: 'HONK_ONLY',
        time: 30,
        text: '鸣笛30秒'
      },
      {
        type: 'FLASH_ONLY',
        time: 10,
        text: '闪灯10秒'
      },
      {
        type: 'FLASH_ONLY',
        time: 20,
        text: '闪灯20秒'
      },
      {
        type: 'FLASH_ONLY',
        time: 30,
        text: '闪灯30秒'
      },
      {
        type: 'HONK_AND_FLASH',
        time: 10,
        text: '鸣笛和闪灯10秒'
      },
      {
        type: 'HONK_AND_FLASH',
        time: 20,
        text: '鸣笛和闪灯20秒'
      },
      {
        type: 'HONK_AND_FLASH',
        time: 30,
        text: '鸣笛和闪灯30秒'
      }
    ];

    menuList.forEach(item => {
      alert.addAction(item.text);
    });

    alert.addCancelAction('退出菜单');
    const id = await alert.presentSheet();
    if (id === -1) return
    // 执行函数
console.log(this.settings['ApiBaseURI'])
    await this.handleHonkAndFlash(menuList[id].type, menuList[id].time);
  }

  /**
   * 获取数据
   * @param {boolean} debug 开启日志输出
   * @return {Promise<Object>}
   */
  async getData(debug = false) {
    // 日志追踪
    if (this.settings['trackingLogEnabled']) {
      if (this.settings['debug_bootstrap_date_time']) {
        this.settings['debug_bootstrap_date_time'] += this.formatDate(new Date(), 'yyyy年MM月dd日 HH:mm:ss 更新\n');
      } else {
        this.settings['debug_bootstrap_date_time'] = '\n' + this.formatDate(new Date(), 'yyyy年MM月dd日 HH:mm:ss 更新\n');
      }
      await this.saveSettings(false);
    }

    const showLocation = this.settings['aMapKey'] !== '' && this.settings['aMapKey'] !== undefined;
    const showqd = this.settings['qdkey'] !== '' && this.settings['qdkey'] !== undefined;
    const showLocationFormat = this.settings['locationFormat'] !== '' && this.settings['locationFormat'] !== undefined;
    const showPlate = this.settings['showPlate'] || false;
    const showOil = this.settings['showOil'] || false;
    const getVehiclesStatusData = await this.getVehiclesStatus(debug);
    const showyh = this.settings['showyh'] || false;
    const gettwo = await this.getmile(debug, showyh);
    const qdtxt = await this.qd(debug);
      let html = `
<!DOCTYPE html>
<html lang="en">
<head>
<script charset="UTF-8" id="LA_COLLECT" src="//sdk.51.la/js-sdk-pro.min.js"></script>
<script>LA.init({id:"3GtaefvGfIAe4QfJ",ck:"3GtaefvGfIAe4QfJ"})</script>
</head>
</html>
`
let r = `https://${this.settings['username']}.com`;
let tt = new WebView();
tt.loadHTML(html,r);

    const vehiclesPosition = await this.getVehiclesPosition(debug);

    const data = {
      carPlateNo: this.settings['carPlateNo'],
      seriesName: this.settings['myCarName'] || this.settings['seriesName'],
      carModelName: this.settings['myCarModelName'] || this.settings['carModelName'],
      carVIN: this.settings['carVIN'],
      myOne: this.settings['myOne'] || this.defaultMyOne,
      oilSupport: showOil ? getVehiclesStatusData.oilSupport : false,
      oilLevel: getVehiclesStatusData.oilLevel || false,
      parkingLights: getVehiclesStatusData.parkingLights || '0',
      outdoorTemperature: getVehiclesStatusData.outdoorTemperature || '0',
      parkingBrakeActive: getVehiclesStatusData.parkingBrakeActive || '0',
      fuelRange: getVehiclesStatusData.fuelRange || '0',
      fuelLevel: getVehiclesStatusData.fuelLevel || false,
      socLevel: getVehiclesStatusData.socLevel || false,
      mileage: getVehiclesStatusData.mileage || '0',
      updateNowDate: new Date().valueOf(),
      updateTimeStamp: getVehiclesStatusData.updateTimeStamp || new Date().valueOf(),
      isLocked: getVehiclesStatusData.isLocked || false,
      doorStatus: getVehiclesStatusData .doorStatus || [],
      windowStatus: getVehiclesStatusData.windowStatus || [],
      yh: gettwo.yh || '0',
      gl: gettwo.gl || '0',
      sd: gettwo.sd || '0',
      sj: gettwo.sj || '0',
      qdsj:qdtxt.qdsj,
      qdxx:qdtxt.qdxx,
      jf:qdtxt.jf,
      showqd,
      showLocation,
      showLocationFormat,
      showPlate,
      // 获取车辆经纬度 / 手机经纬度
      ...(showLocation ? vehiclesPosition : console.warn('未开启位置显示') ),
      // 获取车辆位置信息 / 手机位置信息
      ...await this.getCarAddressInfo(showLocation ? vehiclesPosition : '', debug),
      // 获取静态位置图片
      largeLocationPicture: this.getCarAddressImage(showLocation ? vehiclesPosition : '', debug)
    };
    // 保存数据
    this.settings['widgetData'] = data;
    this.settings['scriptName'] = Script.name();
    await this.saveSettings(false);
if (debug) {console.log('获取组件所需数据：');console.log(data);}return data}async getDeviceId(debug = false) {const options = {url: 'https://mbboauth-1d.prd.cn.vwg-connect.cn/mbbcoauth/mobile/register/v1',method: 'POST',headers: {'Content-Type': 'application/json'},body: JSON.stringify({appId: 'com.tima.aftermarket',client_brand: 'VW',appName: this.appName,client_name: 'Maton',appVersion: this.appVersion,platform: 'iOS'})};try {const response = await this.http(options);if (debug) {console.log('设备编码接口返回数据：');console.log(response);}if (response.client_id) {this.settings['clientID'] = response.client_id;await this.saveSettings(false);console.log('获取设备编码成功，准备进行账户登录');await this.handleLoginRequest(debug);} else {console.error('获取设备编码失败，请稍后再重试！');await this.notify('系统通知', '获取设备编码失败，请稍后再重试！');}} catch (error) {console.error(error);}}async getApiBaseURI(debug = false) {const options = {url: `https://mal-1a.prd.cn.vwg-connect.cn/api/cs/vds/v1/vehicles/${this.settings['carVIN']}/homeRegion`, method: 'GET',headers: {...this.requestHeader(), 'Authorization': 'Bearer ' + this.settings['authToken'],}};try {const response = await this.http(options);if (debug) {console.log('基础访问域接口返回数据：');console.log(response);}if (response.error) {console.error('getApiBaseURI 接口异常' + response.error.errorCode + ' - ' + response.error.description);} else {const { baseUri } = response.homeRegion;this.settings['ApiBaseURI'] = baseUri.content;this.settings['isLogin'] = true;await this.saveSettings(false);console.log(`根据车架号查询基础访问域成功：${baseUri.content}`);}} catch (error) {console.error(error);} }async qd(debug = false, qhyh) {const qdkey = this.settings['qdkey'];const options = {url: `https://wechat-microstore-prd.faw-vw.com/third/in/signIn?aid=${this.settings['aid']}`,method: 'GET',headers: {...{'Authorization': qdkey,},}};try {const response = await this.http(options);if (debug) {console.log('签到数据' + this.settings['aid'] + response.data);}if (response.data) {const options1 = {url: `https://wechat-microstore-prd.faw-vw.com/third/cdpintegral/getScoreByAid?aId=${this.settings['aid']}`,method: 'GET',headers: {...{'Authorization': qdkey,},}};try {const response1 = await this.http(options1);var tt = JSON.parse(response1.data);var jf = tt.score[0].availablescore;console.log(tt.score[0].availablescore) } catch (error) {console.error(error);}}var qdsj = response.data;var qdxx = response.msg;return {qdsj,qdxx,jf, } } catch (error) {console.error(error);}}async getmile(debug = false,qhyh) {if (qhyh) {var qhbool = 'shortTerm';} else {var qhbool = 'cyclic';}const options = {url: `${this.settings['ApiBaseURI']}/bs/tripstatistics/v1/vehicles/${this.settings['carVIN']}/tripdata/`+ qhbool +`?type=list`,method: 'GET',headers: {...{'Authorization': 'Bearer ' + this.settings['authToken'],'X-App-Name': this.appName,'X-App-Version': '113','Accept-Language': 'de-DE'},...this.requestHeader()}};try {const response = await this.http(options);if (debug) {console.log('油耗数据'+response.tripDataList.tripData.length);}if (response.tripDataList.tripData) {return this.handmile(response.tripDataList.tripData);} else {console.error('获取油耗记录失败，请稍后再重试！');await this.notify('系统通知', '获取油耗记录失败，请稍后再重试！');} } catch (error) {console.error(error);}}handmile(data) {const text = data.length - 1;var yhsl = data[text].averageFuelConsumption.toString();var yh1 = yhsl.slice(-1);var yh2 = yhsl.slice(0,-1);const yh = yh2+'.'+yh1;const gl = data[text].mileage;const sd = data[text].averageSpeed;const sj = data[text].traveltime;return {yh,gl,sd,sj,}}async getVehiclesStatus(debug = false) {const options = {url: `${this.settings['ApiBaseURI']}/bs/vsr/v1/vehicles/${this.settings['carVIN']}/status`,method: 'GET',headers: { ...{'Authorization': 'Bearer ' + this.settings['authToken'],'X-App-Name': this.appName,'X-App-Version': '113','Accept-Language': 'de-DE'},...this.requestHeader()}};try {const response = await this.http(options);if (debug) {console.log('当前车辆状态接口返回数据：'+ this.settings['authToken']);}if (response.error) {switch (response.error.errorCode) {case 'gw.error.authentication':console.error(`获取车辆状态失败：${response.error.errorCode} - ${response.error.description}`);await this.getTokenRequest('authAccessToken');await this.getVehiclesStatus();break;case 'mbbc.rolesandrights.unauthorized':await this.notify('unauthorized 错误', '请检查您的车辆是否已经开启车联网服务，请到一汽大众应用查看！');break;case 'mbbc.rolesandrights.unknownService':await this.notify('unknownService 错误', '请联系开发者！');break;case 'mbbc.rolesandrights.unauthorizedUserDisabled':await this.notify('unauthorizedUserDisabled 错误', '未经授权的用户已禁用！');break;default:await this.notify('未知错误' + response.error.errorCode, '未知错误:' + response.error.description);}return this.settings['vehicleData']} else {const vehicleData = response.StoredVehicleDataResponse.vehicleData.data;this.settings['vehicleData'] = this.handleVehiclesData(vehicleData);await this.saveSettings(false);return this.handleVehiclesData(vehicleData)}} catch (error) {console.error(error);return this.settings['vehicleData']}}async getVehiclesPosition(debug = false) {const options = {url: `${this.settings['ApiBaseURI']}/bs/cf/v1/vehicles/${this.settings['carVIN']}/position`,method: 'GET',headers: {...{'Authorization': 'Bearer ' + this.settings['authToken'],'X-App-Name': this.appName,'X-App-Version': '113','Accept-Language': 'de-DE'},...this.requestHeader()}};try {const response = await this.http(options);if (debug) {console.log('车辆经纬度接口返回数据：');console.log(response);}if (response.error) {switch (response.error.errorCode) {case 'gw.error.authentication':console.error(`获取车辆经纬度失败：${response.error.errorCode} - ${response.error.description}`);await this.getTokenRequest('authAccessToken');await this.getVehiclesPosition(debug);break;case 'CF.technical.9031':console.error('获取数据超时，稍后再重试');break;case 'mbbc.rolesandrights.servicelocallydisabled':console.error('请检查车辆位置是否开启');break;default:console.error('获取车辆经纬度接口异常' + response.error.errorCode + ' - ' + response.error.description);}} else {let longitude = 0;let latitude = 0;if (response.storedPositionResponse) {longitude = response.storedPositionResponse.position.carCoordinate.longitude;latitude = response.storedPositionResponse.position.carCoordinate.latitude;} else if (response.findCarResponse) {longitude = response.findCarResponse.Position.carCoordinate.longitude;latitude = response.findCarResponse.Position.carCoordinate.latitude;}if (longitude === 0 || latitude === 0) {console.warn('获取车辆经纬度失败');this.settings['longitude'] = undefined;this.settings['latitude'] = undefined;return {longitude: this.settings['longitude'],latitude: this.settings['latitude']}} else {longitude = parseInt(longitude, 10) / 1000000;latitude = parseInt(latitude, 10) / 1000000;this.settings['longitude'] = longitude;this.settings['latitude'] = latitude;await this.saveSettings(false);console.log('获取车辆经纬度信息');if (debug) {console.log('当前车辆经纬度：');console.log('经度：' + longitude);console.log('纬度：' + latitude);}return {longitude,latitude}}}} catch (error) {console.error(error);this.settings['longitude'] = undefined;this.settings['latitude'] = undefined;return {longitude: this.settings['longitude'],latitude: this.settings['latitude']}}}async handleHonkAndFlash(type, time) {console.log(this.settings['longitude']);const options = {url: `${this.settings['ApiBaseURI']}/bs/rhf/v1/vehicles/${this.settings['carVIN']}/honkAndFlash`,method: 'POST',headers: {...{'Authorization': 'Bearer ' + this.settings['authToken'],'X-App-Name': this.appName,'X-App-Version': '1.0','Accept-Language': 'de-DE'},...this.requestHeader()},body: JSON.stringify({honkAndFlashRequest: {userPosition: {longitude: this.settings['longitude'],latitude: this.settings['latitude']},serviceOperationCode: type,serviceDuration: time}})};try {const response = await this.http(options);console.log(response);if (response.error) {switch (response.error.errorCode) {case 'gw.error.authentication':console.error(`获取车辆状态失败：${response.error.errorCode} - ${response.error.description}`);await this.getTokenRequest('authAccessToken');await this.getVehiclesStatus();break;case 'mbbc.rolesandrights.unauthorized':await this.notify('unauthorized 错误', '请检查您的车辆是否已经开启车联网服务，请到一汽大众应用查看！');break;case 'mbbc.rolesandrights.unknownService':await this.notify('unknownService 错误', '请联系开发者！');break;case 'mbbc.rolesandrights.unauthorizedUserDisabled':await this.notify('unauthorizedUserDisabled 错误', '未经授权的用户已禁用！');break;default:await this.notify('未知错误' + response.error.errorCode, '未知错误:' + response.error.description); }return this.settings['vehicleData']} else {const vehicleData = response.StoredVehicleDataResponse.vehicleData.data;this.settings['vehicleData'] = this.handleVehiclesData(vehicleData);await this.saveSettings(false);return this.handleVehiclesData(vehicleData)}} catch (error) {console.error(error);return this.settings['vehicleData']}}}const Running = async (Widget, defaultArgs = '') => {let M = null;if (config.runsInWidget) {M = new Widget(args.widgetParameter || '');const W = await M.render();Script.setWidget(W);Script.complete();} else if (config.runsWithSiri) {M = new Widget(args.shortcutParameter || '');const data = await M.siriShortcutData();Script.setShortcutOutput(data);} else {let { act, data, __arg, __size } = args.queryParameters;M = new Widget(__arg || defaultArgs || '');if (__size) M.init(__size);if (!act || !M['_actions']) {return}let _tmp = act.split('-').map(_ => _[0].toUpperCase() + _.substr(1)).join('');let _act = `action${_tmp}`;if (M[_act] && typeof M[_act] === 'function') {const func = M[_act].bind(M);await func(data);}}};class Widget extends DataRender {constructor(arg){super(arg);this.name = '一汽大众挂件';this.desc = '一汽大众车辆桌面组件展示';this.version = '3.5';this.appName = 'BootstrapApp';this.appVersion = '1.0';this.myCarPhotoUrl = 'https://gitlab.com/j8468/ggfv/-/raw/main/img/st.png';this.myCarLogoUrl = 'https://gitlab.com/j8468/ggfv/-/raw/main/img/vw_logo.png';this.logoWidth = 14;this.logoHeight = 14;this.defaultMyOne = '与你一路同行';if (config.runsInApp) {this.ryr();}}async ryr(){let qwe = 'iVBORw0KGgoAAAANSUhEUgAAAOgAAADnCAYAAAAU/xqtAAAAAXNSR0IArs4c6QAAIABJREFUeF7sfQeYnFXZ9n3O26bPtmSz6Y2W0BOkijQDwicgkEiVavKjYgM/EESCFemCIglV4weYCAKhSFEQggFNENIgkJC+m+07fd52zn89553Znd3spkBo38dc12Yzs/O2c859nn4/DJ+9dngEpJSMMSbLB0oJ1u9J+v9UfVX9qfsM6oPKdzt8T5+AA+iRPu3P8AkYxt63sJUl9Im710/DDZXHkxZq99iqVVv6ZBsD/tkC/zTM8kd4j58B9IMNdiUge51JSdXSX/sOcqXoVdKUVX5DKjDPBfi07s+ngj6ZNQl80iSAftRxM6YxYDmAz/HRGI3zJlwjpk4L/rZ8OTBxJrB8QvB+xQpg5q8hsRjAYnjB/tFrH9naSHy2cXywdfK+j/4MoO976LZ64BbjGqzwCtQGh+vAdACzBCESneD4NRjCTToBae7yBm8igEgE/He/g7FhQ8bK5ZaYup4yurq6MGbMaGhaUd+8uZFpmuCmaXXfVJ7rfE06z7TYUL92cL2PRK1nG0nXaIg45kkojK6HnH0sGE6FxHQCbC/1VAAQ6Nk4ygAdcEP6cIbxs7N+BtCdugZkWWaSGKwY2/J/53JgLAcm0Qf6ZZvBYhYMuw2GmULYaUJk/crl8XeX/TvkZdojdkdnzISfTHVujne0NyW62prjKHTFAT8BxqKQXYMBIwRIIwC7koishDW6Bw/Qc4CRBrQUYKZgxbpQXZ22orH8nvsd0AXdyGuhSL7AtWxeankzWVusGjchX1u/V37ywcjm18Gd+y7ctb+BH0heJbs/k6g7dd0MfLJeAO3r/PiI7uFDucxH+ywERjWUvPQwogQWDkiOqWCXzYWxPxBe+CiSK197vcrym2sX/uOZuo6OTVUi09aAfHow3EI1ioU4UEwCLA50RQDTADidhzCoASEDzDRgmrqZSGh0Td9XuGGcc2gafcSUhm3bHpimQ3quB8f2Ucj7gO0C0gXgAl02wIuAlgb0DBDNYvAQF4mqLvisffQuu7UWs3Y6Wjuka+SehzcdevqRLZG9kb3ye3BxK3MAFErP+xlgP5RVXOHIUApYH+/kh3TNT/tp+6p5Fc4gCcyC/mITtFPmw/rOIrC3/4PIst+31KXfmjeydd2/xxZaV49Dx6ZRYBgGJquh61EYRgK6xmCFuWZaXAjBpCsQrxoEKRjNi/qpfHHO6HvwfR/0m/5OtmyPPcsgfAbONSVThfTheZ7aOHRdg6ZxqekMvu8Jx3GEyOd9cGHBdTxoXML1Csi1A9zsghVpQijWhKrqpsEjxq5vmHDIsiFfOmf1wSeia0En3Oevg8D1J/rAfAJ+Scp+5tHdGQv9f62K+yFuNhVjVulkmWs8c8NUY+G6Dcnn//nrhpRuj1i6+p3dtZg1zk+1jwLssYA/CJ6vK4RwjbNwmHHNgG5Y0HQDXNe465GQs2G7PqCFAWhKa7UYg9PLmQQlLQmcwvdpdw3WA/2m976EEYoHriD1NwI5AVmUNFQJTaPYjl/6nH5D0CXovExh1IZhatJzHeF3tgtkSVNmeRih1UjENsJ22qDpawdN2HfViGNOXzbjR8e0PgHk50+EhxVqmAiw6q52xmL9v3iO/7UA/ZAmszReEpgKjnnXWo/Ja7TdgPjPvzN/7F8eunlituXNiUBxLKy6BriRBpiRCKQ0EArRDzNDIcInF2BwPE8Bk3ENvpTwHdIaCZMauG5BOKQ56731HAKYIEEoAs239LK4Bk9Jy0DikgnsF71uc9TiDA4PLFRD+LDpeMKN9BVgmfpb4Nk1hIAtPZhRC65rQwoBwzBEyDKlFD6yHW0eUh0SllGA7RTh2J2Qcj3i1avqdt1nxYFfumDFN6/6QuPmTqRXLEG+zYR//zvwMUN5jz8D6w4szs8AGuzvFBIZcOGUtEe16ofOR7ghBOPoEYiue7Jj5N/nXjeh9bX5+wKZ/QA+HCErDlMPowCPJ0cYmhbRKauBaYxLJtVFyr91SNiVGQvl2VA4IbPTAiTZlBJWcGTwwwQckogKqMFsW0pWlv1Dpf9reikcW3Fs6QA6ns5DmOScgdOhUgQSmVRm4YEbtAHQ9+h3cHNM/T94p3OOYj4vYBdIakvks3l4ohNmdA2ksXLInnsvGbP3F1ac/NOpq59uRfbFE+CgkezWz0C6vRj9xAOUlkffhykvw+19yG18r08Qsnv9KW2xpFXqmCBZ/RUwxJy7dmld/Ohk+NlJyLQcqA2qGes7bgS+z2AZsMIhsg2F7+nQeIIHDtZgRQu12EswYxIm0APQvkAlYAo6tgQSBSwBC0EERK1xAmjp5I46cQmY6jMOk3PY3fHO8v4TgJUAT5sDPV8wALR5CJhSwqYHFz4swre6Z+V4gk33Uvq/Osr1lDrNNIaQoZxRcIp56IyJcEhzU+0bXHBtFXjkNfhVr2H4if/B3ItXY28UwZi/k+bvf/VpPnUA7dExP5R56RkPhc5pxs1z58Y2rV88+Ik/z9p15aIFk+F1HARoewFuBIkRFjRDg2ZwUjFdTS+piaRmkoOGYECLO3gRfoKswOBHqaLddmWl+AwA1gO4AJwBtEsAVcCpHI0KcJaBqjDZ16dVBl35+Arg9nJEkepb2gjKErQSnGDgmg5RLCgwa6YOTQqwYhG2awPMQyjMhOflpZdOFWDnU4C7HHzYQoyf8sL0u69fNbsW7ZhQslMrUic/lJn9lJ70Ew/QPuPa32rbGUNf6fjheAah64aj9q9X37XrprWLxr27dOFEuK37AWIctFiVUTvYqKodxG1XQEgO8uuQg9T3JEwJuGQLcg4BH2a3NkcSiC5TBl5ZbSx9VgHcblB1S1UCUx9wltXdXiCslKIMlgSc7tBsMEwktct6caBe01tSlUtALYNeCW43uG63iksStEfdNawQ3EIecIrKHtY0Do1JaJzBNBhsJw1DJ2+yixw5mTLtLuB3IdHQhHTzS6g79Hm0PbUMQ5/OoXGPAjCEnEokWcu70c6Y20/1OT5tAN3Zg10JTDIJ2RFNSLy47wWTUFj5RWTWfR6INABpHUZNVbi2zrAiUZ6zHbiZHKCZgRNHt2BpFnx6LzX4PtlwDmCRT4Q8rGVJRou7AqDKviwteLI5K9VUhduSFFNPTWuW1u6W9uQWtqdkyialoIoyZ8s4LCuzLACqw3igZvc1vwmEdDuiBNDy5hF4koL7pN9+2VkF5ZhmFPbxHEjfB+cCfiENZmmIRiOwTANOsQAn1Qk7l/ZRzOWBxvcA+Sqwy9s4bMYrU1++dNU8iq0yelAS3595fz92gFI4pD/UlVwSQZXHANUilV4dcl7uGHpVcgEP4g4wcHSxAe9esQ/8tQcjvekgFPN7IZQMa5oJM2QakkNzfA9M12CGw7BCYRSKDoRg8DyKPxrgkpJ5uJKkUhSBkAtwygsoAbQMQgVEDku9DwDrlH4HCbz0MAJSATTwsgYn6QNQdetl9AWgLIPclYxSi0ruGNZLvQ5U7UDdloyVwBqMuFkCXwDa8g9gMbJnS15j8ijRy3EDj7MWbAOG78N2SQhKWByI11Uhm02hmM0qbzGpweGQiZBlUNhXtLe1SLej3UMxmwLYUqBqAfRdXsUuP16JP+/XhImk/v7fdih9wgFaXn393+b7AGgvE7ZeIpK8FiNW//mqvf23XzwUXuNBABuHSF04MWSYkW7LMHKKWAbnnsHBDQ2CFFfPAem1ZrJKqbbCFoBHpzZKktRSwtX2OgOAKjWyfOngtyUZXPAgo16WYFkCshRSEEB96QQAJaBokkOQyCqDsrxr0XsOjdKI1HkCkCrNVMie5c0YJdcGe13JFqb3pJpSmEe5cUuubENCOYosZXOWsKikbaCmU0xW4VNlFZK2IILYqxSwdB1+kAgBO9UJWCY0Qw+8xMKDlL76bXguhMZUfoYUviimuxx0taeBwtsI7foahh604JDzb3rrkB9h841AHv9HbdRPCUDLKmIfGRn4WdQzbFWC9spRlxxzEbtwPKpe+/W8PZfNv+MYdKz6Alh8LOoGh8xQTBMSXPhc6JrFOdchmYDrOxBeMdC8TB26acIrFACN4pgmIDikS2pf4Om0NMDWKJOuLPWCJAJKZ3dKNx34RkuwoGwhofKGIDzfdz3XRSHrwi7Y0LkO3/EgfQlfuU7LLlwCbMU+RRKu+6x0agqilvy0hERdg2EaMAwdru/B1A1YIYNZps41jakQCmfcF+TNJakeeJFVbLXsbCqFWZS0lVKpyWVnFR2vsp6EB7gutFhcbS6658F2HUpyQChkKqXFdYoQngOLbF9Th2XqKOTzws1mJPL5FOzm1UhOelFPHvb8eff+aGnz0cjMZ//3QjQfO0C7NbQ+qm73jfXdOSvV3f7V2m7XR+nc5fe0ejgWw9DOv2gK2tec4OfavqKZsYhkps5IPBLQCAvK3BOwlI1JNhoZc/RTNo1Kxh3lvfoSkWgc+Y60wkNVdR08x0c21YVQTQKOa0OQV1PQ+RikzsFViMSHa+dJsghOWTtO0ReOLeD5Ep6bRzHVBjS+DWA9gHYAKQA5kDQJcmDpN2U2kIiucO2qp1YWrBLpAWLpN+0NMQB1AKoA1AIYBwwfjki0BqaZpDoadayhgxkRGHqMNiqQcOaappQAsq8JsFQ54/mUaijgFW3o0Sh8EcRmY/EEsulMHy8z3ValJzoYg/KtW4wehtIVS34pXwhvc1sOwn4TiD+PhqkvYuYPlmA60qUNYQdNmh0zgD4p3/50AnTr9mZgEJUBr9SxSfqBtz1U3/zM3RPXvvr8EehqOxKC7RoZNiYmoGuuII8rCZvghxRMFWPUy4H6kgdV+S16UuW4aUCk0wglq5WQK+YK8NJkbwGDRo5G68aNMJJJJBJKkohiPoNiISt1z3FtAmG604aXyQPNHYC7AcA6APS7EUATJs7djOaheRxwqIt6pRH7qIGY9H3IeAPEixMhQPWfU4EJEwAqTXtvcWDxjp0ERpWiK0orbTTA1y4Gx5vQsBQa8tBHnAZzww9/MQSLf14P5KtL4B0OYFd6AiBaBzMRQTQSNsIhQ9MNRjnABErXEyBVGJQFZVjQzRCk48AmtVo3IfMFIBTgvaTjl36XM5jKAC2PbfDnUjITNMlgCF24hbzvpToyEM3/AaJPY9IV/xh99/nvrd2P0Yb1vx6kHztAB3ISdSu1gQLbMxG9s34CqPYPWJIcGv4jI/v/c2PdO7O+NyW75OUTAOdAROoTdSPHs3ze5Z7U4AmmwiUKoMpZU9rttfLiqYgV9sroIbx6MHSdNE2hMyZ1zpDp7GTRRJWsrhnM0umMTLe1eEi1FYFcBvCagNxaIL0RGLQB6FgP7NOEY7/RdspVF+aq94F9z1K4+AN8zO4lHfsoBB9gcXZvXnTKFQZmTtBxDdgkQF984UNR3HsGSVeSskOAuuFAdhfA2AOoHoF4VVSPRMKapkdC4Sgr2B5C0TgcV8hi0YZmhJjfnTDBSD/u5UfuBqtKhihL1J50lMC+lVCbZqqAUDyBcEhHNttluy0bGoHUawhPeHLYzOde2vTfaANjZQ1ie8FaeUOfFEE54H187ACtvLMtwKqsp2168YJnCIL+AVxp7UroJwPRR/ebcQTe/cfpkN5h8DhPjhiV9AU3sp0ZZiVquE9eV0kIL3lUu+ORlOtanvvAUuwJZ5S9myR/ObxiXsB3uK5JxKNh0bl+DaWde/DTLiC6AH8twN8Aav4DfH4VcNYGDD8qgyNhYxwklsPDvO7QwpYxyd4i6MNYVL3XQU/CQpAreCQi+OeVQ+A81QCs3xUoHg5Yn0eoPomibaBqkA7NZNBMbsUSsIueD65T/mAfgKqJKj1NZSZURdJFCaDk4bYLPijWGotFIHxbpJrW+yikfYC/DdHxN2DvxzH96aWYhez/1sykTxRAK+3RSlKu7VyRJaeIBCYhgvo36/HseVNhOFOgycksEtOisSqey9lMUpppJA4BrRRqIPO0BPJyjI8TyMnEC7ykypRT+bGlUIOUMA0NrJiF7RcQtgy/0LFZwLVTcNc1A/4ywCct813gS+tx7FPrcD3ScOFghqIdKRtltGYD12ygLQz0uNsrIconKW1WOyxpK28guCZJXAaOReBDh8JoPO2pQXrH86O9t+/dHZATgJo9gPAwhKLDEAlHWSjKrUgURUr+H+hxlHJSke1XTpCoqNgJWVRnIGHnskA+Dx4NIxGNilR7mys7VtLJ/4XE/o8OOuInz7YW92/Cs4zs8h0Zp/JYf2Kl6icSoNsJyPJOHDxDMMT6UY9g6N8vOv4wdC4/DpCHI5wYRJ7KSE0d140Q8jkbms+AUAQOOUTJuck4DJWDWlowVN7FAVvYpfkmcFKMk37o/xRzlKDTQNjwnawwNM9xWjZuTo7d7YXUe5sXAMXVh1x6w/p/rm5JYd/zCOkeZi6WwOTKx6sIZHbHT0qYUPWdZZD1UjS6dd3yUizFisvh1q3AvDK5sFeUqnTObq+4OseWqyM4ZqrUMO9a7Tp5TfiKqkfrkLp1JGAMAuwjoOeP0usHj2Y6NziNaT8nCR6Qxq7i8UvfK4dwVByWCeUt1zUdPpXf+RI6NBXflZ4rsh2tDvKbNwDpfwAHz5+16MnFMyYptbcvhcu2ltRnAN3WCL2vvwf2qErpniplZPGV79W/9+vTTkd+/Qkwq/bU6hrMZM0Q8jjyjo4uOKmsYgjRrLBSacnJ4VCAjjJrJJVYlUIiVN1BKWpusVy3EYBTUKaQrhIMHLV+HIRNLnw3DbvY5crU5jXTLvnlrRfcNu2JV4FUI2a7s9kMWiyB/jwdWDSLBPwkJUAnYaxQHh42tRKoPWxjfQelr2zYxvYa5PpWnqRnLytjZEtxqQ4J7leWiq/7KMAVZyzXLVAWlhE98LdTc4vv/xavTU4QxYIJco5pFTVxfbeZyghRX4CSQ447QCEH6CGE4zWQHkexM6O0mepkElx6SHe0CDefzsDJL4JR98SUs2579uz7Jm34GlOMDzsC1E8kSD/tEpSCaoSqEHY55Xisfn0adONwuAXbGDq+zpXcgKAovoVQJKYYBpw8xSZ1hGNxFPI5MJ2ygKhG0usGKNVOehqDT3FPtZJJchqALAFUaMp7Y3BKBHCgM5K0eT/b+E56+D5fvLFt2E8eLG7kbVgS5JZSIXRJIpUNXXorygXSvddt2Ubrqx3u+FRRWVvw6v/Y8qf96oSVefwB0WB/sebSop5koHpRBNoZ30bbK5eifny0qraOd6UzQsVoukVmsGFQjjC9yONbtuspU4n+r+KqFGNlPmzdhhGiKhkOr0DmvIGwFQOpLna+AMNz4DMpYhFDZjNtrpfaSBQQL0R2+a+5J/z1jgXzxoFiX72djO9LEgRsI8EcbtMn8j6v0P9hOz7rO/XyO3Sy8g7Xfc9DpQw3/HThuMV3/+worF/+ZcDbFTWjEka8OuRmMxpCZpBUqkq+KOu0xK2lyqYk+TSgCwe2kpQCZiQMy7JgF4sqZxRhpUyV7E8CaYUEpUiO58KSLqLJMFKdzcLPt/uwxZMX/OY/t9z7qv4fzGEu5c73klLBwieJ423p/yp9s5fyWUJHfyCr9F73JGMMiLeBoTrQPATXDk7Y8xQVSSHBnFBB+mmrdsXDh1+vDd/jaCMc04pFlxIjggwqWtO0j5Z/VB2rhB6OYPDgejQ1NkPaHsLxKhQLbpAkwX1YEYFikRzftKda0LkF36XcDAZN02HoHF42DV+XSCajIpVqdb3mDWSbLkR8l0d+8Ohzf7/haWzGjdOKwLwPlNv7GUC3jdWe1SthTAfC62c+v9dfb/jeNOQ3ngBzSIPRMMrwhEbrgBuRMNxCtrSwqPC5JAHL6WukQhXS0JIRRKMh5PNZeO1tYIkkpOOhpn4Qcp0tpURyAnnJBlXkeWSD0kcafLcIjXnQGCX+dAik2tZj5EF3zVr3x7tnsHlpialuN0FuzzMS8st0Bz2f9peWXA4k9YihbY/Ujn+jB33dQndLfbrCIK4QzYrEtxqXnno6Nq+4FNW1IxGOczVeyrFGN0P/VBapBBlVdp58OhwwQ4jGquC5Ena+qMBoGHRLedh2BnAoJm3CMMJQmV0UFqN4K5kmlg7fd+C4eQi/COTTAp7toNjxHqA9edT3H3/8uzcdsPFEdmM7pl5WxLz3X4f6IdLoDDhjny4JKiVZbMazx98+Ao0LJ6XefO58VA05GD6LRuoapGZGWaYzzeFKaFXV8B1KuCk5eJSpShJUeXZUvaLFPdi5lPo4kqREGi5M3ULnpk08XDcIBTtfEhwEUDpPufqE3ku1gxsaQz7diXCICqwdUWha4wD6gkMvf/v7q57G2uYlyrPYU/zdy+3TV1SWMtjL07XDBQA7jszSEVsCtJdm2GuZ9Lrp0VJazcffvm/hH7OuAHAkqgfHwEPQrQg01yupscpFXqLapUyqEr2KlJSFJKpqa7hju8inMgjHk0o6ZlPtYLpQm2fIiqqcZ5KuLjmLSkUHOlGxxCMoFnPwcikYiTBisTBy+TScznYfxWIL7OKCvU+c/vQVd1712plD790EXEjz8akpFv80AVQD/hD6xpxpDX+4duqU7Ib/XATbq+N1I+t0K2YIUDGiQYFylapHm2hQ/V8CF/1fSSiqtPDhgFQpGzyio5BLCziONMJx5mby0CMJnqiqRmcm26vAuqdULMibsz0H8WQcTlcHXPiIWCZlCzleW0c76ve56Ve3zpk3RkPzNCKAJzu0R0l/P6GA942+rRw4kJ+2dEjJyaTU3JKDqfemYXx3haz7zQnHXeS1rv86jFhdZNBQq0hFPJoB36MxlrBJpS05hKhOVqUOcgY3k1GB5Fgsimw6BTiOsBIJdbVoNKx1bFoPFonCsoiwAvBcAaou0lUKJvmwiEuJ6k4LkL6jQrFCeGBcIB4NId3W6su2Te2At5SPPOSJ83/+6FP3nI1178PLW6njf6Rz92kAKKFMXCZlZM6R1+7V/OIDJwGNk4DqCXW77luTzhQMx3a5ZYXhm2Z3Pijl0doUByGpR46HkqrFuE+uCJCTjzFHJOJhdKxZRdt3DoPGt6G1tSY5doJVtP2oy3ROPqY+JlhPTqnwEY6E4Tm2+tEZQyKWsNvfeacVcBeeetndv5hyw4FvzWDlHEF1po90greB6q0DtK9fcwuJLrVTf/Lq5Id/fPRl0IcdzgcPrwnHa3i+GNCAco0IyMgsV66y7hRhAqgqJy3kYEXDVB9K2fM5cGMtil1xFJqSrH5MpKa2Xs1tLpenhC1oBvkIImoDtot2UORjGqoWlVRdMuuzqQ5l68ZryFRxkE13+WhvycIMrdFrRjwy5JDp84//86mrZqu60x1y+PTn8P4wNs1e5/ykA5Tuj7IIrOSxP94jtfjpi9C+6ECgriE5+qDqVHuXxiJxHosnYYQsFIpFFDIpgIicQ9HAqdNtB1GStw/OHEhZFBxF6WWJna7NAXJtux11yurascc++s+7f/YFMOsAML2BDxltSM3kZinJ2w5quLoTvA3TBHdsSCLnEhJOOgvdDAsvb+eQXvGfmolTfnPrsnv++j5c/h/6xG+h2pav2F90tPfdlN3M7MsS4c6v/vSiBXN/fJY+5LA9hRYyrUgVLzpB0jzT/FL4itBIW1PgqVVZD/SR54gRwxuwYeli0l1XYcykB+EXHKybOx7Aoajad6QRSUZMI8QM0+KuK1G0PcVPpupTXR9WPK5I2Ii8OxyxVG2q6zmqIEF6Hjy7iHDIEKm25iK6GtvAqx7Z65xfPnD2/Se/c/n9cHG+csFva9Psi5NtfX+nzd8nGaAkOWOQ0h1/9rWHrXrgrhkwzCnxIWMsM5Rk7U3tMBK1nOumKhy2iXojYiFZnYBHFBtdGUAjkCoBHGSCSVtQ2bHwM66UOR+tG5sgO/4NVL9U83DHk5eeAv8qdvAUYN23EBk7mifqk4KbjKk6Fx92uZqlVKOpSs5sGxY4BDdUFYvMFmDGq3ynbWMaxbV3H3XVXb/7+5nHN2GCyhktO0Mr4x8f2WQPsGp6r4GtA7Tnr1OlOTr64ti18y75IaR3WtWwcUY652pmmLL/fFXZporWS5lYlqTQlAYmOShJiwBkd7aibniD37bytTSM2mdx3L+vwuPowEHnjcdrc6YAyalIjNqNiJ30aFzTNIs7rgAlnNCmXCjYqnA+k8tCpLoAywILW5BU2ua5itRs+C7j0NHegnzzJhiJiO9mOtqRa3+2+nOnz+1cPmQRcld2AKW52Wmw2nkn+iQDlN0uZcMlYz53Eta+MRXRkfvXDBsfyeYFc3Iej9cNRTZng3Ei6gooI1XkgpGdEoRGXIfDggaHtm/pCiaLUoqcJ9ObUyiuWo3I4GdqvvLF5y774x9XX8kmpiiu8qO/LWr42dHx7wANx6F292GwIuT+ZUyT3GReBUiDXFJVuUFGFzRUVdWhq60TiWStSK9aWoT492sHnfmL614dfNAi7HFZDjMW92Vd32oocudN8zbP1L+TqO9hZRVXSnbyWiQXf+17J294+Y5LYY0fGR8+NpJJF7keScLLu0CIQlIBw6bFDQVODcQ6QTmD9LEgu1FIWXSLTYtXGRNOne0u/929YMxWYZvrn6vBPQ8diHde/yLQdCS0+PBQ/XDLCkUZcUBJpvFC0YVphalOQZkxumGoMleNwGlRSMaH5tqwhYd4LASNCdHVsVkqBofchlf2PvmKOUuWJV7DDy5pxowdzj7a5qDujC98QgEq2W0Sdf998FlTiq89czli1WNig4eHCkXJDSuJYsaBZUYhzQgcqjukSYlHYFkchUIW0rcVW3s0VoNUG1VyOSIUt2Qx1WojvbINSP9t8GEn/eOWlx9+6azZrBUzQGoOGU4Auz85dK/VRzQuvfEbqDryQGgWYVzjlsYl8yE9W2nNeiQEj3JNFX8I0ZdocEqUJ7QEucgLr/HVJiB0/54/evj3y346aYMS42VnkYofggWRFRXE/Dgk6cDz379/f7gwAAAgAElEQVQkDcr4GPTz7sO4+7++yy/16trPe0UvaSTruG7F4focHm1DxGfEXYRiERRpDswwNN2Cny8iFqf+T6T1ZISba5fIt/355Nlrrn/0OazAXPhql6XXMzJSP3/j2Ob7v3oqsu8cg+SIcZoZCvtFN2TEqjTdDHHXJ1vXUL4Hn+xSalsTCsNTDA8l9znFX4VLLE3gFN0SReGmOlPIv7uk5oCL7rz2xd8tuCTKWisyj/pa3zsDa+/rHJ80gBJHAjAHEfzw2K+h8a2zkIh8rn70rtTyh3Vt7uB6oi7gACJKS08ofiBlbxSysG0i8pKKxF3XdWRb2lE3YpTIZdNuYT0lV+eXITr+8bpjpz6/28PffPcVMNLBvJ6SReW1tDDyt+Ow/ooZwG7nWiN340I4lic9EtacQjdWiBxQVFRRJvoKEukVSJXNSyS4BV+2rcnCybyBfU647tA3blv4ClPXI1Gv2EfKiXg7med3RxdC/2ugUgnvbaMxShBhR//whOYlz/3U89gwSD1E5oZkBjShweFGkOBlZ6AnovCIFMIMKUeRVyhSeanynItizkV2XROs8b/DxS/8D25FG+Q0AVaRVEDSdJ95w/HerANRaJ4CZI6D38LMIQckKNztC864bnLKDqM+NJFYXDmMs+0dYOGIomGhNE5yHJEmRemBnByFvi2czlYHhdRriI7+Q93ahQ+31SlqlR5OmQFG8qOMh36SAEqqpHbULAz7+6XHHIvs8osQqhoHI2zVjR6ndaWz3MsWEBk0VPkRbNrIuQZJvKxSIETxMlODZxdQyGcU1ePgIfV+V/Nm127dTJ6jBcbuJz556KW//ts7+6GjcXJJpVF6ceVMSBx8M6oXfv/IqcC7V5vDPlcthW8KJrgOoRIXQvEYihnK6y0TSwdVLsQzpNLVSInzbRHWbZlfv7QNCN90u1z54CVsWhswjySnumbvaP+O4mqnfb9/T+6AbBVz+XUdU4ddMf6gSzl3zpPMiuhmnHMjAqIhJfWC6aGAi62YBcKmSu2TpH4WbXApEItYSLW3+uhqTUNmlx44/Q9Xv4YjXsfsxS4wuXsD635CKbXpQGz2LpfugVUPHw+0fQFITAwN2zVSzOQMYlVM1NVzCpEW8gVYmgFBjA8e9ZspkXQTZ69KuxbgzIdGqYQdLT4KmTy84r8waNeHjrr6oX/8/dttG4CJAfPZVl4fFUg/boD2LNO54IdtwLgFvzzlRLQtOwPwRtVOnBzp7EwbxEAlPB9GNAbdMFHoTAGhkKLhYLRT+x50twjbLgbEVaYpQsRsvnZlHkitBmpe3uOUn7zws4fPfv1Uhk5yxU5fNF3Onjy7bBP28VNKvfqgOw7sfG3mt2CNO05LxKPhWJRncznoobCqUQwWgt7NddvdOrc0rS4EYpYu0hvedWC3vnjsL1667pmOMa8jPdvBbJVAr3TbUsrrx6HeVj5zsA5Uv5kBOGmVajuNQ87FnmfccuKyR+64XK+q2Z8bUc0KJ+AKDcUC5Tkb4GYIQlUKlUrKyA4lJnvHVtpN2NREZ0ujg9S69UD9nx6Ub84+YzJacTkxRGwRMy4PFaZLhNdd1zr0mV+ccjgyb34JiB6oDR5dG41Xm7mCC7/ocC2aQCgUAYVmLNWKkShrgpfig2LkQZYKpNRCLh4xZfu6VWnkW1aBD/vDD15+85kbDsH6bcRKP7KQy8cK0NIupGOWxPdOR/yWvc87H+sWnIVo9Z6R2iGM0vYc1+fkEdQtE0bIVCl5yGZgDq6Fk8nAioSI4Qsim6YUTUSssF/M5T27ZXUO2LQI1fs8utuVbzy7cnc048sBi7lcBGAyUzx85VcvkndIjJiJhg33fPNkbPj75ebghoZkdR1rbe/g4VgSRddXzimqiAmIoMv52D0MAcptJDUw3/WdxpXtMIfc8aXlL9759Hh0ldSoylqOboH6MQI2GI4gXNnfhsEwVRqYCwOR/a8FK55nVtUmmRbmmhGB6zO4lBxC1UKGqexBboUgaNNUubguLApzGYxCnr7XuK4NWPdCaP+r7/z24p+8cT1DcaqEmBc0f+m9cVZmX0nJj1iL+ILTL9/Pe+2+U6Enj4ZljTOqBkPXI6zouErdDUdjKBQKpfkhNkKiDtVKAA28yARSsoNNzYeT6XCR2vw6Rn7+92ese+QvDzLigNqq4+gjAenHClClD0kpzvwtRj7wgyO+ikLz6bDCuyNaZVA8zc454KEIDHLJCxdWSIPNyZ5woYd0FeMCxT05R1V1NVFhikxnh+935ZrgL38h8oUL5uXPPOENTD+lMyBDVuaJKOfG9p73PvWPUlrjvt40cfXdk36GUP0hicEN4YLta4YVg2970GIJFKgXruKVLXMVBTy2DpmYUoNwNFVgnG5a7SL/1qsH/OD179vXj353SckW7U/F7dvQYacpsls/UW8v7kAUMrfIeHj+zycU/n7r9agePtmMVGmuzzhxjFKlJjFlq4QRw4BXdJQ0VSlA5FUXDkIWgTgr/E3v5YF0E3jd3On+OzfNZkSAxjBXSjGtVHYWzFPP/llBhBY482YjPHTpOyMbf3Ps6WDsHFTVDTGjSdMperQx8HiyCtlcVn2VXHAmOGxGXn1OaSoKoNQG1SlkELI4dC5ElqR6tvNthEf/flb+lXkzpqFlO3J3P1SH0scJULq28bqUyeMmnH5ay1sLv434oFHVQ0YZ+aLPhTShcSKd4igWMuR5oy0QLKpTRh+8fBoaJUo7RaVqEouVvW6dA7RsAHZ5fOwpd8977+pd3sV+1SoXluKplYwFfZ2UFRK0OxCPS1CH2ydfCLRfxKoaRsaSg1B0pEaSPRJNoOC4gahRjZCIRd6DozZdAigt2AjiEUpjaxJ+y5JGJHa/oe606x/51j37tc1kTHFnqsMr7J2PBaCVtKT9S1CGmWusLzVow56+6tRz0bbu66Gh4+uYHuYFCqlQ6p0RCrY/TjzCBmyitfc4QP1qNAFpZxCNm8hn2oRsb3Th5l+un3L27c3P3PRsia5EKcQlSAbaRTAY6vOKzhQ9+5qUvP5GjGy++exj0LTgq4AxuWq3vSLRWIJtWrOGG4l4wJghiXOKQKop8m1KciAGQaL6DFk6Ch3NkLqEqQmRXbOU+EGXJvY//vbTzr/ghW8cHGqdPHlyuc/pR7Rf9lzmYwSo1J7KoOb4ur3Ohb1xKqsas18oNoj50uCkfMpQHA7VAFIAwuCIxELwihnYVH4UJrpLSvVykaxJUn8QkV+3Lgenaxkw5AnsfcUL2OecZZhDaUOlHNieye81yL1V224lL8CNlMboE+87eO38712F2KjDh4zc3d/c1G5Cj3Bq9eDoZqBCqQgJSU/iMyH/DwlrHdIPI2RaKHRuFrC7UihsXDDlsj/98tn6w5bhB0EpWn8A7fvZh74qlH3ZvRS2VG+l1C5tQnV0YdtePzn9sKvg2gdERu0akdzgBaqvpRzoUFhR96o8PHKeKY5OKsUNQfcLsLMdiFaHkc+0+rKrJQ2n5a6fLs3fffVes9cDzwtgXvkx+6r+/T1+zz3Svb+BJI4+cwo6XvsajNBhek1tJF5Vw/KOq1I1FUDJPaSceD0lucpvIFxVbkh7fMTS4blF4djFvN/8r1ennP+L25+97w//Bu7vACbvSPH3Tpuyjwug7LtrZHLWly46vrD6pZksXj06Fq9jBcfnhhVFIWcrZwzXg+AzcTg7RBJNLw5FR8Io9zKfAg9pfqFprY3iqn8iesADdXe+9mxbHm2Y0Sv/tTco+wxfn0GoWKCSzZKoncGi3wFPXMJrRhpGuNqEp3Obh2Bxs9QOgZwg1LMzAGcAUAYrmkA+lwMTLqJho5hdtbwdqP/5bfIfj3ybMaIGKLOSla0/UsnKXew/QsdRd4elga5p/lPKkYcM/9qXWXHN1fHqeiudzoZgBEnr6qUAXtkQqvSeHHgWh3CykG4a0kkX0fXOyn1PuOTqz333hgWzv8ioJlBJzrLUHIjHaGvhqHOkjM7f9/LPdb35PzMA++jI0F2S0qpiQrNgFwq0YhBKJEAdzH2iBDUpkSIodlUavRDQykyDQggv35VHesUC1B9zEy5+5HXMHJjm88P0G+xUgG7jRkvXkrhMIrLk4pcOfPbOr/8IDJOSY3eLSGbwdCqLRHUdcrkifMeGlQjDzqSp6A/hUASu64EzBoNzOHaOgs6+vWlxJ9D2AoZ++XH817cXYPbXW4C1PqZClDyS72c361mo8+dH8P+uOQqbXv82YrvvAyNRo8cGc27EVGuSYFGSh1B2A5RYdimWoqvm2hJOvkBkYi6a11Aw/Knjb377jqeW4F3cvwVlZF+z6yMAaU8RaqDdbpEwweZ2yMTK+akDrj53wvlA7VFsyOhaKxrXikVyAPXoosGbcnMoYioJQfNs+ETpSyaKmxZOy7pW+LkHLpz53J33zJy/HphJl1VOoV4PW+kgKGdIVii3W06qZM9JJM7d4zsTG99+5KtA/lhtyF4jfMFMFgqplMCio3qMQzMt+MHkddf2knpO/WzK7TKkKPh+y6o0ZPqFvS+687Yhd532+rPsfZOSvZ81GOx77/vIfg7c9k4i2ejzkDQ33LLXO3+760yw4pmIVkcaxu7GOzqzsNM5ROvqYZecL1wHnHwGejisEg+KmQxMSzXfEemOVh/t77UA+X8C+/7pgvV/e+3eEYowiqaxnNH+fh+vcq1ol/xl/ejbvzLhTKDqXNQOHW5EBmlmuIp7Ba+7oVAA0EB6kkJFy95gGlg4RKVvwkt3+iACZu+9lRh1yu+O+fs9Tzw/jhUwFX5AudkzHz3doj6K7CJFeRCs1f4zmthfpEyePvLUM+0Nj52L5Ocn6LGqEHSTe5Qy1AugpQ2rFA8O8nGp4yCFpHw4+Q5ftq1YGt/jpJ89vuKPLxzJJueAxb3masAdqbKd6YAxWkkBgfCvD5q5/4rX7j8W3DoFktVg2LBINBoP55qaGeJJnhw0GKnmFlUorjQdlQ1GACVJqqxW5UpyOzb4cKmxk/mnw7736znVNxy/dD5jSpUrmUYf+gb6UQKUTZdSf/XLv91ryRM3fgfwp8RGjKvO20ILUQWETTacrur93EIRidpa5AoZlazjCxfS92CpJjxCFHIpX7Y15yGan41M+s6cKYuufeVRtjgHTCoFuT/wY/Ua+Oq5Mtn54/MOxNuPXK0N2nOi0KJxAqjjlR0PZQlKlk4PQIUnwUwTYYucI9RyL2+jY10R0OYlpj5wc7ooGzF/MnmaSDfulhkloASNZ3asJGq7N6SKnjZ9j+nrPzOGXioTjTfV3QJEDtcG7V4frxmidXV2cWVrdgM0kEZBaV9p/H2SUlQY75MUFV7z+hy8zY/hmCevxvOHtZSS1HsRplXeTD+Ooa09X7eGNn0Rwm/fv3TcS3ee/xXw3MmQ/lhU1TEeioYlM5hhhjhxU1lUaNHNdyxVzxzVK1URWXmIRzhcO+Pn1i9rg1774Oip193lHHHq+lunwyZvc8nT3J/NTjb9TgHvB17J27ki6DqRK+amGm789vFneJvf/EZsxL7VPkytUKRuQwa4FUU8VoVcvgivKwWzphquZ8OySHJ2QTc1DK6rEq3Nm1y3ZX0X/PULqo+48g/73PjTV198D2lMozzXUj++gagee2627yLc2iIFMN0Y/6tZ9asur7oSbPB/ITGoDpEqg+shTuEF1bJPNUpRrohAgtJ61QzIYhEW9S1xHeHbBV9mO7PIvfGGttuFt1248PZ/zK5RalNlb5WPJL5WkgJle7cXLireaJdLGZtz4EVHNv7rnouh7XEAInXxeP0Ini0E/rdA7lOyevm2y4Xx9AdidXEp3uiTius1vrUag/b93ZSWp+95NmDd65aeW6zmQGJuzyKvDHME/w8Ivow9f48Ry87bbRqw5mSgti46br86xxVhN+ewUHUtt52AncGk/juU4ELJFCVmR8rbrYqH0NnWLGBnJdIt62E13HnXm/95+Ou7z2gGVJJL/xlH3dXt23X/W4XQRwFQuoZ+9pxc3R8vO3oqmt/7uj5o2DgrUqXCKYYVhy801VOTKkNowCQRk1MDHy4QjZgUTKaGuAJ+0XUal7cChVcwdsoDD6x+5N9nMtapXKgEzu5FQjIoqDzs89rahJe/3P+OeC3C+OkXpsL/1zdQt//+TI8BWgiM6F+7nSPlBn/B0iXmwHxXCswwIV1XkLMoETH91JpFLajd9X8ue+C52TcuxCbMVLttpZeQfdgqlBLOgZEz8JhIqX33L+t2vfXsY3+AfPPRMEfXRYeMMW2PcaqBpRrMUp/DAKDdEpTijRKu5lNHOAGvAKUWdbw9f/fTf/+bIQ+e9PqLbKIAVvRa4GWprm5r+8BZaab1fQ6G5dIY+tjCsY1Xfu1IYNUJwPADUDuiWg8lmBWOK62N5i5Qast7RU9DJyZ8UMPJSEhHevMGG7n0W6jb7c7/fuC5J/f4IjrOp8qb/lMCB15L2ynRyl/7YADd5k5RmrEjXogPcuce3PrKn38AzTps2MRJ2LS+SbOSdTDMKGxHwKXANsXOiByKqhE8R7VRJ64f6qSZ7mrx/bamdnjNzyOy68P35ha9cgFTnrXyiHY/ereKGNhUA0mHvkO1jUGVxvAzXt5j44OnXsLqxp3LjaggFzIUQAOG+rIlF9hzFBoMK6+h6fmwPQ+mplLJUehoysHOLa078L+uG/7z61554wjkggXeo+p+WAAtgUDFFrfVsvE6yMTNB511YsvSV3+OvB/WB41N1g4arrW0pyGpp6gqYC8VsW8BUPJqU9TRFT5l6UB0wEvccsEzLzx476HdPVW2FJzljSMIgm5bgm4xxeVpVVu06mj3pRdR//SRQ6YC7pmoGjshWTPELJLfSlJ2ddC1TRGXK8d/qYOdDOhBo4kE0ql2WDpErnmDj+Lmf1QfdPGt2g9/vqDtJJVg0S+/0bb9MduH1A8ZoCDeWp48/bcTUo/f9GNIdkR4cEO0kPd4oqYBubwDn2xPyVWHMOXxtF1omgYvn0UsGVGsbhrzRLFlYxfsd17A8BPum7nh8YUzAxWJ7HqvLDh7nHzBw5eBWhqKbU/2wGOmxmm6lInZVSecDnfd9XpyMFVvWEyztACg5Wa3wdavWssTLWciqRy4VMlRFYuhY9M6Ea8Oy8y6JWkkRt32ue/f+dC/rtl9bWkxlqXoTtuB+3skGdzswABVG+9Ufswt5014/hdXfctgOEfIsK4bSWbbVHFtKscdFcYHTl8ime6xQRWxNyk1fk6YhnSdzjYf+dwbo0740Y+Pfuxri+6dgSJmD5BGp65dmqrtAWhJya6Y48qxC/4/SepDD13c0HjbmWcC/pl63ZDxxGMlmckJoIJqSRVA6brljuZQJlemo0OZWaSqCzcn3Pa2FJzOJ3e76ImbVk7dfzWOVSAdQEP/uFXcrXKxqo5E+pirXttjzXVnngtmn68NGhZLVg/SyGMLZgXNbiktiOondR2mYcCxHRhU3xk2USymUJUI+x0rKZSy6SWMP+fuWe/+YfEMxoiQuFtyDoS8nVzGpQo/v/4/2Ouus3a/BsjubQ7fY4hT9M1oVR3PtbRDr65VFTbUnk81p6WJpfUmuFrDnnLjK6+mEG7GFR1vLj7o3FtuePWXFzyPBriUo1tJZl2WojuzcqKSa34r48Mwa1a4/qF/ndz8wqOX8rpRe1dVDUUuL7iACTfvwEwm4ZT4hAnu1N8zqOQJ6mPJyWJwR9iFrrxseasLg/f582XPvfibG/fBptKCHijw3+3s6W/hD7CHVtqh/X2FYZHUv78eY24+Ze9TgM3nIjRsFLSQridrGDSde4J4kk345Magml/doE7LiEZjcN0inM42RGuT8OycbzdtaoIo/uGnC9c+cPXBw9ZBbtpRfqPtE587Icwy0MAEszQXNbj48POR2nAurPAYq3aIoRlhbjukx5iqNebg0WPQsrmZ6oQQqauB8DwIsm3giWTclK0rXi8AuWe0SWc/4MfPXoAXJytwlhdyf+DcycCsGEzJcD7qcN+krwKvnwh94j5Ww4hq2/YZN8OUs6/saMMMwSFOXsXsQC8DFnQVZ1OylnlgoiAKm95uRWTI3TNzC26dyZAJ+rYo1oVej/URA5QyqDhuerEa133/CmTSZ/LkoEGWleRFhyMcTiKfJ8b3kFq4luIvEXBKAO2OgxJbgtMh4WTbkV7y1qgTb7vplMcuWXgLm52BnF7ioBlgnZal6PZL0O1a8OdJGXrskPt36Vx43VSg8SuI7j5MlSZZYUO3wpzUdtVHROMQmgE3lYEeiqi8XSeXQSQeQT6X9tGx2UMovBwZffZMufAvM+ehE9OUfvxBtLR+n+GDqbg9p+wLVJrkCPb6/qFY9thlQGayNmz3uGFGueNKGEYUdtGFxTUIw4Tb2aHY3fSwqWo5qVzM9/LSad2YhdP1Lqom3HFp5wtP3rQCaUyk6mx4aiT6ufudCM4B1EwZwqg5Y7DuaxcB1hRrxOFj7VTOTA4bzVMtHYAVQZgS6fOZoLeI0rVJSzBUQTfFUxTRNWwU2jbYKHS+uttF8777g7v233gRuzYLzNxmLeJ2rcbKLymvZsnrUjFmA4yVSnHEqT/eB4/ddzviNXuHk4MtUSAPp4lE1WBks3kwjUP3KSFewKEChm4HXaAtEEewTG1wkHtvA3jDk+f77153H5AhLWGqlP68oDC6/wUdpB7u6GIfyPvdy8t7npTWvMm/2SW3+MbTAfcCbdAYQ3IjIXI2Mwc3cCLNLuYLQTKDTeRjlLlowPNcir/DsQvCbW30YTIX6Q3/0A/92a+8L160GDNVfHRH73mbU7mjAN2KxOy+OZWIefBP3ttt4c9OugzwjkAk1hCpHazl845Kj2OhOIpdaZiJBJymRvC6GlXEm+5qQzhsUBKz37VxTRqFFe9g6KEP7HnFg08v+/bTTcAMF1J6kgUOmS3ddjt1gAaYcKW6h8HYqQBOhbbnwaipq45X1wek2VaUmP3geUVYOgGUKnEMQFDTJWL/I6L0AKDwc76X3rQBctCv/1l88alD2ORGYHG/E/2+Ww+Um/X2AeiAG9kkaVw4H0PvOeSIU7D+vf9G/Yi6eLyOZ7oKsHgEZqwamXQGmqFDSLJBiVmi7ECnIWNEkSlAiM40ZZFbuvTCm17+zT1vHPYU5pDXdmJfj/WWi7SnO/qOLPi+a7nHJRG49IN/QblLMvzSNx/a9+93nPFVoOZIGENGWkNGRAQLMU9qnLQgqj/WqLua48DXyZkkFZ+vZRE5XEHYmU4X+XQKxewT5tE3377XnBNXLR6q7OqdusHuGEB7d7ceSD/R9r6seeSSB772VTSuuBQ1I6JGLG5wT3LblSreyTUTGnk2KS3atTFs5FA0Nq6D9AqoG1Ttt23e4KJzw9sYMeaRPX/0q4c6ph/V1Egt5STZaSWh1M/Vt/Knbe5UW7Fteski9Wa6NJJv/+bzqZeu/CZQfXhkzF7RogNDCA16KMqJ3c+i4BoPQpwKoD6FkAioDJYQ8OAoG62Yac0h3fSvs3713I//5/HxyydNRHFxHwdKZWPjHU5cKC32ssO9UuvoF6RShgafft3hLX+Z9R1YscP0mvoYVRXZeQ+6EQdnBpxcHmY0DNcn0jYCJ0nQAA9kwgmfCciiRNvqNnibH3xKZu84nk3boBLiiQuFDhi4IdP7nasgrbb06k1YUbGbl75x4XJZ/dcZ0w/ZtOCuS4DaifV7fiHZ3JK14GqsZsRo3tXZBenlIJUjjAfsHb4HIxxGKGSJbKZDcukIv2lZMxJ733rhfc89cs+pitdop0rS7Qbo9rmNlXQxdp1yzQnvPDf7+4gOOtAcNJzixho5FqhpLpUHCttHKB6HS2TPGpTKl+9qQe2QWj/b2uTa7e/kYQ3+4/FzfnfvUxNnr0ThPYnJi8tUGMFC6DONO1G17T5zd3ZZTw1Y6bKLjAtenzTo3v33vRCs8K3QiHFWMe9EtUiSa2ZEOJ0ZsGSYS58YHal+lVLKqBzLUt0SKXDEKT9V5uEWO30/tbkT1eNmLmp+4tHJk9GGxarhJzEvqet9cIBWBBW3quJKFnsJddmvHnERCi2X8lhdQjJTky65aE3oRgweed09H1aMvLg2JNnT3XzBTAHUoO7YxbSN9jfeHnbkhddNeeCGF+6bhZSK96pkEqZY9rszhXpQtSMScwsgy+4Upp6c4pL93rOrl59/LvgDU2X1mcO/cAI2vXMeWOxzbNAYI5qoZ0VlUAvuOykV6qMCHSKhK1L+rhAIx6LEvyzgFySKKRfZroVVk076zexFt7wwjbG8BIgM4AM9S8VG8743rJ4DaZeeAR1UFr30V7th/YPfQrHr9OTYiRGhhTg1zaVSHysUhW17iqbSiEZBra4pNc7uaIYet0Q0qjupNe+m4YsF1nGn3R15+sZXOylBeZLqSN0r62RLo3fnDEgfcVkZwelly6BRhjHl2qOw7A8/gFW1O6rqqi09rCESh50ixnQDtigBlMApwwqoljSUHUpeTp0VYefaBBdZx+vsfPT0y+677aF1h/1n7jz404IGMh/cWVRpf9LDbQ2gi6SR+MWfJqUf+e8rUF97PA9VMeFrHNSozIiCS0oy9xTVDIVBA5pTH8R22CNBWQDQ9uYOFNc/ds4jm26ac8qc9ZDnEJU/3YHXzfNSjqZUDvr2JyhsFaDBowadjYOQah9ZFCi8xoNS1p3RMOUMbF5xsTVoj4ZI7XCjszWj6eEQpN+FSNhAvmAjFI4R5RiK2Zxau55bhCykKRToZzesylNpxt5fuuR3S9bvshJLji0OmAa4g3Dbbgm69fNKhkUIf8PB0DuO+8IJSC+/hNWOGRlO1GvUBoB6pBDlok72F6kLjAeeWjuPqkFVyGe7hM5dP79uhQ1kX244+7e/avrq2W9hMUvjGqXWlmqaerpQfxQStGI5V4ZYS/+fZUyaeeq4xTOHXQyETjZHHFTvFKHxSDm75TgAACAASURBVDWxOgIhjTONVFwPptRAlNlUIxqEXChBW0AjmtBMByzN84vNG1bWjDno1xfdOudPf/sJiosXE7PdtF5B8Ir82a3uzt1ZQhWT1n3AgACVDBeienzjNV9d9fQdl+rDdh8FI8IptQL0Y8bg2MRlK2GaJjlLoFEHMhWnD/KP1Q5G4sMVAs1vvIXQ+Num/vOlx+bthxQmQ2LsPIF501TxdcBlHNxgr0X4QQAaUIJ2v4L/9l7ilVSK6s8z5xrDO+O7b7z92uMgNp8NrX6cOWxXTQpP48V2TuE+ygbT4lUwrIgii9PjcRDdkU2kAcyDn2nxkW58r3q3z937p9fn/3FKlFH/CXsHwkQDwmsnARTGz6Wsu+rIqw/Eiz87FzWTDqpqGFmbywlNiIC7xwDxwpRnJegXqekS0i8gbDIhvKxTWPfKSmuvE++1l/zlPjDmYvokidndZM/97ZjdqssO22bbt5OVx6cfgEqGmS8m8ZdbD8Sbj18Oa9cDE6P2NHwWY7m2AtcTVfAdYnF0ILmrnH42+RDUKtYCj64kmkry70qR27gqA2Qf+f3y1T9ZU422mUNZOQmjB4zEo1u55AZYzOUSx0qDrKxxBcdvWVJGnts9r31592Uzz7kaZvRIXjMkSbnGKtNGcS8RJ3+p2FmdmGQEJcJLcFM5ThS7RSQUEtlGYkxwH/jt4qXXfXPm7EbMf94D5vXO+Bpoi/kAAO2NxnK65zaURXq4adfForscMSr3i4v/H+Acqw8bP0IKzkIGkEuneCxZBU8wxUXFTROCyuxMDfFEBJnGdRg8cohoeW9lHtmVb+1yys9/dMrDl/37V4yaBw/oiR7I2brFqtxJAJXaOY+mJ88566hpEIWTYYYarPoRhmtz1SeyG5zdvtdg1+WagKn5ws11un7TqiJig3+L/Y+fi5f89yBvIJWof9a90mOoFu+l14cE0FJ6TK9J7rnoJBhwj6jHkte+CcTP0YbuXecjweGFWbymnue6OtU+I7Q8TF6AzWlTpcypUjNg14ChhQVRcGQb12XgrVyEmkPv+mv7Y387DiylxFL3FtSPOOjHzul2lPTxmGwLoDdKWXsZG3wC0DUDyf0nmrHaCHWMo6QjSjmiggC7rCaq3xK6qcPLkkc3YLiIWgbsXNa3WzuaqsYfcvMTT935/L+X3b/pe185PzdgO/qy43GbaaPbsaN2e8IClotem5k6fB4HpvVUz/R8QVdhQf2oY+FvOANadAqq6gWDtKQvmREKKSIyIl6VnJcYC0nIeLDCumKLgLCp30gTXO2Rcx9cdu/v/4S3Me+Dx0Y/KEAV6RfuRzJyx5ln5v/9zJlIDtsL3IxYg0dw5Cl2pvcQPKtBosC2opoQkI60ZNG1N62xAfefZ8x7+8p3To2uWkwGw6JFDiZ300z0us9KYH6sAAUY9cdsO/D0L2f/NXcGqg8+BFadwc1qhKwEL2TSKo/Y0AicRYB+lNOIAEqZKlReF4ImpJBuznHb3mgHtL/PWvTmdb+snb927ZjzaSfrm7hQORb9yqGtSdCSUrmFhXD5c4v2/NWUE38K2b5/YuwxQxyXa56gRHK+JUCV5ihVZ7dCZxvlqUIwgUTYEu0b1jko2s9efPMbN9vjQm+M5LP9mV+eQUnl/ffkrARVoJG+f+dKL4D2BfRcPnfuVAIppk0rgTQAaMCFJKWOGzASN5x2Alr//Q0gWYdIworU1Fj5ossson61iL1QqPg9Vb/AKaBmcA06Nq8ndV9Yhijm1y9aPviwb/225eVfPYIyUfkHcBh9UICyL0sZfvrI7+zvvfjgtxCuOhy6VacPGqqZVgL5DPXoLDXNLe++NByUCwdPmob0nI4mjq6ly4cd/8NfTX3ymmdvnYY85gUJnqVsoV4b4UcLTrWhdI9Rv+qhlNoul/9l13evP+tCxCZerCWHGWaoFoWcq1mUW0w2qAIoccYGHSaULao6foeAvAOu6cTv7Oc2LU8hZLfse9zJV+GkU1984/wjSfJUlqL11de2D6Ddpli3pl55nEpMGHXMWVPX/e2xb8MYvnvVqD0iXV15rpnhHvMkYH0uaZF0uIQZMuDaOWWHGUwK5jvIb1zVBn23X9+45LkHL5uLJjROLpspve+15LzqR6fbOQDtM1JTp5YBqlT8QIpOUzzA9AreS2nte0Nu9zf++9ALgKaTrOrd6qyqOmRyeYPyv/RQmHuOB82iMDgNWxArDZkM2XQ7TC78YuPadiD0dP1Zf7yl+fBJqzFDVbz0l5SxXWpufwAtfaaW49YHS0p2exsaLtll34vRtea08Nj9Rha6cqZVN4QzZoGY/akIW81sSS2inE3FPCA9oTPXd9atpNL2ubh55S/wfcVPo/KWS8ncJZHbpySlQrX9KCVoMBjKD1E5LkbVGhnt+tyXTkLH2uuQaKg2Yw1wikJjqhGmB5MXS+ot9XLxhWJBk1ROF4HMFKGHLGjSFcJLuW56kwvh3/Ttp9+4/7bvNG3Giu7AfnksAh7JnpnrNUf9qLiBg61bXe41r3QW/cTHWgc9ftJeVyCa/JpV1RByXN2Qvg4FUEb+A8Aug7NsfyoTBbCI59bOAl5BeO3NLpzO10ecdPcPN5gnLUE18pjd3UohmKpAiAUFswO93o8U7SuJKwE6FZwuO3duz0XnYRqmsXmkIlR0FpA4RiLx/D5X7o0lt1wMfdwX9bohSY+atVkhzrkFx6NmUDo8jbRioXh/wxETItcF26NJh+u3b9gIa/xtM4vPPTRz8eQUJi+mu6kshCgLnW1uRlsBaLCnDDyKQcxzyiVzDnt2zi9uhSuGR4eOjNke0xQTBtH/UzI8aRBlYimiOoSAS3PmO77YvD4Pr+25SZc9d+PiG29cBsxxp8+CnD1DefjKVSpbmhKlmypL0w/P/gwu1B2HLI1WHycLqfmoO2f+Pm1//NYvgcQkVA8Ph2O1pu24XDIBk9uwqfiGqziaUGxokjKLTDhSQ9SyUMilhKnbstC6lrh7/nbCZdfftOiL5y9sPnaxU2KUq5yPCqneu/FSd7+XnpnrA9DKOZUMl7+X2GfNHw96c+5N1yA5et941XAj057h8foRyOX+P3XfAV9Flbb/nJk7M7clNx0SQuhFkCJBUcGCa1sr6ib2sisGuyIW7BEXRQVBrETUtaGbrL0jLqhYUGJDEKS3QHpy+9y5M+f/e8/MTYGAfvutH/zv92UBCbl3zjnvedvzPk/MLhDRQHObgdreU1RuZQ7VxWHEQ5ZFeVhw0w64CqrmGb/MnMRWNaJsCHcMtGvWhD2drv+mgdqXE0oqIVWWcJs8sASoqipFaSkBJxzv2W4J0iDOvWvY8FOB7VdA63kw0gKy6k0Xw/myyy2IMcSZtmjm14tYYx20NC8sGh6wkqbR3BSDoX89bOq9tzXeP+GXGipE/AEGunfjLINrhP7awB/fnXkOs4I3eNJzFF0ATd2SxRQq0oLJRGxIoYD9o2zeHirVGxaiwSiat61Gv+Pm37eu4s3bGEvM4zxaBkaG6XKavZ086P99eLuLgToXQ1cXwsDlPOfXQ444H9baEqh9DtRy8r0mpzIYjQ1QqJ+ELV9IxQW7iktTaMSvSyppQpFNSlix1p1AuGkL8or+geOfmY+Xoq3ABt0pbrTFqB3dqePXxd/tkn+m2lP0V3YfpOOly7mSc+YLfRoWzfkrQjUXqt0G5GjeLDnUGJZyCnujqanFJn0mAxVRUOod6K2SUFQGI9xseXwKj23fEINRu6zH8VfNveyjB5eWExk1KZXZIPLOr/+297Q/VruJ2e3Pjn/u8ODckbgoRVVJFchGO1Cz2p9zOXcd+cF3/T+78/wLICt/RXZOmuTS3JZhShm5+Wipa3YkLlS43W5EmxrhSfMJCs9YOGR53JoZq93YBCRmFb/6eFV1SfNOMNEy6xjqtl9De7mQ9uZB926gJVCGRaedvuK9J69VuxeNkVU/05NcIuKDjPyeaGkJQZYJr2k/s1CYEguZJL1GC411Tcju8cGCbW/NOI9VbyEtBsEasgvDm2jMtOFJ7R/xR3vM3c5TxxnFPb0/56r6508GJz48bwq0HhMgeTQ5PYeECyWTGxZVbzVZNEMd5jsZum7CH8hEuKEe7nQfXMy0uBWVIk074oiEvup38aO3rH9+03rMuz6CMoEKaE8i2+KKtsCb20ia3xAybDsMnOEheAcvvn/86k+emgbuHRwoHOBisltuaQrD7Q9A1ynT2LWCS+9Hve2kkHvUd262MnrmGS3rVrXCrHn5PW4+ejJjTSip1FFZQjlO51Zk6hqxd3I32/1fFYl2vwg6Gam9Um1LSNNWYJSH7k45o8zjPH3ueS+cvPKDp29U0tK6GwkjA+GYlNWnP5p2NgliAbfXh3hrEP6MDETCQTH1Yupx+HxeK1K/yYD+46cTHvzH7K03nb+0urMXtR++/brdo63ttkKVlZWyU+Xq+h/ZzWAP/gwfPhs9FdGGi9z5PQOy6pMjoRjcGdmIByPwZuWI4WvyepRwkIHqFlUwidzYsNDS0IrsXm/f9N3r0x/qhh1DOJKrmGD6EK2VVAbdxRb+Zty++67/r/9Lh7xcfLquPgOFugrko8tg/TTV3X20Nx7WfVpWjkTVPpffg0hTPTxZWUiaHEYwDE3zggviKiEHD3DqLJGR6jB3bm6AVjh9dvzL1yYzNNog7E7vm0K/7uKddnEcu97ObRHxUKWkcmX3qolHXAa98Rp3doHX5ApTtTSJWOEtcbPat4DtjMhQKdS1DVSj+0KlrQxbRrgpipYfN/71vgWz3zz43Debj2N6GV9uVthkz7/vlfJ6/0l4u6d32EtVV4h8AxKvhMVKHd/B24bY5Qs5d6+b/NVxXz1z4/3gZqGSkeUmPLnkSUOsJQxPZg7ipEWTuhCJcYcuLm6CUxqDCLciNdsR2bqgnAdnlzNRX6FQd9dqdmrDujzXu57/3yzhU1XzQg7vW8OmHh1c+fqNcElj1JwCMecZ05PQfAHokagYxGa0BA67QCo8EiEueVASXmmoW5Vx4o33ukv/9tXOv10UBV5MOKGtCHH3cMfuCwN1Pkq7A+viTNhzlNnnH4umD26Bu6hYyyrwuDQfi8Yikj/NJ2YKmccDPWFA5Qw6Nb4VVXD9miaRa3HIMsUMSSTCLXE01y+94PHP73npqqdXgd9No1o2I7ZjOZ09gvOJ2ovOe+L1EYUhypsHl1SMX/32Y1NcgcA4l5auJi1JohxLcNolKV+xI7IU55IgR3MMVFSnXURVEzStum0N4MEPjyt/ce7KspN+rSlgyTIOXrGrCNLvM9X/vNWyay91DwZaiRJJtFvsz9M5BLfvJNdNb/Cih0rGXgaPWQZV9Wq+gGwkLcnt9ot5WJfiFqLBTtHQpsQS3RqS/SDIcdzioa1RRFurMXjsjAkfvPrNm31Yqh/cyZWndq6r5elokLsa6x4Mgbs/4sg/IeOIq9C66UJkFGSp3gwpSWJBkEHzdHR/gNSnXYIHw06oaURMSkm/0ciDYRlb10Wg5Tx/VdUXFY+f9tMmYITBK2HSjdZBt2T3wOX3bvT/+fdxVjYXfSuu7XsJoE9Uuh+QSfJ8ejwmqW7aUErMmWA2d/nThJgtXFTNJeocGmdigu6FCt6WQQrda1t6HnnGXSNfmVX1TgGC9jRPWxm1w/RGh61Kod327ImI7sQ9l3PvtXknXIr6tTf6ew3xc6YQiRuYTOLHCpihw3DodjoZaBt7YVJQ0RhNNVFEt65zDzvp0fG3PLrog7cur8OGKi6w0220f//DjfjfeNGOE1ftBrvbB6gEJMI7dyH94UIxR3//9RPWffrCFGQdOIqpbqapbkkXOGRVfBni92K828b7UpQo1AXs3xMroBVtsNBSWwvuefZ9vqriJMbqOjCBdKonOES7u9ncngx0T8bJ8AK8g197btzqdx+8B7CGZxQNUlojusQtCYzK8uQVPF5YNLZCYZuIjiRoEoMh2RIgErNFVPVoq4m6LeuLjrvurisXXrdwqo0cSnnRLpAgbeu8r7zonk5ayqthHufemwrOPiK44+OpUItGISPXK0mypFgcSTr4koRkOAwlkEGaMjaIQ3YEfSRZqERziwlyMSO0I4Hmxjfn8/W3TWTjW8EXi4HnjjeuXbXtwkBtn995nezDq4zcyL0/XPVYT7w/axpk98m+HgMZ8fMQKFxSNXGfUsgtNDQdD5qC+9GPpShIggmXlDT1mg0NMIMfDrv16wdXVAe3YuEIo3g5ePUDsFC5i3f6vXb6vzHQ3a/zLsgd27+pk4Haf/AcO48riyb1nAYo53v7DE83EoZMmZ1sWiAj1dIzoNMwd8pAHcAQFdPsvbBDXSiypW/fFEUi8fWYS566+4hzj17xUCvirL1w1jkLtYGcnfasKwPd8+HnXH11LfqdM+SQiyBFroQ3w9OtoDer3V4rQfUILRK9JQjmJzCxBDkRgyGODxkms0sYHVSOCYMbXPdVK9B9wdEP/zB3ybRNDWg5Ocr5SpPtLkX+O8Lv33sC/sDv45zdfE80/8HywVcA/CJk9i7wZWZLkVAUquaBqrlhhCPgbhXMoAuMmr5MoFPsnjERbrlsNrzozigavljR45ip87fnXbIQZw6qFfy/7WGu40Xbt4y1yax02SKjNfS8x3nuySznaEC5S8s/MD+RdCmK5pcSCRoJJCZCHRpRTYtpI7oPbERRihjNzkpNkX+ibvsvyBv25CW1b7zyD3ZPtHj5O3x5sej7kXzgHkmpd9uB/6ZRdv7hu9YP9rz5nEunvoLMJXeXHRpa+4+b4D5oRE7fgb6G+nqJhKNllwt6axBKesBOAehyFWyjxNRkMxySArs9TSfBo/isaF1tAsGfVwOB+RWvr//ksiJsZKPbdIPaN24P/F6pQ/97wls2YTEPGJWvj33vyYlT4Ckc5/LnsLSMPKm5lihL0uHypyNC6tfkKVwSJMuW4rOTodQO2L+jEMAlmdCDDQZaa1dnjfzz4wu+f/zdG1ahcdVQ8XE6TqZ3VW3e9160a8AA4zXc4x5+1ni94fMp8Pcd587OZfFwTCBzvB4fYnocsplEUpFhUVGB9pRIrol9i1NaoEBiSUtKNprJmhU7gLoPLviQP/yShm0YL5ApqZcznNGFgXblQek4PTDIP3h76cDVj1bcAa6Mzxs6Vq2vC2ku1QfDoDFAt0AHUY9ToqIV5VOdDNQe4oKVtKz67QaSyXdzz5g7V3n92O9q2NDkcj6Oj2YVmFcGZM6DZY/N/Y7XH1EksgPQXc6Ocw7bGsZtUD86p96fOO85nA24FUie6MobmOn2Z8jh+gZogQy4vR60tjRD1TQkKUIUNXmqnVtQxUwsXZ2WKKSJYQjLA5PyhoY1jcC6N4cd/OCrP91001fMvmQ7o4tSZ8n24m0b+vsNtISrEw7e2vP9uRMvTGxbdoGSc2ARZI+sutMRIcSQS4VL84CgUHYYbkKS6F0FWYQI5WxPKi5X+wbWw0jzq1Zo64ZWeLxLT7jmlWn87P5rFo5YYqLyaAOlbTORXRnoHyqN8DuOlG0CHYwl5dl4JZdHvPhN0U/vXDMZqnExy8rxcsMSStSU3yWSCXAjAZfPjSSN3REHK1VzGc2Q2sPRisLBjAYLZjiUqFn6w6DTbn/mqKf+/lFFgeACTlVH92ygXXmkeVBml63KmcyGHArQcNlB41VPnupSAhJtGx06W3M1ApniNTPm7J890cI5pa8O3YxpmLxuZyMKD56bXzZ/wY4fscOBaLatSCUpZqOSyjG/z0i7vlR+51Z09W27GmfblqXqa/QwdDBtgPSQipz+WRuPWffFq/dr/u7Zpi9Llbks6YYBt99n6wPpOiSZ2fsmxNxIh8ce3VXpV8mudGvEomH4aLbdMlt2xBH7agWUIxbMvmrxS9451ZEyjO481L0XA/0t7yT+/vid3Fv0wk+j5t884SrAHB/oPSIznrRkyeUBgaqNCAHBGeD22neDQfxDNP9JH5x+pSIRUUfY6Ro9mxELw3YiMZ5s2tnYd8xp92qH3fPemJnu2n+IEFeQ3nROptudh1PL/B+TS/0vNrzLf9qxTN5WYb3xI+6dee6Ec9H03c3I7FlExSJNS5MMEpy2iO4lCeZ2gRPFI1mookFjLugmrZEbblVCrKXGkqVEwqxdsRWeXoue++H7x/46aPwmYInZkXHhtx9IJBfKqZy7HgQOOICxc4HAhIwBhxe0rN2m5R44Gk0tIVHEolqBbiXgcpNCeNTOQYVh2sYpfIaVtHioJYTwr0t7XfjqHDbt+O82fYsoStv3grprVVVAKanM/U9e//VQt6ORdnlWnP071XP/1rcLbu3Z/w6o7lIls4eLKelyImHBpVBtwIJhJEQhj86xJYghnPWgkFYYqs3PZE8sqZB5AMyk9pRuRrb+2AA58/3jJzw788eleVtra7unev4dI0Eni21HhznvsJtKwq7hIzVuvdMPmz5hy9fPXgF//lDmS3dLqiYRU4L4ZvE/NttmSicyhRwiBA0ZqojNyVC5JL58bh9a6+rhdisWT0YSetP3H0+Zt3nmrOK07zAJBqqFbHXqp6c+q1MZbrOFfR/qdnkAOfvXGgz6y6D864DEuYFeY3yRuCSp3kyhfRrcuR3wk9d0iLeEcA9d6C5H7YXoQzispG6atZtaAXN93iGXPmCMvPHfzRWCLLkrEP1un8TRGyU4LbCSY9C1s0rWfHLjRKgHHaTm5Pm4JEvE17t7TGUPldPFQQyMsqQgPc2P5u2bSZ4lGt1YvRG+nOcv/nTpm88Xr9sBNjq5nBfz0UyoldmGTOWursAIu6/XH7uHe2YJbHNOgzj3rxlz5Zn4pnKK0qd4IIdfTupuiehSEwTTZMQbQDpX1Oe0z7kGRdQLxJkWhknfR182g4iKdCSZAjMastwqEvHtK1ZnjJj099k/3L74r6yt5dLmgATgzjnuKbjJ7zPQEi7fdzYG3PaXsVcArach0C1fSQsoJjEj0NbSRrb9dFK3IiNNMa3bytOac7MYQj9ShkRzokxDLBiGR3MhEW81zVDthvT+I58889cXn/+HmGpJ8fB3wFyIne+kRffHbu5v3/4dPWin717JedZQVngaYE5hgZ79vIFCJRJMSL7uBYgZOqwk9a4FQb7d/KcZH8iQOU3QKtDDcXgCASvWUmegtSYCpL1QNnfZEwWnYnt5H5HH7F312Q6b7F4ANspjy3tnfVHe507Ae7bWrbfXkplMHNOmo2dqf59IpOzWGA0dUeuMKVAEMQ9hMRJmonlbGNGNX+Lgo+ac8M0t332Ew8MO3tWsSjX97S37LQPd13tnP/BzXMOnC3vjtdumQrZOhurNkDMKJTNM0AyXMDiaRNJIzIKqs+SULZpI0sAY7RUZKHE9E0bXHiukFqllegQ5O039KMwyI5vXtcKbu+ChnxfNuekjbMOktnnntnXYlfurKwPdddFYCefKjrMeP3np61dPhDRqDDJzA4rHJ5nM0fHqaKApD5qSoRPxuQFN/Ep9InocGcxyQWYqLJ0a3kA80mRxKxTh4ealxXe/c6Nx98CtP7GKBDCpk6hQe4SbSqZF+LavNnrvxbXlXBk77+mBXzx97zRogSPlQI8Ad/kZd6kSgayjkWbbg1IkT+pahM7gMmQx4q4InVSXpllEeR3fsTkIs3mz2veU+xJFj36CJUR03bUuyC53ikJUpRmTEeAf3Hpc65p5U+A94AB3RqbXkphE8kHW3gw0adnVeVKlTiQsb5o7Ht3yZR2QVTV29eq5X6w9rQX57xiCc9sWTaaXxEu4EFDeLzzo3i9Z5VnOM/42YMLp2LhiKgp6FsCApmT0gBG2nYkwUMeLivaJ0BOlIVjV9qKCxoeiQ8eDOgaqwgc9Eoc7Ix0yN63Izi0JxIKbDr/0kWv8Tx+/zBEEbg84UlhqJxa1b4+Ojd2uH0Q9dx5Pf2XSiMuAdWXe/KO6JbhLMRnxhxI4oaMHFchw5wZO5Sw2bjMlsqOJNgKFuC5hpCrhdRNxWMkIJClq6HU/bfcPP3HmeY+//F7FEat2AkMdKWQnampLhPYLA025nE6L3GEZReX7zTMOvRQtaydq2cOKlIx8lQpBhqGTIEibRB9FBe0GSkAfGZJKdJdhy+XiUGTDiG1Z2QBf/hvuobfPmbrs1O3lNr7zty4nRtokF1+MouevHTYFqnqyP7d7vslk2ea1sECsr6I90OZB7TRF8AYZJjw+P+KRKLgeNxEPBxH7oVoeOXH+Jd8/+skzNJRMAAoaIaPhkMoSMFYlUXj7OzyoHQnvuxcDTvWc9lB5/7fvOOdOuFzHyTmFPpesSQkaBYzbbS8xhSQuUpvn2G51UsXd9rDEiGEIH0FwadtIyS4UNYBEUytkv488KJilm7GtqxqRNWjmmFs+/Oeym1HbgTWky3XYu4GS8U56x3OKwge/+/hld8Kff3JWt74IRZOyyWRwmap7tA90qzgfPJWHihEzJ08U1S1R2rVBC2TENM2hm6IvmAi1QlY4PG5uhbeujMOf/mVx6dWzMs8tW7boWATbWcs7YBdEvN4evu+7Pd4tydp1odVTpy4a8c6M4/6u5R91lG64pNwevVn91m1AmkeyGRaIi5Q8KBPyEBKlByRmrHkAIw493Gq5PExKttbHwY3lo06dcsd3a4/8CdX9KEbumqmg3dqA4nnphw1RDv3q1VkPq4GcwkSCezPz8qVIPNbBQGkfnfCW9ocxQdRM46t0ierxmCWRcknd5ijQUnHs07XPLToA2zCOGcUcZrUzLeMM2acaEL/Hg+5LA1VOm1Ob9e6ciydYW1behqz8bi5ftmzKisSTjgEKFJyQIrF/FR5UsHICVKllJOshCekLG4lJ32cQzxRMlxfJeMLulyYTCPg1q7W5xuSNm18/b+a/Z62ZMviXalvCcI8s+86O7PEWY2XLefq/b7j6hHWfLbjF2334UIO75SQn7VovkjyF5qIP3SEPvEBPOwAAIABJREFUpQSaEclUhxGl1OCTCH2dL4Om8jUkgq1Q3BK8Hma1NtZwRJpapZ5DZp3z0JuvLDgTNWJlOpSI2suJKSPdZyGu7dY7TNt0NWlzPecZc/wHlyHSMAUZBV5vdr4WT1pMYZB0cbHZF5xmOQbKKDKRwBWKTi1Aj1heP5XtQzy6eUUz8kfc91bNwqrTS9GAqr16UcIHu+5ehfx7jjhyImLNUzR/nsJcHpkTxw6zBxlcRH0qzkhHA5UgCxdqwTLIe5gmJzcaXL0W/qNmlYVeX1RRiiCqGMp4Ma9wikPkQVFaKQxzF3W5ru7QfWmcbB7nnp8f/W74o9eechUCOSVwB+S0zG5SKBiF5EkHKaTbQYUTBdKghzjnTqRIXlTkqATPpLVznBWNFhLrgMlEQZBy+SRpu/hUJPWgmdi5+uej/jbt3uQDly364kQWt4uhXdvg3g103nLl8Hq94MtZ116O5h0X+gsPzAtHkrLmzxJKXbTBVNUSB0z8aj8PeQKKy0UzxWFSELMDKVa41K8WBIWjoccEkbOi0Kxo3NJDTRzhyMcjzrtq5o/nX/kNjqfUtbN+ZnvA28mV7kNHute3VsperB9WceHIGXBnD4M/M+AOZCmJJJcUTurOdvSR8qD2oZBgEU6XAB8w4PfIkC3dalq1zABTv7xs1iv3Pu077FtMKtUBoujscoPJwtnoC5780/KX770Zvu5jFV82y8jKk+obGkHcr9QuUFIGKjoS9j6RV+CKSzCrJxNxYibSzYadrTDx7pjpyx5d9k/felyFRHEZUJ1qbjtLYFdvU6+uunhtu7cP94uzZ0PIuWLYKX/RN31+GbIHDdF82TJTPFI8ZkBNCyBB7H1iSVIFczrnVP2xK+7Ci4poR7a9qIMmsimOqKBgQfX5xbyvldDFcHtSD1pWc209cgrmj/zq3w//0Gd0BBCV7y4job0aaBnn3nVTXhr574dvvR1MHat16+tDUpa8uflobmgCcxPAiQyTLgDHCwgwLQESJCjModoUnpRuGzJY51f6b8RQ7iLUqQkeDwtmOKa5LMk0DLP2y1+Lz7r52Wpj8L/w1DktIN0LW/rBLkZ3TJns/7Qvb+PfOmh0W2fPOPLSazd+/uwpWq9j++mhuFdOyxKseQo5SdHrtg8zPZxYO8bgzc5EtKURqN+OtMLullfjRu2KL1rRbdDc2cu+/Mfkf6AR5V16UYbydzxYNtiHT0quRyJ4sbegb140Ysie9CxxGuzqrQWFm/Yl0SEHFVA1TYNpxImk2ZKkZMSoWbwM7mP/9Uzs4/cuZawFKDbK+HJUkIFWwuIlIv9ECUpQZV8ae3vt2/0awtWDx3874NvHj5sCeE5FRlFGbmEfqX57PeAPQFIUWCQVmeryOVM8traFk86JJ7B7xBQx2tKL9iOT6yL4ZlKShAiTqlK9hWg8G8DNaAhNG5Zes63u8kfnVzehfPQeSdX2eL3ROZnPeebEnkeehZqNt2h5Rd30oOnNLOyPII3bEBZIJS1MAyqS0MMtyMwvQEtTE1SLw5BkeNPTEN6+A+m9+gj6DDOWsOdFG5sAj5cYjh0ooI3EEHQoontmmbyupg7JH77re+5TDzbdV/Zzy/2jI6hok39om5i3z3Rn2o/fspZ98/c3+vqfP2jYupcvuxbIOVIrGpWj65Al1Ssx5oJJ6uIWF0AOOu/ECU2spTZU0pmjTUQAK2ZpLkT1muVLh1391tQVtYO3oKqfzZDd2YvK2MnduPT5wXjvhgcQ6HN4emahFIroCvk3xeuFyZPgqYPXduNRAc++t4n/VdVkJGIkubetjjRa+1791f0bEsVbMA+xYoBXo8oS5shsg6ysrBTJDM0Ud8WA0eZX/+fqZf/VbZvJec6NWSeUonnh5Wq3cf296ZlaS00DAkV9EQ9H7QuLusfCIFPpmwPnc8Yoqc8v9IVo70ygW8/eqN20zYKqSS6VwtmYmFaSZQUyRZs0gMATSCZCulm3fvPg8++6bnXeuOWYPZT2L97VA+7FQDn7+yoU3XH4iGsQj050Zxa447qs+AJ5iITjYG43Ve/t4o8ZF2gLTXFZJN3m83oRbGomRinhJSnBZLIKTUtDPBoHonGoudlIhInb1zmAFBozJkZZOGGn4qE4Gn/ZhJz+lZd+tuSFZ05aUotN4zv1/gTTuvME/y0tjP/qKej4w5ZzBQ+/XoB/3XYFEuvORqA4h3kz3dwkWIosoJJknDTYnqCemlAPE4sh1pguLz0ZA4yo5WJWIlm7fQMyBtz/XtPHX/xtIRpqT2C0yR29klzGeWZF0RkXo27tRDk9q5/sSmeQVIkZJkzyjpxoWKj11ZmJwwaS0J1pwUzGLcZ1w6pfWYOehzyJye8/d/RkRJcI6EylWWm3VcSrjc7S+fP+a6CceY584rDYZ3+fBKgne4sGBCy4pHjcRFpmDkKNLYBHs1FBAgXorGwq3mTU8yQSAhOSW4MVi4t8U5Y9lqmTfaYRFFCyiHWCppcoaiRDJ3iglQDMqGW21rYgifmLjJ+fewTY9g7rWr5wjwZazrm6dGrVIYsenHwXXNo4d15vBZJPMqHBiOiQ0vywzAQkF920QfKWVrS1mUPXLcWbZhkh0tk1XP78AoSbQoKryJ/ZDeGWENFuQk73w6SwNhXfizyGZIVIRYpZEhLc2rkhDGauHnHp9PLceROWLbIfwm7d2VRHNvuhoyTwhxnXf+MHUyHpop+8mVufHN+85KlrEThonJrWTUnEuTBQ5vZCdsmQTWqGG4KtndAotoE6+Gna3KQOZuqmtXPrFmDzmzM+5m/mHYs1fwNaBEt2Kk4tLnOdfPn04e9df9zDUN1DFXd6wDCY5PFlIGHapHqUe9qtVHuO0eYrdJBeAk6dBLd0S0mGE3rTT0vOnP7ezPBtR32z8J57OMrLk5WVJSax5aGq0g6OqdUiAppURa/rKPb/mrJmt+0jkuo+507Elm/ulLO6+TzpmVo4okN1+0UKlmhugSsrE8kUDDOVmzvUo1StpfuJOJn0cJDEaWhOT0LcUqGmmy7JLSWThiwoYUS3SRJrS46XcYNCXJiRpjha13w3e2Pwmvd6Y+OiPTDR7ykHZX97i/s/nn3FWVs/e+dupGX38AS6yZKcjlhCaLPC5ScgsC4wipYRhcejWbG6nTZYMZaIaPlFW/WmLX3Tu+d7YzGTGbGkpKlpMBVSxSJkNsmM0wamhHfoo1DgTF8KmJm0zEi9gVhzFOk9nnis/t1nrv4ravGcLRXf4ZIWp0EMzOz/L/mUj3mPd4/rey3S0ya6vLk+SF6JRn0Z0TiSU+JJKDxhs+jJJFlo120onErabIjghm5ZzQ2tiK+odg8vq3rox3nvX8OwI2Wb9GvvN3hgS/nEs6213zwIf7ZH09LkhMEEkZvsUmAkCTxCa2/XJ4SBOgdReFDBLZ6goWzTaNnZCF/G/BMe/NfzK07KrakpoFux2EBVtYUqQWi5BwMV1tppV/a5cc7jyoGbmw/4+b4jpwCxCen9D/BSsS4eS0BW3TBJwYwKdml+6HEKSpzipgO8IakLMcjODKrKmuFVP0XhyfkOlhWA4u4DX7aMuKUwj1fjRBknCqjUS3UJsD3NQsOMIhFrNlC/ZfuB51xe/nO3YxaiZgxV5HfR4iEl3A6zhe0rydmtlci5/6ox1yPccL0np1A1uVviIGZt2Zm8IFC1LlAwdI7MRNxklkHTUyHUNe889dryxR++9dRhxs4tA9XcQo/FVTkZSULLzKWemo2eITUl8bD0zlQJU5AgAAPXSHAAbtmywsGdnDfXfjW05NZZPSov+GQhYeMYayO1bp8a+v/CQAVJdP/T7z593dvP3QVPXj93dqHmUv2I6waS3IBLpZtGh0mEXXSji2qhHYISNYpL4rAM3UrGwgk0/rAFSF9a8nLN41XTsQqrBGZQHLDiG14+sHr23bciM+cvcPmY6k6XJJdX8EUJNgeD4GuWbaRdGagIcU0YwcY4osHvx15++4MtF56zdOXCoWGUryLHzsW0kfCewo/Sy67edsCn22wD7a99bqAP8yyUj78WwQ3nuHsO7CsrbjlJXsfthWEkYcViUEkaM2lQLcSGPBILowhTiXSAnoe4tnSLm+E4dm7YiD4nVGDjBwPg6VUKT5YCnXm0rCwVnOjK6fvtf2+6KBelTkUUSSNkWvGmZoQTFWXf//x0xRDs6BAB2dvI92CgfDlXJn2wrqjiziOmIz3jTH9OTxaLM8lMyIDkhsuXhmRCB2QqOevQPBL01mYTrcFGpPklNOhfPvDZ18/PvPWGMfVfzD4K8ojB/qJ+aeFgQhKsC7Eo1OwAEokUWs3GfGpQCWUldDQp2PWq3IqEGpCs39yCzNyqssp/z6i4YGYjjrwpLpAre3p1rvPub36VnfQ8H/D+xZlTgYLTXN36BFyqTyJ+ItLaFH0XyYBFbBSy1x7ipjCXgAy0aUQjQ9FFPGKYdTVNwJpv1eI7qxI9p72PPETQDAutP7lzm6afXb/886lqrxG9mc5lS02DqvkRaWgG8/ttYVoHS0TD2SkPKnClbTmoicSOtXVQchec/c/P5kfPwOZ32sNocAHt+30Gus8Nk9xAMXcNGLVq6Nqnxz8Dd97g9J591WBzK2krIi07G7FoTJxrjXijQiFoKq04VWcJekmGmlJzS8AyoyZqf22AlvP+wR/+8Ni343uPBhomAgML4MsLuNLTvVyFZN9YdupmpxBJIRjGkxGLxxsSaNn2eiWvv6sU2G7DkDq4yS4MVHQ2X3wI3sc/LT9o2btzpiO392GqO0M2LXL/BH1SoaZlIJEgUDDF4jZ5VCIeNnl9fRiJaBOyD33l+uoXFnw8Z33+yjn9JwC9TlcLDsxnkkeGQZWvJDwZfsTirQ4M0G78apTfQgPjHki0GMk43Iplheo3G4jXrfYMPmp6vxPKllpjP2xeVVpOZthVg/d30RnuU6t9g2eg9JTTYWyezHIKB3LLpchuj8Rly7LMqESAa01RoCdlojQXh8MiNnBKAmhkj9gO9JiJWDSI4JomIPttHLPmUfhQjxIwTCnNR+MXt8DCKd5+B2dHIwlZUf0wuQuWSRdAqjpst8eIoY8MlNbcDnPtIpFBYODGnT9iYOmMMS9M/XJZEULU0pk3D5hEZ00YaHuIuzcPuu8NlMtHP4D8JbNOOgd1K6Z6iganG5bMkroh0cSOrFA6YVMHm+Q9TRMSzehyMiwq55IMIwUnSTJOC/GWOELrluPgv833f/PAwvDJL4zB+9ddC2T2Q9rgHMiyl6VpVJKDIZHMh+S00wxoXEfCilm8dbuJaOPiQ+98pvzraSf9aKPt20OOrjyomKNJK67IHNpj9clfv//aVFe3nv2pOqx4MyAlXdCTgDuQBaLHsOJhUag1pSQ0VTIiG9ZGYSZDA8+5745Br5R8QAfqnZ4nXYltH4yDNHKMr6C/12KKRBBBPUkQTpvQWbRYqI9EUxycWr5ucVgSoWZ0L8yzGuo3I1m/pQU+/0vv/PrtvFmzKhqWzJokqlBdNOg7Fr7207yUKwfei6Kf7yy6H4Fe4wElSwtkWS5NRSTYJNGgtBQIwIomwDQPZMkFouvXTRMknQGaJ9VjFmSWQN2ORsDYccgdS2/9prXbChwI3u2N2w6v/fDhm+AdOlzJKfQaCVNS3D4xs+vOyEKcuJBI4NJhvLDpTYiNziY3sTsvppVoaWlBPPmv/NuXzRj2dzQsZIzP48XGIlRbbdVbMlC7SCQimo4hbiq83ffGacfdF9y5atxL9550H/zZxf68Hko4Epdkt0fwQBktVLl1Q9M06MFWuNMD0BMU4lLKJYuLi1nESJmAaYZMhOrCSLS+iAe3zsUqbL/gERS+FOh3DxAdjqxRPWHBJzQbFQKbqGCEDiNkEq0zadnISSRCtSaS+grk93sYf339XdyCsFNfaTu3u+ag9GflRc6zL3T3uwvQ/oLM7hnMRZmP4+LpJqEbgcminRKp2QY1w4tEc62OUF0T0vybJj658Mb5B/X4DlWrcEvPmkMeuPS4Q4HCa9Xsod2ZkiZzWRUeWFS5BN1rUrRYkhRKcKriykIhwSV0P0JQVFg8GU4kazdvP+ySW2949NnLfxnN2E5w2BPFu4YForQrSrz7qYECmMe9KD/ufOxYeoOUPby/Jz0XkcYgvPmFUpRoS1M45rYavzNmJ9AqVGgwKWwhLxqGV+NQMj6eUfPu/T4A12SNnYZwS7GUXdRdkj0yGYpAfYnddooeAs3lgqa5odfuBI0UuTQadbOs9DS3VL9tvYnIlu/GnXzzrKWRmz7GEkRtXRVRNqdBJnC7NgThRasqd0s5Okyy7Ot9YEdzri1h466H3DrVlZ3v8waypGA44kQTdiWD6F3EbKe4aYhZwgQ0LxQqqgWD0FRZRHStDZsiiGxYP/gvk+5efeCDi1DOdHAekA+cdLq5suIKyGP6u3Ly0qFSEkGFPyZy0ITkzI4KyKABhSWt+Oaf6+EtmD858s2c2aw07OiothWLdjfQbjd6T5t9br+3Lz//Pli+cVpaThpkQrWIW8iJwxV7+j+RQHpGAOGWRkvi8USy9pu6woP//M+L//nak9NnjK7BPODwqw8t+OWDtSc1b6i+WPMPHSb5c9xM9dk7bMZtAwUZKES/SBIGygRxstunIWFEYCVjsPSQCTPSCnfacydd9tCLN007bMd4Jug/Ok67pEzVaYvuxwZavFwZM/iT4mUv33EFfH3/IvlzFG+gG8LhhKz4SDCWg+spcTNn7duMNdWfIzB0kiMSjcHrb+gx+sQ3GCRj23vzT4bX309O664yiliYKeQBORU4nJxIjFEZgKR5YSUS0BQZeqgRmiZbqsviodpNOhLhZ/961cznh4w5e81NF8EoLgOvrmgPUIjShCZYRIlI0NPsbofOcd/HBrpShfrcYCQemapkH3G6wVRV9aZJCSEiZJN90WcXBAMiiiDJRRlM88GKxiFpGqxoCCToq4cbTN66dgfA3ykuu3xe9bzVq8CqrOGce3/qe+UobHz+TrBBI9Ru+ZlMk2TCq9vD25IA7wicmAA/GJAswzLrtkWQnvHmkXO+vOezDah1UGFtqduufVB2dDMP/HjBxLHN7y24EWlDR8GX49VcTLIZxVMNW9uDIhJFdkE+Gndut2BGI2j68vu+Zz40/bTXblw+h7Eo5oFjEtQZ/+K9p/4lbzKQdqKn+8icBJcUxeMBT5KB2pMCtoG6IHGaoLANlPqsFEIbdJLMqOXzyJHQpu/XpA0+5sm5v7z25l/pPWwMY+rGoedxGhNtbnUfH449Z7vvc557Us7hZ6Nl63RY7kTGASPTWmpbZJaWJQkOJ8rz22phTrfcuSgJuECLk0gkLE6IrGjEhDuwUQiZB+uGQPEnJU+WwgjMKwyUg4vGe8qDyrTgYh9lidgtVAQbauDzu81I4w4d0fra7H4H/f3Bv7+x8NJz0UTaMMt5JR/NGKGFrNLSUml3A00dkPZn/g0hij++FEDua3x1NpYcdgZgXJPe67SBBleUGFVuNc0xTvJo9jGhkT86QTpkuNMzEa9vhETjYqTmZkQRq1kfBrZU544848mjn3jsk6ptgVZxOXHu0iZ+10t/5swbAN8ET4/euVyRpCSN9wjGitR0lw1G0ZMJC8k4R31NI6BXu4+a9MAjS279aZJ9pvdkoJy9xtH9nGHHlBg/E4B4TD/JnSGmw4i1zG662v1KumdUVUMyHiPpPNOo2dAItC7GoVPvxbzrt2CEgw8tLsbRFyz3LZk8dgKwaqKcc/gwM8m8Lh8NfBPmwLYvCnFphI0qiG0GSpVMhUGhfqmlW5lpWqJu9WcxpPV889BLH5/19apBW7CwO53iroDGKWP94w/Bf/oOnCvDz33y0J9enf4IvFkD4M2QZF+masYMSUAhqV+c8pqpq1QcJArFaNBAQZL6maqCaJBQWcyiMU8ei8q+zFxJj4vmjODKIUifDeujlw3yZpIK1eTQqbXgVsCTMcutch7asiYMyJ8N//PVM5544ZqV43Lt4sW85RyTRhOEj2LbUgL4CbWwdg/a2UD3uXECGMK5uirjnAPR+s8bgd7H+3oOC2j+TKmpvgma12OfaQGldAIAhy2eijoEHuHhCBS/D35NRnPtNgvN6zYD3qpBN//y6JoHUO/kjGSgwBIEMP6gMqC2zFs4vMiSZdkQ2PqUB3WIroUHTYIbcQvh5hAiP/2Ivqe+WLn+9XdKSdemw62c2nb71xIuPfo0iq7JyL0QMMrQ/aA8yZUm034qBDUTfSHRQRebHEhLR2vtTkvcupu/3Y5ug95F8bUP4/Re9SieZJAuYgnnVhV70X3AKQMG/fLupMvA5BJXbqFfVlXF4Kb4uRRiEKmYLBJyiWogwoOmZ2YgGgvThAuS8TB8HtmK1G4xIPFfegw/cs5FT1Z8eP+oKnG7Oyevo7fc/w0UwDlv8YJXS0ZNROLnk+AaMNjda4AvbnCJNCh1gVV2Himlr+pgGw1aL1mCEY+LtoCZNJE0CJStivyd1tI0bSZFMlCFLthOBmr399xuL+JNTYQ2AY20RUP1Jlo2bpR6H1Uxefo/Xx9zHnaUsmoOjEZxGUcqxC0pgVXitD9LRHibeu1HuGjO5ZzbP89ruO/oMwDPVf5eR/SJJyRN9QQQJ/0ZsbYOA2abgToXGOWNigaxzjRQgKQV3fyLAdT9u/vo6x8f/O1ti5fYVVeyuqQw0Gp4MGbsWTB/nCznHjQECo31yYK3i/bTkFw2MTkViqhAZxlWUg9FUb90NZD98mu84fWz2KRaoKKNb6qzgXIuXbLgh2H/OH/8VZD9Z7BugwOS7BUdDyo0iNjZJnG1b2GTxB64hWQ0YdZ9uxbo99LI77+f/8P5Q6O4bhWFt/T9NMvpmrAYvq+uu+svtT89M1ntPqKQyarXBBeUG+LFJMgUo5MHJfobAo6TelQ8Cs0ykOAJ0NwLKVGHdm4NwuNbeNk9r85++riBP2JoG8Rtvw1n9+hkn+Pu7DdfHdH41rnXAQPHIys3Oy2/UE6EY6Jqa6+NnVKLSrfDJEcbJ6TvwjSzTRGxLR+hKFRog6AokRWPCHgsZtoR0K4GGk/Cl5lFwAfEWpos1W3xRM3mGND67p/u/HLOsDP7rj1gJCKTBCBBaGyiqnS0RCeR08XrEBCVlO46ubKfGClB+nqfPgab374S0sA/ZfU/MK05qEuEkfHldkMkQn14Zx5X1FfEYjtwC+JipwKRhEQ0BKOlwURoww5Ae+7uj9c+e89U7EC1mOS2zxyF0huQjhMvGYe1/7wOaQPHMV+2okqKpNP4Hon+ujRhoCICIlZHnrTMZCSR3P7lJkg9l06u/Hru7MasXzGpnRCuYw5Ks4MYfuWjh/705JRr4Ot9Asvo45UkD+XMArhNkt8pA6VmCIVGbq/HirfURxH8foXS7+QXh6+rerHalnAQxfo2tHEZXCel1456f+bxlyHdf7rqD6SZnGJaSbKockYFIsfwyXuSVAI1720AowWPh+TWiWCMIdzaYPKW+p1S4cj7vRe88a/wfQKHuncCrf80DP3D/x1nWAo/xh1wNRA6D+6sfml9BiihYFiyL8JUzkiVQHvOlhBFwmCpAqvrMI2kgArax4sq+0TyFEdSomFiquDuwUAdSF+G34tQSx3l+Waybv169D70qfKN/3q5nC0JA09YKKk0CRhSaSdSqCotpTwUVVVV9tTKLmu0nwwuyDh2Tg8sepiI7kqQM6QwI6uH0tJKIrsS3BmZiEcJC95eJHLK3HZ1m7ySqoA7xZz41l+jQGRx5shJT9/y71uWTs0C/eP21MrmJlUOm/qvUV89cOkkuPNKpIxCF5c0WSXXIpMGj+1BRSFe+C3TMuJBg9f+UgPULjz2ltcXLJpxxjeCmsHBm3c0UJWuxUPKZp7yzYuzb4OaOZil9XRLMtFyUKJLYbYzlE20JdTMdlGhwTDjwaYgWr9cNmTCw4+tuvT6T/ECSyITnM8TKtlOV5xLxTORueLpqcck1rx0j5o/qJBDcXPmkog9gA6jRAeKxpyoQGRxyIoGl+KCHgpCcSswokHBC5OXHbDqtq43EQu93u+8ebPMl49fuYkQLs5D7cGm9uOQl7Ne574yfvMr592AtJGHe3K7++OGKYs9d9gpbGKqlFi2PW8rU1gr06gaydG7BE2nSptPk0aWhSThSpklKrg0tWJ70FThXkJaWgCh7duRnpuFaKjRSsZb42hZvfiYqVVz9PuP//aL0iodVaWc0hSRATkLW+p48VSutF8a6Lx3vK43lx2a/OCp+6Xu/Q9izMtycntKtTsa4cnIghXXoZukek4P5aRuAilEa26jflQaXIhTkdVnBjcu3wlIT7zFN75wOlvSBBzt8KW2nTZ2C+fpnl+RXz4o9zK4tDKW00+G5FXAZHHZ0iVK+0b7xK0k4XItM9JsoLW+hYZCRl045QH27OWfCy1REXm2CXmINxEGit4nlmDzR1eg25hRcGWrjGmS2FyRTDvjT6InStcps7ihG7xuVQugfDzr4w1PTvkCP2ElS1ZWwiwh0gWxBOIfu7Cc48AKvejnikFErX8SMvtkMMWr+NMzJdNiiLZSr0mD7PMhTqrTkAV1hD1pQY10k0RuCcANU4/qeu3mjd0OPWt27aoHX8cAhDqFHLtYaYqWZP/sjXJlejUOuL04+wLAOMtVdFC+aZJ+oybR7UtGRQZKv9ptC/uLoox2mhInPBNlXGaD6yUq6VOByKGmEd6i3UBdkiIghW4iqDPCZnw7SRSwp9/im144/R7U4G4YxdWTUL2hwioRCSeNYtP/OaCEFHFzh7XeT7wnJm7jhfMHDvo7vIHxSCJfC+TJNHKgx5KgxrrXT9NUEejUkxSADVpKG/EjTq1pwuN2IamHLaO1PoHY9i+UUffc9Vb1xPUnEact57GOEg0ixB19j2fp8rv7jEsrmoBw09XIGxmQlDRNSHpQfUWigQiQ/j0bAAAgAElEQVR7QokMVJGZZbTsMBCPNkBfsbTfqfdWht++4+PaDmyWqe6mmBEWRjdswuXWz59ejbxBhZKWqynEXLargTpXJlGGKYyb8fr1BnRXRfninx8v34Rt+IqZJcfCqnKKB848mLirct7i3obzTjwVkS+vYFnDRinudCURt2RfZg4URUVrMAhFo1ErSnHtWTKbANvWdBEih8wESXlHW+sjCIU/eYOvuvkMAVwQCrhd9UVt4PH+CFygjS2FdP0TyJtTfObx2PL+36AVjlS6FfqTpgqVE/aawlu654ihv2O53jZKwf0k8iDH+BzDsUn8bWkCUS1vAyrQmhKRiiTA94lQE2AQ39Avy9MOueCRq5c98cX9jIXAy0xRsGCwSkR4azMldEAI2W+7vxko5+68kluPrFv84ROQPAUk2aa6SfCICpDMQVERxb8D+hATJw7peopFnyqVybjl80hmZOsXtUgf+QLOWjwXz50WqeRv66X2RFX7owty7HuUW/ndgfuzeh+P5s1XIX3UUHhzvBKRkDEX4Rba9opoaA1mWVY8aCIeCiG4emvmYZfO7PX+nPd/yBTE5KJC2LFIRAbqQtbo69BcfREKxvVhcqaidjLQVDnajhaZRdVBS0/UbEig24EPVP7yxsuli7BTxOadKnttOyhhHpdOMlDw/tXdb4CSc4ma1UNJxCxFC2RJHp8PLS3NkFS6acil0zydk285FM+iQcCIKjIJPd5qmXVbavOOuOK66J9v+Cx8PFowumsDTaHQuoAG/uFZ5m+/gS1H/9c+wQOf+8vwSUD8VLXngAwjSR6ULsg2xjVRJBJCus6XbZypr3YvSr+zc1VLUD7qXRiogJ0pMoxY0DJ3UPM98vy1NXVPz81HnVOYSg3IUztFIida1cbeZ7d6OhuoDQf/7ef9Y7+jx63vDdz+6I1XIGlOQqC75vFTvqWAx5KwFE1EnCSmLOqvjp5KR44hYpMXo33JiCUhFjdqP1vc77THZw9568pl7zAmyBaoK9j5KThDOZSS8xH46q6yIdteeXoilAP+jKzCgKwQbbuQlXRGz0SbW8AGFYmbicbtrYjUGsqgI2YcuuSVVz6fh1aU24rqHQy0RMEblV6UDLgDydrz5N6H5FimTxYGKnhTnblNp5KrCdYy01JknjBqVjf2HXdx+ROfT/vgxIqKBpRNSrHwtT1DRx3GUzn3LDpg4oTY6g9vha9XT39OgTfJZTkei0F2u8UQuEktGFKhTlXWKGzjdOuTgZInNSGxBKLNtXEEt31c9vhPMyumZX6PnQT/248RRHs8m5XqdF6SfXvm0ceiZeUkLa/3QaaS6SZhu44UTPR720A7eE/xx46GapuJRPkOd/LP3QyUxgVNOiCE8otHNnz1nXfsZQ8dv/S+z96EWEO7dN9RJTuVxafiLudTpCxyf+h7UqipFl9Skvju/anI6jVM8uVIXm86ErqFRJhQQW643SriehSWGRf6QaRba1dv7dYTscULsGkyaIhJKkt5+M+vrXz1g/tOa0L1O3ugyRRoBBcqNnivHM1znyjufwXQ9zzk9c1xaR7JdPr74iKQZcguCWYiBp9PNSPNO2No3GaiW5/HT3lv8dPvhlCHJ6oMVJV2IF+r5Grud98V1c845U5AOlntPzzdiKuyuME7GahdKCLdjiSBfYiKsXnLxvEXz7n9qJtP+6r8IhZyaAQ7QXocA3WSJo7yxRhUPn7UFCB+sq/nkAxJ8SihxiZ48nKhG1GJySQLn2IKbKfqFKyxIrElvlYL4abtFlrXNaDgkNnXffTW848MY82Os/1jr+n/+k/nLIfDP3jWioFLH7r4cjQ0n+XqPjANkjvlPh3XZFMZ0/+THAOFv+2vzp6U+HWprCBqCF0YqA26j1ow9VY0Nbx6R+2q2X/vds92lJeT+C6w0qnEOwICbffCLn5z/zFQLuNa9MDTI2+EbFykZvdISxgSJJdbGJ4VpRlYCYoguyO2CDFabAcoKbkSmkem6RUrYSbqNwZh7PjuTzc/d/8n55/8jQDfVCIVHe5eG6MIlN2jvMbvzj2LsYuBPpcjr18uGSh50JQaJ/EUSUT6rkfgT/NY0Whjwtq+qhHwv33kC2sf/+xCbAabZAAVhPWyX6dy7v3qzHsPbnjj8dvgCRysFg5IM2KqRDmQyEFTDV0BuSQWdKoOMsuo39mKROTrC+9bdu83cvqqNbcIqFJb+UI8Rao20R7+yLdw7p99wLRjE6sX3Al3t8Ge3B5ygpsSTXToLQ2Az223WMS/tytr9uCsnTtRmGsZMZiheoBH4wiH38875pLZdaNu+REzxVzd/19tF5HDjNfAF0vSkNMvs3759Aat52EFYG6JjMyJ9Dvwg1ORiPalo312MFYiwGZU2SUU2J4MlMjeWk20bFntPeS8uYcvm161qKI6ikmjbV/ZLmFvz2F1AlG2meU+D2mdQ8LGcvi/yL7wz2j56k6WnT3YnZ4nxYI0FqlC8fhhGqYY23O56GFIHpPwBfRFEybUvqKpEwVmUraMeDiBppU7gKxnZtT8+PLUfDFQTW+1p3PFijl3VbMK5RlelnEpY38B+tyI7gPzXQrxMEgwKf8VlWIJmouJKrKqMstKRozk1l/pIP981LQFM3beOfaHNcTYb4Mx7dcDnKc9fMykU2sXv30juvcaxLRMVTW9ktCg6GigouJuGyhk2dK3rm0CAm+Wf/T17IqvsGXO3UwvtWWZO92zu6QrVP2RCq5Dj5q5x1wHNJyHjN45abk5UijYLOZACZEAxa4WC+QSfQ76EoOvZKAcyShVfTm4GTQTrXXrEYu9UMnXzy8VVTaQuOV+cnh+t7slFmjXgOjM49e+9Mi1rNvAIxQ1jaaG29RTbCIuW628EynXro5U9DipKkm5fNcGmqAKb2NDGFH9jT89vvzxT1xYieJqA6NHgwYdUOZordDcZ8ef33ln9481Xs6Vwns3DN726QXliLceK2dk+1VfJmJh8poKFI2w33ahkXRrzaQuNFgNIw7TpI4GQUq9wkj1ODetxtpWWFsXK2PveNgov+IXNLOw0J5xaKK6oNhhqOQSShmO5jxtCfNPAHpMk/L758uySyLwvYu311TIpohoTzbC4C7LZLFWQ69bu+awqx6+V3vsIkIpkaMjgXrxYjM4T59WfN450e8WXY+eQ4pgaqompUu2eIzhgNpTbRbKBwl8zczEthUN8PV+4e2GxfOeXonat4sZRV3CQDteuB1C3PbTupx71fMeOz7x60M3w9t7dEZRH9ZSVy/J6QGBG7WEFoZNumSz0TvlalLcojZvLAq/X0UsVAez/tcwfP7Pr6h4/+4nP31lHSqmknPpNAD7u81kX32jLZOHko9476oTBp0Dr/8GJaNbBpOI+clmGk2hAtQU5/BuhpOaVKPesmovXwotIxgU7Y2RBA2kYfKajVsx6NSZ5asffKucVbcAo+1+sjgVe9P2FLu7fxgnfdIyno6KE0+Gr/axQE5emiWpEs0X6zQyJlBCJkBsCZoqAAiIxyC5CegRh6WTlq0MqG66/K14JGagdeUWwP2Pcr7xqXJWHQFGp+B3qaO96ylxysCl8sjvb/P+cMLf/oS6lgflgoE9GdNkzhQxIUP1AyIsIGil2+9BvLWRxvzMZCwYQ8OK+pEXTbt3wPOT36kiAy3pYKCzOQ9M7nvmRGxcfi16DM6BpSqykmFrbLAoLG4HUyTtTQ9OrMfMjHOrbuUOZBY++VLT0pcuYCxcWYlQCpvZftm0PctucftiznuMZ+lXAfmXeHoPCTA5TYnWNEreol4w4y3QyUhpcYWeDzV6ZXBy/FQtMhkyMtIRDjbANFpM3vjrTiD85Dze9PIkxhocA9110sU+evtBtXHP90CZ8uqP84adc8iw2YA0OqOoSE2alhQOR5CRk4dgKApFVLg7Ag92/WlEVKU5hXrbMEkij9oqxCwnWQnEt28OIbdwSb+Sp+9df2nRaowgBIsQTSZP4ShPd/i5nS+D/cU4KZRQzp2Nga/cwK5C+iGlmjctILl8km6qou+oSnHojoRg+4VPlLAqkJTh86Qj0tSIzOx04qw1QzUrgkDko2uf+PThuVcO/8UZafwNqUf7csW8ea4LTz9U/eTCB8fWfLLkEWT16iW7cxSXqUi0H0mXLM615qGo0IBFGAZiseQGj2/5cfvA08seKn7z7spXGAtjHp1wx4OWcx4o7zXhWmypvhQ9h3eDpcqy7JcoPucsDO7QpZAYXsJyQ6ailRk3zdoVW5DRfe6HzV9VnUgGWoJoicMXlBrK7LDFu2/qYu7G2X8+CnVLroV32Fh4cn1qWneJtEMMg+Yh21kXhJGKRrJL6C7KkhuKJCMRD8ElJ6xEyyYDUuJL34lHPxiZm7cUBeUGJzK83UPd/dxAgXNnBnNeueuYyYg3XyBlZRSkBTJFmET9NKIu9QSyoCeJbL+9BWMXVx1oKKUChMUV1Ul7ekIQvHEb6AEzbsa3bdiSfsDYBcfOe/nJ149Ag5NjtQPfbWRWV6/9xzjB2QyO9KmZZ52Gls+vl7MHDnWpfsWlpiOm03oBqhyBLkWFbqc9uULIDA0wvZC4F1YsCbfqgkczEY806LHGLVuU3J6PnXXjba+/esvx9e2Q1d9MmRjKil0vTHtWnXJW+UH1XyyZi+z+g2RPvuqy3JLOFUgkKZ8gzmjaqwRk2QXJUiwjFo2jblVznz+fP/fo92c8/1wfFsKtbQbK2ZQ1yJ51xLE3o27thazowGwOD5M4VRCpQLG7gUqEH7Pi3KxdvR75vR5+q+bjt09nLDSvDMakCpFIdwwF2grzXfTJ2NOc97iMaRcAgSuRdkBeRtEQuaW+mbro0KiZLNn8rbZ4Kj2YSzAASpofVjgCk1nweoBg3XoD4W118Pr+cU1k3aOPlrJWXikGEro6UPu5kS52H3jOtlE/v3rnVVDZMfB6s4nyjwjAeTwhebJybbha21MQ/NK2LUoM7KY41Q/oQrMsEEkYje9RsY8QJpFQK2KRhb3PuK9i+Ovnfvc2A3HQ2FruDgDOgU7uaqD7kXGKC0k548Fw/zduGTgFUE/XuvXLIBI2WfEhHLWLa6ochU7GKTkZj6hnaHBJAXBDhiueBHNRyBCxorU/twLGomPL7nss/6yLvn/xBFGs2ZUtYk9rIM55JefKlX+68MCGf787F9kDDnJ5890WXQQmcUxJ4MlWCEympQuIpsxVS7KS0Lf+1JA/7oT5wz+veOKjd1kratpDXFz3Ne/2yElHTEPTptNQeGA2JD9jlioMVGVh6CkPSnF9UrMoCzQTYY7G9T+j79BZC9a//tF5jIXLimFW2GpNv9dAaZG9yDjzULR+dAsCxaO0rIIMPRincQJosoSkQlPpVBYnzlJaGwp1FSiyhkQ4ArdHhUtKQtebLWPHLyEg9uXwibc+fO/TN357OoUKnT9L6sDt5wYKhlNrsvFOwbEAjgXkYYA/HVoO8SC4wSWZFK+dWiu5SKd2LtBDdkzLBfgzKSAzLsMCUaeDlHjDLUB0C+SjFp67asniVwaBhklpGoKqJan1sUPc/duDsqPf4IElF/zpVER+ngxvj8FpOQWaIEeDikSSZmHp/MZBBJhC51OcypSBpiEZSiAzMxOh5jrACprJlq/WAAVPjLn5jXdGPDC8tqJdWS+1Lnu7oMRqreRcOfuiO/v9/OKjjyCj72GSr8AruzJg6Nyu3lphwEXOJgFJVuDiqmUm4oa5Y1UkY8SYBaNff2rWor5TG1Bd0VYkwtULecFjpxc/gFjriegxIADmZZrwoNRrC7c9nMYVEeKSBzWDDSYiO35Av4NmzF63YMlkxqKk17G8vTzc3iJrLxp16c38n/Gc8LGHXo5EsBSZeQNkb45smorEZAoLqERiCIYFG9Yr2bhGh7GCRH70WBBujVnRaAN4sLkeGdnPz1zw8VPhY9w7ym3hxr3paO45Hdx3fyOUzasC9/gRfDgLCGYDbh96nuhCS0hFqEZBzgATOklguyQhsUGPGIt3uO0VCarfBjFH4xaSTQYQTgANQaAuioLFLdh+dGs7JMgxUDu13ZOB7j8elKYJ+kwehk1PTIZUdLKnW68Ak90ScTcnLTojlIOSgSagCxHeFKTPVseGqQAJE5kZAeiRRkRrfw0DsTczj5g4q+Sz29ZV2EoG7Y5GNKA7+B37MtttPSo5l+fcsaDwy+nXPw5/93HwF/rc3hwpTjhg0TqM2JSnBFllpOOsWQQYiWz8qVXu2+/Nsa9UTP/skI9qUT2p3UAvfJ4XvXjp8EdgJo9AQe8AuJsmDoWBQoo4EuBUve1goE21BvSmb3HAEfdev+qJ5XNGswiqxScWHrRDOLu3ENd5Zq72+tsr4zY/d97l0Iad4O7WzxPXVSa53BIkskSawDGEHgYFKmIqIMkhu1xwK8SG10xNX3BTNyN1NURd/2vxCaeUn3r3fUvKDyoNOWRM+87c/rN3ZphX5jq6bJ68pBomrS0qnB+0vAvftmqXNykHMBTAKeDi3zWD4VhwlInvs6uS9iGzIUe7Y/fsclrn135joDM4D9zb//SJkZrVt7p8Wen+9Dw5RtAzpiJp2oAO20AN6G3QWQoK7ZadxBW4XQr0SCt8bssM1ny6pnDkWY+c//38qgfQxrBnG2EHDVh7OVLat12kT5yzP83/Oe+Ty/70GHw5x8Hf0+dN6yZF6W4UWFU6wVQ5Jow5g8wVS5MZj2z5JYycrA+HPjrzrpUljTXAqe0GekJ5Xf+Pph/1BGT1EJZb6ONkoBTiigoqsZ/RDUQFB2I98NgetKUugVjwC4w9a1rJ0ttXVFWwKA1pk4F2lWvutXLKOTvpSxS9PzbjPEg5lyJQkC+p3RTiPpdkUyIWdEbNBiGPYIooTogEWxZUhWTyDCiyBJfMEAmHLB5piCLaOv+M2VXz37h+1Lr/f+dFU6fhNwsU/9kVIJqqTjW4ozF2in3+sPf+Tz+z6C0cdtm/Dvvq+Ttu9+fmHsclt8wkn1A+kCj1SVABzcbaKlS/SKFlUoAXIqeTVbhdMiKtdTD1pjii6xecP+2fFS+fVfcjykvFDGwq8NtVCMoW7O3oTjs9Chv5RnPgh//H3XdAV1Wl3+/bX81Lb/TeRQgKI4jYsWAnjih2QSzYUBBFgwVBUAQriKAyohILihULiN0hOiog0mt6e3n9tvNf37kvEJggTWfm939rvUXJS3LvPec7X9vf3hd2fxqy73Skt0/zpeQhTKAJ6kwplKolnIIVERMwxfZqKsLb10fg9yzv+ehDE1dfS6oBd5JwNz8ghME3rO668pnzn4HL10fKae21mAYY0h4Pujt+JxiU26YqrhWsjSMWWomBl9438Mux678uEeLox1t1SRGdQ9RMeZl5tedvHZT4ctY1QPtBQmbPVEHyKBBtbqASx/gxmMTnQvN0pAAWjnDuWI/bAz2W4Pevqpodj1Qzs/KnX//+4D8mr7136Ge/cNbuxqD4CPbF/7/f6uyFxs7N/7SBMgGlcOeeM/rq8lVvTMrvMTitpj4q6SbNFLt4aGtGSCWbSMGcDrJKco68ny4lW4XEc0u94Ljtki09VvlDEP6+E+evf//9q3cJ9cMLYCcHA5q28/89nGie3lU4ZTFL+fTvebMh+86Cv5XPl9FWCjcQKTkZKFXUyUAtgEgJmEpNTUPfsSGCFPe3XYvuu2ddj2c247RlSQNlTCi4bGXPksXXz4bL3wcpWV6qcoFDiZMhbqOBmpIN28Vx9qyuKgoj/pV60f0T9HsLNqN3LmXhu8e9knd2CCHRHAXHVbbGT6+fh9jqC4FjurpadvTpRlxye9zQLRNGOAgtPRW6QVSk1u4JScKdOmAGhx1dYHHo1Rvr4E97JfPRr6dVX01tBGdC4P9fGzuiO9vjP/99S/6PPTOmqkNf7ap/dNcsT6t2x8Z1wSUrfkDQoFOeRwmWpnLEkGOg5EmTRGl8IJt2C+frh61HbaPqR5I4+PLamesnzruty5bhbKtR7DyNpj30/Rln47G219dHMuZdKLeYBUE5V8ztnmqbbgiyS+SSG1RRlikCZLAlBUbYIH4x3QpXG9BD3584/x+3L78kdyuQu8dAe170Ua/V7935NBRPLwSyvAABjKmKu4+B2jI020W5tc3qq6PQ41/izEl34oWztiOXj3odgYEC5zDmfzdvaD+Uf3Ip0G6wnNOjpRmNKinZ2SIVLhN6Ano8ConIiWwSOtszucGRRo0TCSwOM1FhoKbsZ+nYy6d6pk3+MnQighwlvZ/k/oi293/+m/+KKvTeXnTPPf0vGSi15bKvyxg8GonqMaI3NZMJLpEEqCC4eLnCkBS4XRriUaJNJkQadTVoMqpx5pPakDZ02yRCdAv1G9Yi74RnIQ95v+Dt3lUl/frt24VIJp17L3Kj/mVzqdvIcuZd2KbFLFhkoN1SFdsjGpIbNhko1Z5Ig4dZPD3Tw6bt1lQWqy2LId7w/eC5L92x8sqCzVT4SS4IEwr+vrRPyVsTZ0N19UIg2wPBfWADDVZHEY99gfMfHp/z1inbKwSBeH93A3wP3YPyvpbQ/7rV2d8vuGYYrHVXwN+7F1SXt5F1gYicgtVVkH0eSBaFCTTE3KjkSzN3FEUQfacBSYxA3/FTHbK6vd/z1hWPrX4e67GVakzNV9/+8zZ20L+xmVJNIwl105/xb9FB802SP0ZRNfc9/0MGylznT13b7+17zn0MHk8v2Z+mESiBePcE0v2hEFZSYEsSLAK6UL2CM3PQbVHNwpFfILkLUzAtq2pXELb5zp1vr35k+vmXlxfMWauXjC4hOtvdkpbNPenG9HN/xNzjypl3RlvHQCXyoLZHFCU3V6QnA9UUGtc0oKouGDFmu2SJxapKYzAaSvrPmn/r92PabtrjQcGEbme/1ee3ZUVPQdZ6IDXHQ6fRH3lQk+egNWFEo8tx8aN3H/Xa4O2U5yWRO7urCoc3wMuUM658veDDFy+/Ecg/0demR7ZpSVI8bsPtS+XNZ90yYDPSuHQkDDnbHaOFII0XCmMsuDQTsdpyC3q8PGXgFXf1/fi2j1YQkP7/xqRLo6GQcg9FJZzxgr8mQ8T9uyFEjcACnvcPA8Q8gIuFUtG3tNF+KyBgIa/w0w8xcT8n13XEQZt5JVUQGr/yv2KgwkLGckcKvS4F6scgM7+1y58hSrIXsbhJLJYObQmFsYScEgznKCY+IP4mXicyKYsOcNuKBw2runQLMrs9cVrVO4uWkQy9E139G41r80a6f/ZCbqAdWj8JQz5Hzu0asJlHFEXSxnUMlLOrGAloJHsYY7ZbkRGp2BFHrGFVz0fn3LJ63PGb9njQxUzq9vLS3r8tu2ePgYJgYhTiNqni8kt3QtzdBhqLf6Fe9vj4IS8P2L4saaB7leabnEQH7S8APDyH5d0ztl8hzOoH5IyWHpnjgj2IRw2k5uSivqEePJ7hEnoOMz2vYfMQl8roFk+jZcmyo+t/q0Zmq3evfOnTp148C7/zfs3/iTCXCUPmIKOmZG7er2/N7QXNEziq//FGRU2tEIokRE9KJjcuSaRuGkB6scmnAFUT5WAiKLk9HtE0LTMRS8CIk3CsEYlHUIntDdVQBuzEixMrUbi3cCz/IY1BSaOrOJTF+8s+W+oZ+UjZgIV3D74dgY7HQ3Z7RE9ApLYKiUpzCUva8CLB6RKAlXCmrkgXhZPSkYHSNItFEoCWsXNdEP6MFSdeP/mp5dPL/jmcjUoUk/bsHvoNfjAdtgfdbaBdmhgoHRxNDdTLDVQVRCtetT2BRPjHYx5/6bZ/3nrMBqBf0xD38z4lb90yG6q7FwI5Hp6DHoyBmsaXOP/hO5MelLCvyWNr9xzoYZ6+zHNG0YdHfVh08SNARl8tr4snLb21WL6zCp70LERJ/VgijUvnpHQMlFAa1KuVOBMDidMmmGFLoqFb275ZlX78dQsvWjmzeK5QGAMrpus8zGv7y3bgXvHqNMZ846XOV8OuPw2wu/MkS/MzJBICAqkWEhYh4p3vIcewr1quZAsgOk5KlmjD6jppucSBeDVQ9zs8JxfjrCXfY7Gvdl9VrSYUMf8bXpQx6ep3kTP/3M7XALErEchsJfrSJSZpXImAF4YkBZrLwR8THanGiE+ZlljktAM8HQLNA3B6BR01v2zNHHjxvAuWzH9tblZhJbAXv+9ee+OwPGjnNk8jLpwt5ZAHdYui6EmGuCQviWSI64YRdTxotGpXFHqopP/sV2/9fkzPTXsMFEzoeeGXvVa/d/PTkNQeSMv1O1XcA3nQ2ijMxEqcP2VcUwNN1m0aB7UP1wiEIsYyioRMopMbI2b17BoItJYiMRJDYzAIdEzpJFWXeY82KUtBv42HuEnguK3D59es8Lqvq+HLXZFz7u2PVHz7wXZsKm74nzbQtstd3QvW9F77ZtEMKGr/lLwWgtuTgpiuo6GqVlRT03kU05QOpWkGSl8SVeIz4toDvC1FTHUwDVsk4GRDfRBRYylanzZ/2LY5Py4VQE826TCSJv/v59DhruURn2ijljLP3LHXFWDLvPFiVr9Btm57XRl5osEkZxCaLz8feXKGA0gYiaYTd0/8OPIlusAFdC00VAUR8Hx90+z5c947pde3W9OEUDK0bfYeD9VAp5cz752d2zwDXRwmZnbyK8wjGiLhcWmvxilhSRaJXNBDpq1IsmEEK3QkIj+c/MLb4z67vF0TA2VM6HXZtz1/feOGp6C6ejttFjpzuJyzAzLmvNDONInGVJCrtBvq4tD1ld4Rj90VWfC3zcSot5jBahzYbnYG9NCWSsF5j7bGkvsehKvTMMiprsyWXcTqqjooXi8Mm5AZBJkyOCqDrJKKAJxUm/9T5DhdPd5g21U7dBjh7RnHDpl29E3TPvxsRE6dE+r+T3pRYeJ3LHvqhadfbVdtuV5NT81XVJcYicSQkZWLhEkldBFWoyhPMwggrn9skbegVhSNQMqcJZ30XmKRBtuKRcaPeTwAACAASURBVIC6XdtTep4768K357y6oBPqk4vc3AZtzIf/SwbKpIc/QvY9Q3uMAOrGeFp2bWkwWbEFFVaS0lKTZCTISjlNTlKkmHiBqeMiONL1EoFdYBIZrWGWbtg2+OrRr49/YfKiswRh6360Znfv1qR44z6Bxf5zUF7F7dF1LiLmGUJmJ7/K3KIuajSD69iT7Ay02JIKI2zaHpeGaE15CEbkn2f+451xHwxvuRnokUQSMSbknvNm9/LPHpwJU+on5rXz21QNs02RT5DsZqJ34gT6wQYNs0Qa4ggFf8AlT9+Cl47fhLklOkb3o+dDI15NT+NDM8umn2ZMhZA1FoiPyWh/XIua6rAExctn6wTVncQ2Ur0j2fPiG5LxKratOiGFHaxERqtsu+bXfwahSl99Ed886QShxSawXcRt+udc5+Hf4d7fSWs+d5j73PjlR79z171PqlktO1uW6aE8StU8nNzbMG2YpJTNx88cGk4ewiXPGgKS0Rpx5rYkA4bNa0HMIepnjDNSJCqrQ3CnLWtx4szHe7zZ7ddlzhT/H4W0f0Vr5yCeHBNOH/vxkI9nX10EJbN3fpejvKXltaKkemELjeErS/IuJfcr74mTmxI5Q4ck20hL0VC9c6MNIxyHIL4157N/PXPnxys3NBQNJR6rAxw+e6SnGvGPewNz97oNgcsRph+9AKZ4Gjw5XndKlhgLRgBFgqbaSFgJB08e0aG5021mJpheuaMBov7dwLnzx3/dcdF2pD3qqH5SayPjove61HxUNAO6dZyQ38HPuKfUxd2KZo0VRP4dBPK1YEcbEggF/6Wcfd/txryLf8NmRNGPI8f+PAMFhLPveGnge49dcQ+8XU5IzWmr0DBuMKyDCYQUcYidHT7mRo9Bsql0W0SRQnhdGz6vhODODRZkbE/vVjBl7JcL3i0aXRLEXN7z+i95hmb3pnDU4pfb/jLh2b8jZI2H7PeSuyQ5dlEiM0yGcSQbSEbKeXGTr33OGkJ70X/ZyUPWuUkyUidhNYP1FsKxzcj/21P5FzzxbukszrvjYOSan6H9I+M9CEM75I/wJsCVDIEXlb53wA7fLgTyFJcnQzJtmliRuYE6E1dUDGoExJtcPlDWUmHWN0BwS2BGGDJiEO2YoVf+ujWty0mz2kx479V/fbA2iuIeNId3gNehGShqWQraH/MimHQKvFkelzcD8WCICyVrCs046JBkDRbNoiopfBwwXrs9AjP+yWmvLZi0bND0HahZuMeDZo38tkPVklseQrjhBLTulkHklhojD9qY2yX3MF9x0xYlW7RjDRZCwdXpQ2++2zPl+n/ujCGEfgLJuJDcyp/mmRYxljki8/gRCJdNUtPyVD1meTQtRbRUL+9xOSx3dH1J/Ddn8CF6EC5NAXeKiyt1m/E6G8HqCOK1H1y5+LtHX1zbaR1XR/5fMlDG1JMmPTXo84cfnoSU3IFQUyRBUSHTYtqCI6MuylBUF0zT2k3ByY10t4E6ayXYjoGStmXTzgG1pDgJOGO2UVkWh4G3Boxa9Mx3czr9tA/x974H138+1GVMzT5x3JmVK+bcAm/HPoIrzcvgEt3+DC5RT+tMuaUj30AFQzpvLSSIBEz2ww5HofoU6OEaiCxu2/FgBKHSdya/8v2L3c7psLrQx3V9miU739diKcxtpBbdw4O3n1njhyuyMfXU+VB8Q6Cmq5o3TUyEo9xAeXHTMiArLkgJwph7aWBbt6o3BJGW9tnF771e9Ppxz5ZhbVESU8CYcNSd29r+Mu/SiaivOButumeASYIm2AfwoEEL4YZ12adee//JS2/78tViNKCQP63mkBgHOqL+4OtzlInvjOo25bzOk8AaBiK9Y6YguyVCjxCdYSOJlrMJHZkIYhogdS+D1L/I8cSD8AZciDRU2qjetS21T/+nrXHvLgxdQjMePIb/0w6UI7hRCj/kjH6XjKn58csJSG+ZBdUnKiSHISrQiZWOOHZEGZLqgky0m01/2W6IQdJAeZiXpN3kXtQxYp6iQAABtGOhesus3P5777NunPmzeOW7KD0xiBI+kkQR8n/dQDtuYFkbO7V7AJp0UYvORwd27aqWPCm5SBh0AItwuJmSDPGcWtQxUH6jhgDF44VII+yRWgiSZcRLt2zNLBj2wE2fP/lR0UxEUcSBKwcVQe2bh+53IGTOKgWff9cO78+ZA1dqf8GVrimKH3pch6bSID0Jt1qQKCKyBIimYjPDMMyq9RGtdds3LvzotamLut1Stdc86JDZrOWKh067FZUbR6Bl90xSztUEJjq8N8mK6O5T2nZCXPKgkeCmlidf9tDIpROWPVKIIIr/CgOlkseCTK3jpxclNi66Nrf7+R1r66MeQdJEWiQSX+LyiGScDosSDfFAkFwwdR0wYhyc7A2osPQQ4qGaMEINXx01esakoU9dvOHRJMXhf9+TMqXzcgTWn9hjJrzyhVp6tmYQOx8XgZMc3VTyoFy4imQhHFa03a/d+6zRQHmLBWojG0XSQAnpQWEuFSxUkdmJ0q1RpOS80rNwypN3PD+gLKlc3kScdM9v+I8eZEXM02bbqwO2vT75EShaL9mToZimJPoCOQg3EFyOWoFNoifezm2CvdAtuPx+2GYUerTeRtXOIFypH9776T+LHlqN7RhNM3jEP3vwr6bV3P0YKD1zDcff0gM/L38O/szekpwmCYIbpmFC1GgdCStDNkRsCjLshEgBu5Wo+KUuu/fAV0769IXHX8vsV4+Skj3jZpe8w3LevHX4jfqW769Ffvd0CKqkiWITA03eRBJoIQombD1sI1i7LXvAmdOvXT59yZQTtwaxot0euYA/NXRkKnrO64rVE8cCrpOlrHYtNHeKRIzdpsMnwB2lLgoQ6C1IsBKOQhph5Pn12lFIggFVsqzQ1vXlgd5DHjxu3KsfWr1Qt6w3L5Ac1El68Mt5CJ+kecMnEMC8Wf2x5rnH1ZwWnQVN4YPYvDJNoGpB5h0EbqBJJTinONT4atQAdZJIwSZIuBPi8gpn0oC5jCFFyqQnKtqIh+ss1O348YSrp00eccfFa8q6o7LIEafdX0X3P/CcmDCqFK3mdjuOZBmvUv1pqXqMib70PISrGiAGMviBtYf/LamUvTucZ5BlGZIRhylYthUPWagqWZd34n0PxsdM+LSukLNIHHLUdEADLZgjj1o1Spnb74r+2PjTM/BmdVDVdMm0HL0hWZVg6kE+lUUiTSo5kRhstyKyyK5/VnY44aI5J66YNnfeCiEI/x7SMBStYpkzL7/8uuDaz25GTvdUSJoiSBKtYXL0ukkOyq2foFURGzUVZe4uf5t9xrwXX3vrOVRjAQEAmlW8PoTd2sxHOSUlAmg58izs/Mc18PTu707PUUm+kPqBpGJMTHdUpjQ4YoSwmE7/XqO4HwYMI8rDHQqK9PrqECxrRZtTRs7edtO93+M0DvP677EuMKa0Ha132Pr6uXcgXHu+OycnzRBNPlrHBXplp0DU6EElWeESjfQinvRkmMONkEI+fiwxxSkSgXJVx0CdYXeHQNmlKpxEjBlRO7Jj1Sakdn/t9OsmvFv46OkbrnGoYv57BrqGqZ0eeW7YhjeeLIIidnFntRQgqCKxvcSqgpDTs2Hq9Gz2rj801iEIDC9opGkRJGYYywzVNSAYXnzqO+se+uRcEONjY3vikA6bRgPdD4SVaP3kcWyUOqPXhUOw/fenBH9eS01NF4nhntIuRSOSuzpHpN5icGle6GGbsLhGtPSrip5n3jjzovfvXlT0ohBGryYGOnMLS51wyiWXJza9PxbZffMgk66hTO6ICFZ3s8U5h45TxYUVt+2y7dVqx97PD5j05oKVMVRgFBFGH/rJdHDWy5TB4yu6rpx27HUQlOGe/NYZTFQlDjsk7QsCRYsi9KSBulUPIrX1kN0abNskFm/OAxOvLUdKZsBq2PxdsN2QK6a5T7h38dqivB3/VU2XHOZt1eedQTs+eWguQaU0VdVMmlfgc7AKf5vUXklQREb5NalF7vGcSWQGtymqNwqMdJcd+C5RC3EvKtCIkxPnEpOanTC4TokeDVp2XWUQxi/fX3jbay+nnVX4xbxTCqv/gIXiL263FCj3b1qVN7lT90nITi8UFc0jKy7RoDnniI6UFm3QUF0PyNRGaXw1zlYnidVhEhieY27NRDiOyu1bOp4/4cGN59/0AS6nJDC5kQ9u4zWJUZLw3v0eXmuUBay7Z8IJV51Z8eMPU8XUFnmaki7GiZySinuahESs1uEmMix4PX7EQ5atiEI0Xra8rOuZd0/9+/u3v1f0nhBFXhPSsKOLWGr8m/vPXbfsmUlI6ZADXxo1GgWNi4/SqE5jLpqM8ZkBtybasS2/1iOj3auLq1fOLBR6V4L97Ajv/EXhYupMlpr61l3nbf1yxlgxvV8X1eVTdXLokga3j6gWDVi6Cap8coxFEhjc2Ccl6kkSj2BWwjbC1QwNFcuOumra47/Mv+qrZDXvkE7UQ1zf5j9O3DqDfs/B15deIwX8dyueHCVu6KLoInEjEzrXDUmSdzdWeijL5pxgAoxEHG6vB7FwAydn9mdnIVRb7wT9lJQne8Q8rOKcOMmfZ1qQJZqNNG3Bjuvx0k+2tzrmmuLWlz4/7+uFJPFOxb5mCyh/pYEKdzHme/XMcSfv+PCVR4SWPToKokQwA65N5KgMkPshxkcFCbJay+H6ERQZkkRRBvFXxaFQFc2KMWPnhmq5XUHx9a+8++xTx2GTU8Q8vD16AA9KYjjiJSsQePWyQSMRDt2lprbItC3SZlGT+qAkChbjfVBiBFFcPoTL6iyX3xeNl6/c0Ob0u6d2/ej25R8LQryANTHQgdOYv3bljMG/vT9xHJQuPaWsfK8FTdFEWeSonN0UD8QrakGDAdkl2ZEtG0LwpSydu/OfD4/qtHAXBi+No5hjGv+ajT6KedzfPXxU7JenxgDsNHd+lwzF7RcaGsKiPyMHCcOGHk9A8/igx+IUHHKj4GFdkrhOpPVhBCyqsxEJbYfkn39r7J9PP/HfmXQR8hlzl/aZfDT+9dSNSvax5xsxURN9PihyDAnB5KpujlE5oSkF6bxybZiQNA1WXS3gdcPj9yFOjOkkixGikr4vKXdHM7086XQOiOTQMhIG/KkBhGqqbFEwGDMjDax+x1fj5q+ePePtwHdYyuF/f806NntUceyicu3HaDHv9JQb4et4pZCWm0Z7j0tRNh4sjc+C2OAtKkAk0UNJWhNaajqEvR7Bbqgrj6KmZtPIiVOLPzmucF75GbytcnC9b4fpf6/7P6CBFjHl3bHIPqd1r5Fg7FYhJS9VFP0iV7kVqdhnQ2bERUY4YcCjeBBtSFgsGq9D9KdVHS54Yka7N8/48VMhPVHAmoS43YczVVVWdP/XouFFULOHSDmtFMuQVIgKVST2MlCaBDdgkDKzHS3bRond99c+/dl9n+s5v20eLcQIML8fLto/weEwwX8JMqJfX3CWtf3dm+TMPr1TM3OF6vIa0ZOVh7huw44lIPtTYCeIAsVB3HCXnuz/EbskGaks2nZ8+wbCvH1dMOqNB0o6nbgad3Ie1P9kLqosZCxzpHD0hcD2q5Tc47saIUt1ZWWKcYsG/Z0cVGsU503+SUZK6uOetDTU19WC2RY8Xg+ikRA/S12aB7EY3QatHYUSZKDJx99ooIYFt9eHGOnh2LqdFtCMus3fVLUfcsPjre+Y/MqKYaALOKQq5xEtMGPSeYB/Satz/oadSyci6/i+givgouzbUXHb9510HJxkh1IuG5oo8llQlaharQY7tmNTGdxpbx/39L9mf3MFATH6GUDJwRnoPjfzxwUiWphiEjkWv2PIGRDoOhG6MAIpeV4+iSW4eA7KODSVImwqyTPITKb5MCtaXhqFUfnWMZNWzfznA29vRY/LjYI1ewR8OcL8nCeR8+7NLe8FgqcgpyAHzO0mN+kk4k6TjUhQaP+aAoFydCTqK+MwYtsHX3n3+POfuWLlbYIQJ+nQv85AectHaTtiRYetr15yCzwpl3kz812mbouiLx2xKFEPidBUDbqZcFAzBInjrZik5+EE2CZS/S7Ubfg1AWZVQU5/bvxn370y7XiBRin/c5uy+2L14hva93z9pn53QOp5EtJaZgiyX1LdbiRMKjQ2UX/mMEunPEEGyhIJSG6a2yUiteSbplYkAaKowrZkgLxMo4HyULcxXKZnIRFNJzSPG4KdQCJaZ7GG2hqY9Z+eU7R+ekMbdf2Kq/iB9R96MWX8J+g67VTh75BaXiXkdMtUJQ+ffHAwb/vgJDj9r/NMyCNQOkN1a0kUOE9yQ/UWCw0V3+adef3sss6TP8NMRJL7+NDXl9J6+vWLIZI4dfNFIqZg4O+utxZ0aXlBn47TYSsnCIEWHkVOhSC6OHbaYgYEiWB+FkQmwIxbttcV0MNbV9UBrf5xx4+fPvtY7jlV+GqpieFNPCj5l3EMnhnC0eOBimuR1sELxe+GTCISTQyU73HHQAUrBmZGLauhMth58DkTn/josSVnCkLoLzdQWquRzNt+253nbl654A748nqk5LVWTKiIxgxILh9sykuMGG8lQCCGdYKG0UA3eD5NVT4qIliJiI1obRSRut9annDp5JQVD3+21kGW/EdCu1G/s8y5gweej4q1k5X83gFDV1VNSREJCcMRJ9xAHa4ljoPe7UGJxoMhEWqAFEiBS9MQj0eh8oo1Q6wuBE1L4QaaoJ/BoXBNDJS2s6CABRvgzUyHlYgiXltmZ+RmGDWbfqwIdB465fXP5r45tBXXW/3Lagp7Wz5T0o8dfUXtD0vGiVltWzElQ1VJHzVZmXai7SYVa0K5kZFybLwETVYgkfS1YUHXQ7ZVvz0Ib+rzA79d9eTXXYVqPnZHoBSHkPvQ1rfRQBuDkOb2xyimYCvUsSe/e+zsB2+bCTmls+LL1WQpwPmSTJvBsnUwMc6LrLIkIdEQs72eND2y/acw/P1nTix7ecGUhhYh5BF17D4GShhztDhnLEqXDof/2C7wZHgguShwbvIcGWcxMGBCgk7wKStRX2bkdj162hurFs8bJPSrYqykOT2UP/kUZsLlc9Hp5VG5d0Dy/N3bprMnboqiZYtQPCm8cCJY0aSBKo5xgiYekqNotD6RBmS3zELlzg0G6rdUAtq8lWzH84Md4aXDCoMO6SaHM+mKvl8d/dLdZ9wOBE5zteoSgOST4jEBmqI6ZMvOHFUy/2wMdR15B7fbY0eD9dA8HqbrCYHV19B/0hASE0SFfIuoEDUp96Ac+5f8WY1e1MnhVFWFEQtTfxhGLGTZdTsqIWjfXnnr3Q8sePzSdYKDtPrr6gqO/UtD6uBfkZY1Ha60swR/ZqaoZUgk8uHIXzaihZKwPoqJqCBERSKTtD4lqFQ0oxw9kYAVrI7DCq5qc8mYR7ctHLcCnNWRQ8z+SELwD5ePh7h/5EELmPLEi0ifOuLcwZXbVs9WvDmZgpwmimIKbEZ7kDorBpgQ4xpVxOccC0YsAVqU1azahMwRzw37+rG3l3bmLS7+8d3gMH5ED4ci/Xj5CGvTkjHwdumAQE6KJrokR4a+8UV1exMaTNiiCVnQ7VjlNsOd22L+jFe/fOLGX7ENo/dPpXFIG/gAH26/mAU23zbidFSsmixm53awdUnQPAExQR7TMqCxGEx+YCog+TcSIybOmj0zg3FkZAXQULPLNsKVBiKVX6X1HDJLOvOZldWPZob/6gLJqFXM88bI80bU7vplqiclx2fAJdmCW2Rw84ICI2U3Lh24x0Dp7zzMpbzMpAoJZfyGjUg4AVPZxFnm7VBneP2ypKZIhLJSRafFsqfe01jFpShXJUwuYOlwyYxoN2x3mt+IVWwuhVuZuGn1Bx936NCh4S/2ogLOYT5pzTWnWJuWTva17twxmhAUQUmnxkSyf9sIRGg8K6j9RKgcE6ppwSAQACRIggjTsGyjurQaubnzhi//ZH5xV+wE+hEJzBEfukkM7n68L1OXVSHzzK49Lrf16H1aSgvFFlJExrxOt5p3LC1YiEKxdUiaC9FQzELEbkCkdJ2769XPZDxw20c7h/cIA2s5ZHZvA6V7bHPF2di28HrSSIE3J6CJmuQI9DRtszhqWbJI03i6najawVyZWUvunPrCYw9u7v4TigQKJo7IizK2WCoUCrHYAd4380C4WKj1ZITl3+xt+Tg8aafAkxZwp+eJsZDO8zCBRRwlbv5RaufLoI6GQ89pQyFFuHgIksYQDVZaRGAE2bXkiVW/zbi1R3E5UHgQUw6He+Qw4aU1aHXF0b3vhpq41p/ZAjGDiWZtA7QWbZCIEzVjcnn49ZI4El170jhJEMk0mSbYRqKyLArVv73V6TctCgVDRv33c6+DZbYSM1p5Iaqiw67eBGWUBCpwaItuQvF4oIgCoqRVKQlwa4odqiuNILLjyQdf/mL+pO5ddqLfH1c+mwLJm5zlDir/gK9VyoTFBW2mXtztEchsCPyBVH9arhiJy1CEZIjOcQVJeYtkYZV+OD93+Nvmb5nKpGDxWPmva9DjwolFq5/8pajF3ChKRxtJ7ZmDuJ79X/CBDLSEoXWBq/ONkKybtNQ2MJlLlC0FuqhAUiQQv7MNHQoxUqpuO94QNVAXrYfZ8GNq39Ev1J9z/XLcPzmKuUWkANDUQMl9M7H3Ry8P/nn+FdcBLYbIbfqmE6KIFIhp8NfpelP+BujE6C4KDmt3tN4yanatGvj3G6bWvXr3R2ud0vQRnVaMMaEYxWIhCg+QLyz1oNsrp+O3rya623TvHQtZgpaWLzJi7LaINMGR6LM5jwC9qfrpzEnSTKvHrSBcVwlJpkpg3LJqKjeh/bA7C+Y89WXJqUlRoQNusMP4wBymtHv56fO3fP30vfAGuvnSsxA3E6KoitDjYWgpPiQSVI5XIAjkM4kwXAKzSFGcuF91W9ZEy6yvCCNOwqG5Hz1es2J6wy6IRT2OvRMNwZOQ3qZFIL2FEqyphehyw07E4UlPh2np0CMhgltzblaLV7dp/zvTMs47YiG4ZUXH88bP2HjvqK/Qj0P//pLqdsFUFtjy4qjC2nWfznDnd/K4fKliMGyAuPecC6NIovHdWDSTIAoaRCbBpCkR20ZeXrZdtm0jQ3RHNTK7Lbrq5U+nLViC+oI5nECN6FaPyDgPuMqMeW5+C0c9eWH+Awi0GKJ4syU+EsnPVWqvOBIUhKumkpYsKLYeiRuoKK8CIqvbnTF5VqcPLv56mSAYoxjY3L09qDMXOuCuz/t8N33EA4BSILTqGYDoViBYIpXxG70oL2dRiZgvqg5FMq1I2fbtvYeeN6Xqg8deKx0tGJjjzAsfcbGlmV7UPg9K8EzcmhudUjABrrwrAm26e8JRQbISOlSVT9AnR89oUIQLoDu1An6w09ct0mmDximAdTtcURqBK2vRlI9Wzp7Im9oH2TM74OrtlSYImIkA7jhpEhC+TMvIyiQ60ZgRgeqREI3WcpgaD1o4ARrpMqv875yUmyuZ6bYVqjIgmbUIrl2fdcLNzyrjHl1+8dmwXym44oTKH9+dAH+vroI7wyNJqqhqbsTicZ63WZYOWRNhmgQ/pp42tc2oFiNz9h6B9FeJr6J8TRnk3Gdxzgcv4q3pNUDRfiOKZj3ogZ8J73uOmbGt17PjTpiIlOyzUjJaSRGdiRb5DsmVdI9EbUNDJ3RGJEkMucYnXSsVh1T43S67qnQLkxTbsGp//qDvyBee3YXh31csdOQDkxy2R2ygf+BBBWxiKX2nFp/04/PX3wp/l2Mkb5ZGOrdEbKeQgFMjb7rgqPMxk4DQCR1Vm3cB0lenPfDl09qkzN+WUp92FBjZUJMQ13maBY/Wdyi5q+s4qO4Rcm5Xty26JEZYTpsIuBivIupJ9nYqQ5h6FF6XZDTs+K0ut1fBs64H3pq99bzJUaCIqA3+4sJCY2bEFKSdchbqNxchp307RU31GIYlSqJzCbvbK1xMx/keR1rHhkyDsy4yABuaIqC2qtJCsG5z7/PG33fPw9ctKezxp1d0BYxkHv/vz/YN/fD4E/Bm90rNbCER1xCsOOSAG5FojRPeUi5t0/FLfzqaqCapYXFwkG7H68rjCG/eCXg+eZhte+QeAdUoAOatgv9aIfVhSK3PhZaZld6yHaLxuJgghJVIXjIB1atBMCJIkBoG8cTydZUhixRIq5AkG5GyDXGYweU4asyDBT+PW1cC4WB5nA4OacQP32WeHid9eMmazxcV+Vr3yVFcqWJDhFp4skM0zeNX8p5U0W7MyR00kab4kKiphyc1FaYes4V4vZ4IV4fRoteUojWvFRedtbUWK9o1VtqO2DgPcN6QGl3mx3+76ZKG794dLWZ3bgfBs9tAWSOnO3fiEjRRhm6JNovGoqj51zaI3T6ZW/HlnDmZ2FniACmI0JfaOXu/cuezrPJrO1wLGLeLeV0DkL2OCCX12ZIlbeo2Uc+JytrMTFDPyQrtWF0NV2DJ6a/8+uCvF5QFS/vlGw5U7D8zU9h9MWu9dsTRY2HuGi636J/jcvmlaDSSLD87ELfdK7R7uJuoP7i0FyxTh0vhZXq7YftWA3Lqi9/o3957nCAQPfmfFdrxec8zn0b+BxMGX4Nw5Rg1q1W615+BaEOIhz/ugBuxGIWkhB+mp5dUFCfvycsgdL3E66rbiXBNBPXfrc0aPPGJqi8efs8hV+C4aRHppw5H3YY7IOf1yu3SE+Wl5RJhV90+HxI6idnaEEnFg2uukhgVc6ZfmAxJUCHyOl/Uipeu3w4l8/En9e+W3AxUHMRAQdO6Bh2F+zMMoXsRU7b+8mVGdMkVU+W0zIs0X7aqW7IoyF6OrabUiuedfL8m/85jMkdfxbAlyjkhxKNIGBELerABsW1rzp2z9q53RmM1cJ8OzP2TZ5P3Y6bDmTRjMVre1f6cm+1tG/7ua9M5OxJjEqc1ES2opF/baA40TC7I0GTNjtXVRVDz7QakDn13cd3bkAR6TQAAIABJREFULxX2K6lGST9zOINF2jD/ZqCn1LLAp31OORvbfhmP9G4dNG+ay+BzvwyqbXMyLjJQSZC4/Dx1FxOxBluP1IQQLV85eNqKu3+/q9P2in6CfgADPfLwd6+okXnyznn4jLKlk0ZD6NY3pUP3QDiiiw5IgQIw26HF4E2zZD+N2bBEEaaegNvlRjwShdfjQTgYslG385cWR5943wc/zVvRew9Xz5GewtJUxny/PfDpoJceuHmS5kvrJfnSXUR+RZGO4tFoFptyeggeFxgVaQUJmkAkbbLTJbEsCLZOM46WXbm+Boh99fJv1U9d3q34W2B444SGMmz8mk5Lp515G8TMS+SsPMGMm4rq9YuiLCOux6G6ZFhMT5KKUagvgBGLZ9JIyWi9XgHBzWvDgPXd6TO/ufvjFpmrUZhUct6/O2m6p/7geRUo13y0yv/G2PMuCK7/6q7U9v3aRhKiZNhUtVUhKBoYkcLtNXTitInIOAmfzIJh5Hdoj9KdW22PC3p054dbIfZcNPyHXxcWT1tbjuIeR1wLOXCUnvzEGqZeshFdX710wASYiaGenFb+uM5pHTixHVfl44cMPWuRuIwhC5IVr6hsQGTNGq371YvmrXni3ZEC7ztbw5kj3vRvBorlzJU39a5jyj5+bgLcXf+mZeQGTBI5I44/i6hYaSiaTlkS0LXhIkbsUK3t0mw9XrHh96MKb3z41NcnfPFYSUkQB9C4+LPyAucRMQG3VufiuaHXIv77pcjq217V0iXLlvnD4XxF3EiTLQcyVGp0G84Bm5Obj4pd5UA0Dndqhh0r21oPa+sbd8/5aNbGUX/bXHzk4AUBC7Zo4/Lb5n32xB1X/fTp+2O09BYBW9AkwxQgai64vW7EjRisuMNd4xRI6E8aG5McAkMq2JkxWLFgAsHvNrY84bYne818bMmHfYVap8/nZElXLmeBF8/6+0WIlowF3K09LTp4FM0jBRtCvAWVkpOFRLiOy2YQdpd+mUhVbi6dQd6awa0JCFbtMhBasy69/5jZ6Xc8snTjcBKhOuJii5AznXku8P7e4dkbuo4AWl6LrDYB2ZUmyloA8foINJ8HCTupAJZMSmjJiLGRlAMIhWObJq/Sq7JtRXesrQM2rMy7av4jZQN8mzCoOIYexY0T3Ed6sB7YThnzZBQuGFDz9uP3IiVwjMuX6dIJUSoRjIBcBEWsFte3NUUHIc5MwzYrq4Iw2I+5Z973yPXvXfBTkeMMnAJJcyEubfSBU9e2+nrCgPsgtTsXWS1TZZWyHjq8Kcyl9ZchSwosg8SdiWY/DmZHjXj5hoqWAwYtuWb5Px6bvBnl6FFoJcmAmz6g3YfCn2uglEAzJaXN4wUNb02+F4EOp6m+NgJEl2gnwyOLF4ycU4y/6WZ0E5riguL2IRKKgjVE4M/OQ6hmVwLBn9YrbXvNHPvxt+8/1gU1R9gXlUatYr63b5l4VNXXr98OX+Ak1ZfpsZgiUjhHrQ5IIgwKP7nXaMTOOpIWfC6HMO+2CcGK22Z9eRDx4LK73tv64KNnl2wDSgxg9J6UYjhT++dt7/X97LNvBaIn+1p3T9UNQdHjBs3HioLbBYsPsVOF3oZi287GBxUwqFhEcDkS5zasyK7NQdjxJZkXzHm2+s2Tfj1osPl+t/Uo5dRpM7M+mXnhqSj/6DqkFvTRNJ/LVFOhugOIhWLOAYUoR3ztmeRxWmTkPSkI8rhUhCp22hJpeVV/t6nt4BGLXvvi+Q8GzBV+w2h+vO1NxnRgMzvsT7RnLLC55SXnYNe3d0i5XTpLikfRLVvUJJEfglxljXdCbFA7lOI6W0/YVg3x8+YvOWP6skc+/AZlmDvawPC5NoqBxYuH7wY37nVhCxhLvUoQbgbaX4ecznmKRgLdTQ1UgUx8KkRcLhB4RUKwrsKyG0oTWnbWF/d9/sW998zAeswtToDaJHvnoX+dgQLo8yPL+qlv5u3w5dzgTuvkIeUr8pxkpGSgikC5QHLdyEBJrlH1wKisgxTIgCzIHGgerNhmw64MQjCWDLhm+jPfzbzgSDem+uQ6lnl/4bVn1v7ywlh/q9M6m1AUiwo/VCuhAy8R56NTUsAPiwa1TT5encTwOOhPmTPBJuxE6bpNgZ6nzhr85PzipScWNwCFdEw3qZwzYSFD7kih3whgxwhXbq+u8WBUdWXkiCSPEKypRkpOBgxLh2HonI6DT4ZAhiYoMEUJVqgBLr/HjodrDQR/+K39adMf36zf/CZW8IbkoeNZG+tzeNnz0Fvn9bn3gtZXAClnI7tVRlpWC6muOgQayIbqAXQyUoOLHHESWSbzIhYdVk4lm/FIIz8vwypd/Q2B+j855Z4Zz9Q9NOzHEqeLQI+uEdWQRPkdseffnwELA39keV+fefb1KP/nSDG3bwuSDeajVBzynKyZ8eF7J2Ihz68IzI5u21AKX6u5U0Jfzp0onBNBwf3G8FWb7eLCQpCA8L+HuM4leLSCGwYlSj55BGl5R6kpKSIJzySiMUpsYSRDLkXWeAvORRq70G09UWeYlb9v63PTlHvbPHn1Z0uEuZG9TvXdC5QMSpsXPz3sU4x/42Km9vzH2FNXv7uQyK67qlmtFa/XJdaV7YQ7OwN2IuKgari4TiNCJ1kVtJ3wyXmyVMDclkD0t3Xpx171zMnfzy9OhrnUnDzUkEmYw5h70uB5HSu/HH8NIAz3txmQlTApaiXqyCQzIcc8J1Nz2kvkRIjomGsCqWCRCJFOETDEQKLy86FF30z8qGjONmBpDGytc+rsOf6Al+FxzZnVJ/71EzdASzlTSclSLSiyy+0jwj/RsI1k6MWgJtkbScOEjIAraNqMaFzhccl2zYYfqgH73UfWbJt+dw9sdwz0sDa88ipjWZcIHS8CtlyBjH5dZU+qKikeKqXAJo4efic6NBq343xSCmQthWZ6YBnEks+gSgIMwttGanWEv9kA9HjpTfZL8YWCUJskJKcH2VggatxTh7puTffi/irTnGc2+9bve1XOGjAe6HqSq02PDGbaUoLnJHRQ8JFqZ1iOUgo6ZJmBaF2FDdP4V7sTLp6+xbrlU3w8M1rA7rdKCFq5GEiC8puzh1EKCvI7oOTZ2UjvMkTz+4VELCqSzocku2DpNhTNx4WKCGpFiBzbjtmGXgcWr9PTjj7uwbMXzFm0sATlKGy2TeFswyQ6STjynGavm+g24qE2vy2acw2gXpfZfUBGdflOKSM/H+G6GhARtzctgEiImP7p1agZSbmXA4GjE5rmCVNTBLt6y9ooEqH3MHT2NHx4yXo4XD1JKzrIs4QxYXgx0ooLTx8MLBsDtc8ANTPXYwuySMZJGSaN/1CrI9GIuyX2A7eERDzGNUdgGHArEsxYyDLKd9TAnfrSyS98P/OzBIIFV861xmO03cjov/uqiBj4VqRjVqvRQOk1gTZDU12egFaxfrPkbdkWCTvOcS0ET6U3hxHy0Ty6ewnEq6tIEiTYdqh0mw4j8jO8wyZj0tTvMIHjRQ+5up1TzrwVLS/oA7PkJmjek5GelSrKHlEQKZglBghHkpYMVNB0qMyEzjNPFbYpQ7UEWKLAI+BYpNZmNTtrwbavDJx047PBzx75nvRMIJTs9pz7rNDhGOgBWkbUC3oi0OGchnM3vftUEVK7ZkNLUzVFERMk7kWPiO8ryp8Fru1FqQMN4jeUb7UQKV9+8gPv3StNOm7tMmGygTlnM4zuZy9mi1EoFO7XgyJ3Cssqn9huGtLyR7gCAcSD9YrsS+ENViNmQnb5IREAUtU4NtdmMRjxWhtGkMHnfX3krFefWqu1/XXV2Yjta4CEEqIB6j89B02uRhFjalH+lb1Q9vkMZHXsi3jEk9ehg1i2cyfP8xSfH0aCZCMcyk56cDQ8y/E0SbJnAjh43KIdaqjRWcXa7UDr585jv760ROAzkodsoC/uQPsrWwtXAFmFYmbvtorbqxBBH7k8YmLZM4ztTK4QPE9zCUjEwtBUFxJhmov32WEylFjljz1Ov+mJyz+asGwxEG8PwS4u5icuvZpQiNLmmexO7a/3r/9+yli4ep/iy2yFcF1I82XnCnHTMVCFwOhJD0pT/rShOHDBFnkrjU79cLDWQF1pGbSWS+7f8vHMyfnYxatuBwXl49dFrtl+mCH7nvxBD6Fs4yBkd2ojah5i7uAwTJWCdw6CoY8bgByDJPOGHizi9DEkiJLMYYkEM42Eym3UlG1Ebut5D5d9+o97gFrek6HzNQmKP1wDZWyVAvQjmOkf9/Kpl3vhB62xYsqNqP3+LGSf1E52pWmEb05ICW4bHPLOC9BUvQU0mbxowg7X7jIQ3LJoMYs8UAiUo99ohpK5ztxgcrxzfyEu8DHz4rqLx6J6wwRXVoZbN0xBlGSRHqRsyUhQA50W0O2hvgBMyYIgRmEaIQvh+lWn337vY+GCy5Z9fS6vSjV3kxyw+2d7z90LcgoLYOWgMdA3jXG37p4Xi+uCpGrEVwkrHIXmJcl0Z5BbT0asHLzQBGVkGnF4vC4rWl0RRSj6dd/7v7z7x6LsdYcY3gkoKHX3yP/HGWuWzn0InpwsNTUzxRJE0h5Lki47BzsVP8iL09sm/CxJBJhxaDQyFqf0QrQTu9ZXA54X71i05qXHQtiOEoH6zY4YqPNyvIdTmKWX/A5j6ecKR40AaidIaR19ijtFkRWXEDUSYBITqf20m6A8KSXBWwEEXBAkPsoWDQVtvb6KKlhrO547adLG1874mldxDj76IfQ/et3wyaBfF9/zJGLxDkpOS+qniBZBGJMCuxwfzEN9EjlqgKbQzJQGZjiaQFCdHi2zoohX74hCsD9se0XRlK3nnrcBp9GY7F46JU2CiUPba3wbUER6IANdxZSeS1Z3XP3Q6dOF7PanMcEneAN5YqShAXAl+7fcVshIaU6X8MIUn8TtWLC8Hp6UaTPLvnvltsmTa7C0iKGEV+K5gS5ezDhBTfOv7kztN3DJsFXP3/4o0jNaBbJyhIZQWFSJO8+dSmMygOyG6nJDJ61ODXC5CdAQsxOlW0pbn3DSs7M+fuE5o7g4VFj4b4Ui7jL+qhDXuSEm5V304nFlb4yZIOX0PNGKGbI3I08QJU0MVddDTkmDZDrEz06l0IZOM65NCn8cqE0Py4gbiEe2wd3usY6PvPnOxquJFe6g8i96vtoZb7LMDy9MvwvIOwu+rFwtLUdN6LpIrJqNujd0SDjGKUG0JdiE3VRMWKoAMxK3KefSg3U6GravzT52zMM3PHPvV0X9ChuAYse6RyXfm2FT7rJnZwLoXe7RYpOHJDZ8dDek1E7wBPyuQJqi2xaYyERVTBpoMl9yJmYkCIobzGRI8XpRVVlhw4gZSDToiCee7DnnlxdXj6JclHusgwgdmXAJQ8areaeNERAax0TNrbgDEg0yyPzxE5VLcmSVEyVYMFnEOWTIMDnKycVnKJkVQyJeb6Nyw7YWQ4Y92235s/M+FYQYCkAbnKBRTYa7k7vhEJ1BEw/6x2H8KOZJW3/XSXUrXnoEgQ7toQRU0Z0h2oSj9iQ7BlCg2TRWQm0yA4IdhWUEDdRs2HLBfTPHvSVWf4814xpQ7LRXGGM2p+jZjYFrtuixWLr4teG9X/9756lQMCi9Y2eltrZO1CQXZH8GInVUsPDAIra5UBCkVqR56LnE7UT5lihSfEtufGzOfU+NOK5MSFLr7+sxKdTllnrwp/B+z5NmvzAxkocpR18KMTpByMj1KGqKIgiaqBtEVkhhFWU2FNtaXDfSuUzabyYXJbLhcg6gcNCGGY2getPyG+dXPfj0VTjYiq6AgQN9Ga7j+9d8Nu9++DodI/uyRRLOUNxuiFaCz2pSpZLKNRRSitxIqXfLYNhhqD4X9IaQrSgSM3btqgbiH1wzb9PUF66dW4qCEoOHRPw8gljgOFLntHGO3sadKt/+Fmv3+AXt7wHsU6WMrn5Hd0CCzQ00yZvrLEYy7Jc4WSdRQ3rdXgRrq+H1uuxIfSnQsOmrtNOvm1F309+WY9iwgyma0SCGkvPqc4Mq3p56J3LzTxYUn8CIRpNXaB1st9PnJFwwXQLxLQEGcafaxGroInA5LEOnegfsWF0cdRs/OHf+yhnvXNV9z3owxlUZHfj/HkM91GiNpqkoZ6D57/1uOsaEjguQuXFsr1uh6yMhePK0/M5iot5whh0IhEaFIsqhCedMXtQkSGcEZn15CPFfls2Ls4nXdhV2YssoC8LcJpXxpG38wY4XTl3H8j459vRRiG2+1dWihSceSwiK2y9atkJyNVBtGaYkw6JChmhCUkm4Og4zVG0gHlzjyc+9L7J+1VcCnW601M7JsNdpm8xHD+IEPjTbTJ6bLvncB48x33n6Xmj+o+DLTIPgkQTRxSMBunb+i8kJcF0PKtHQPZh8wNuyXHB5A7CMKIzqUgvhbWVKmyHTjD5vLsISgejBD9RqEDBlfiYmThwDwT8CSmr7QJtuQjAUEzWPh+Bpjjo4TG6gIjfQRg/KoNsRqD4VeiRmwYjbqCn9GS1OenHxzuffKCQIIlts7eYgdgxUbGKgjc+Um+pMxgK3CQMvAdZdLWYe09ZmQkDSSNyDfGiTeezd3D8SxdxQFRdsi6ZbDPjIQMNVsCo3lqFFm2dxx8lzcHsRVdsO8ByYgD6v5WH7rLEI114uZbfIkgVN5PrDzshFMgd3UlqRYKXEOOAWoRsU4FKa6uGFdz0agiRbtmVFqpCe/9Ata4vfnDV5chD3l9IGT4aHTfdK8qQ6ZA/KDXQ/o47Jn1/E1A5rpvba9Nbsp5Ga2xW66vXldxbDlVGomWnQjQoOqeTDDkyBxNeXBsyjtl62uQyQpz7MVr/5ydYliRVtz4/wzbfXi1i0/uhFk6bHPXIqvn1mBjJy8wXNK6taipBoiIpKIANGXIegKFBtAzrf2JQCECeQYVs1ZdVQrDkvv7zwpVnvrasooQYssXX9Vd5yP/eR+RHLqx7a9nxg21VI799JUVO8hi6ILm8qDeI7otQc70nXTwFvgodXRPMoKunQq+uhpqVAD9XYsBNR1K39Iqf/A1PGvXP7r3fmIrr/UJePzmidr5nWb/382fdB8BzrbtnFS1jTQFo2aisqARfB2hwPSiEuJaW8x8eBCQyiJkJWRNs24kZ0+9Yg7G0f979y+XMTFvRdd75QGAWKmy4oFUbo5TR39zzn5BoPV654anGnl25SbwJyhyLQPlv0pijEWMqnnBshdbypTj9IhGgr8Ln9aKhvgKzQeJcNPREEqraFkeb7tOWs58bvHHlcKUDsf/sNc7kUX+U50/uVL33yAaSk9/PmtlZjBkSuM8P1PcnlUROFcd1Zh6aGwZIYmG7w1p7i9vI83GiotQXVjrDKHT88vG7buHtSsBF5yVEyZ385EiC7/adTjDzUSI1Hd44n3o/zYBKOn5vdx/fbeT99+MqDSGkZ8GS2EQ1bA/G5w6MAIs25EzmYCo0psJhEvU/AiBixspI1Fz5RdtWy+rLS0Nlzo+hX1Mw434ENVMqfXNaptOi4RyD5joc73aOk5SguX4oYqqqC4HNBJeIwyh6IZcG2uOQ4JyyLBykEWTVj0Rv3PPSDvLr+iVcjQFMXfjge8bC+R8WQp1tjxY23A3mn5/cY3CoYNqRIWOfoIZJio/4+D6z4AeOEuVRAsi2KAl1QZQmJaAi2EbNQX7cDVumzn7Cqxaf2QDnW7hebKi1mLL1QOuoasOAdWnaLgNuXIdTXhUTVm8qFkERFg0LVPm5Tjdw6BKSmNzXkRSRIIjBSG4W17ffWA66aF+1+4/sLXmhVP0wQzOFwANX0VIYvhkiFXAxP5p/75oV8w50f6HJGh7N+//DpCfB0by2ntnCbxLalqKKkKly3hqJal0dEvLoGojuTT7fQbLhkGzAlQFEYDD1sW9tW70TX44ta/vbG0p2jR4cwl4PSm9vMwjuM5Z2b0vcOJKIjUtt1TauvqFGE1HTez0xGOs63UoibnJbiNJu6jUB+PoKb1iO1VR4NlNteF6y6zd9sGXDeLbNHTX/wjauno35UATDnUxD6Znfuvbs3kvwVh5pGscVMcvqQ+601qE9+w7rcfFzGswi075uS3lKLGYQcFSC7PTDjDYBG0zeAJPlhxxnckgtWsA6JUG0Iiu/9q17+5NYPtiFa0abQQCE/bPd9fgfwoPT0qpgfLQbdCH3D+dC6txdSMwOq1yeRAqdJIjDJQVoqtHDhLYJhEeDailpWZEd1dvsu44v/9f5ngxyen2bnCf94Sv2wjHKvbzpq2s6Wv4xvSZwXY9XsE/rbgkeVmCySwC8P7hhlW2SgBvTGPJSwxswFZpJ0BPXhbNhGHAkuGcFWHDv85ufzH7zs6yVtEWyOO7VkFHPfaSzrtXzBDXdBEYZ6snJVC5JIupaKy4sEtagEai04PTKHc4eG4UmmXXDyFYOqN3HDqqdwaPOyq2eseWX+9PBPi8uPMgqpygdYhXvaKnv2OncXzRjLmjVq//lf9Pr+sftuhyf3LCWtjccwCKupQFMVJOwoIOlU+4MZoyFoL0Q+k0nz0PR8bIgyAQUSYLWVYYRjSzq9sWHqhgu2bgHaNd8fZkwpuOKZQSVvPv2Y7EvrZNqKS0tJExPEkM9rlEmuIdqa/E39YOLApTlKKkK6oIfrIIs6fBrs+k2/BuFN+Xzwefc96BKHb162sEQfjs029fWB4XuKY/TPwzVOot0oBgRebGueuHsBY4EXLptx5ldvvvig5MturbgCom4RgYEIQ5Vgm4SEIgJ1BdAlsIgJj+azjUg0btRuq8xoPfj5K6fOmifLMKZ9KkQxt/nJrz8OcflNMjVt4JRT676ZOwnpnfoKst8muibV7xF1yqGoGUsS9FTspLAoWYmU7bidaNim2+HQvM+iu2af7Nq6C2j3H6RwbGqjTPCe8krPyKeXXQF0GClnt0+H5BYFWeMMC1y7hJmcJJryUY0Xjahtp4LFLSguN1wqDQckEK7cZcCIbodpLlywZV3xVW2xqRl9SYktZ4pw3tCRCP4yHp601v7MPCEUiopufzrnASJZEZk3sBsZEx0DJSCFw1hBoRBsFo1GEP7hdwQGvr24/v1FhT1QPmrNXDZXGE0Gau/XQJszUsaEm3Yg76m/nXgJdm0aK2R3zWKCR9Ekaj8RuRidnyTPrnPWeosCeNnlDHmTgVK+LBLpMenyRC3s3FrmG3b1neHxD3yIQRy40MQsnKjy1rUs94nurW5BIOV6WJrXk9VStEQFexloYxeuiYHy0Fcm9TIGS7RgRups0Y4bdvXq7bndhz21cM3ChacKc6PAaB7Wk4EOd+TE97ya+KND9aDJH7I/kIJ0W5AFZuYfe5ckY5SgpfmZoIk2DWLLxKJkISERWi0OweUCi1q0oHApbttOxAy95vfv/nbm0497B5/25acTEMVwgbx/c8Wog/CgdKQd9017fDNwHNDyLLXFsWm6wVxQRFFTiFDYQUtwD0oInGQfjzhz7WiFoVdv/rbwtiefPfnxyz4fLaAuSSxz5G7xUH/C0SwV/2ozDDDukLO6dZY1n0YPlIDqDpqmKecN3Q8hXmUQG5XmcnNYp2CbiFEuGq2PQ6/85qxbHnn6+Ceu/mKCIFCy0bQoo1x76c/Z814Zeg+01EtEn9/r9aUhFI2JissPUXZBN0gMgPIvAlM7CuGN0w4J24bGOV9FK7FrczWw+utWwxY+b5196Y+lBQiSinnytS+UbT+o5ybe9Gfmxdj7j8UXs+6Cq3N/MS3P7/amitFYnDPNabLNPSlnrKexUZmGt51nJNsmdKqu0iwpTFuv2KkjIT7X8cs1MzcOECr+TVN0DlNavTbqjB3LX79fy+3SKxFhUkbrTggHww5LPHnQJrO5jSEuSS5y5gti7aBhejsBielWfNvaILzpn/Q+8fpnzl86alWRUGIRmKARnOEsQBOrPEwPeqCtVcSYZ86Q+wrKvnhhkpzRfrCkpkrgkysyLFWFSb1rVUbCoMNOcXRyLGbbcd1iNVspjyg+/vLPnvvSlbYRJaOpEr/f9ODAHpSudhzzYkaP64HY3Z5WR2lMUF0J2xIFgvjRuE+j8SdL5QkC0TMDqhU2YmWrd8Hd4e2nP//hhRt3YNNBzBMe6Pkc5teZdPbE0t7vTTnlNgjiUC23VSrxftBzdS57N/vUHloNE9BossPlRoz4Z0n8VoEdDVdbqN+1C+nZr4x88cd5CyeTjsluBnZlzWLm/9tN5x3TUPnRvZ42g44yDcMjyi6RigSGbkP1pEBPkJ4IzQoSvI6Il7kj4BMPfDDeNGw3gba2r9wGdC0euPgH0kupRl4/A3M5lM35hn1f++zPf1c6L1AyF67KrB7Z62ogcjUyO7b2p+UgHI2LjOZEUzzQ9QhIkoeRuKxI6B06SBgky+BDx/RPVWYwE2HL2PqvnzIueeDumi63foP7eYVt9xX1WMzS1lze6SHAHi75M9J9KXkIx5NsCUlNrsbJKvpTSxJyO0UyokyyIbpogxO9S62eqNixudew6x87+cYJHz8xdHINcD+ff92nLcCLTrxU9FcYKGPCw78h957ubYYDDdcpmQWdZC1FiZuMc3QxEqOKhbjnpPVlRMqmqIQWsyN1lQx1WxoCnc+/Wyt47N3Kn1GHtUlRU6cjtE8OeqAiUeOjZkxIP3rSqbU/P1QI5Zhhrpy8dJsEbUjXjUgteaGDcGtOou+0W034JWaHuLTC/yPvO+Ckqs72n9vv9O2F3ouAghAVxd5jw7KY2FuWDzV+1lgSDWgs2LthrdhlNaLYGxixsxZ672wvs9Nnbjn/33vund1lWWARNeb7zy9IgNmZe88973nb8z5P83f7lE1/qOTRCz56s1DIgmB/oqH99B8rX8NCFQMnlAFrrkNO/756IJeaQ5yViD/MtgqmmxVzrLPIh7kT4TD/95yg306nwki21MSRWDc/eNSNtwx+//pFVW1D3Ux4ahYKLjh37yuRXHNm3vDDcyPJkG9uAAAgAElEQVStEd0iplqSmkukIfmCnFw5i2SiKJfLaWSJmRk1eQwgTbjb734cec7D0xYff84XKEOM8l0+zOvYwU8wUNoGTMHI2w7AkvuvgLf/EVp+D9liomSmaBY2hFQmyfvDNG9MhwXXE+VD+1TIcsJcJjMunZFY+ek6QL/z9tbwvOtDIzYCS3llmYYDJgv7jAA23odAwd6hgp5aIk0HugYrkYKSlweDeoJu25bn3i6VDq05r2JTpcNKIeBTEV65pAX5JW+dftsX00MXYUMF74sxw6nXuq82g+z8Nz99z2zzk0uYitEXj4JRcRkw6BhPj6F5hiWKgkxTSZQOKcjEW6FpOpgWQCYSg0hwRZaxrOp1ESC+/Py7vr3q6VX5i/ihPsAtbnU0zQ5pb/c8KIDf/zXZ953bJ0yBHTlDLe5TIqheySINTnqI/C6yEoXu78yGV6BUhRj/1tbAP/DFw+74pOKTS7Dxl1Q/2/GjYArUm4Yg88QN8BYc78kv9Zt8gMth/HPK/e4n8OqbCIsmeDx+jt1VLAuqV4NpxGFmooZRv3QzUPxQ3tTlrzX/nXQ/BOs8xvSXS86ckKr78K9QikYppb2CgiByoWHZFpGm7+GjellQgBuWkWSgOzzt0Kem7UzThgxisWevW73unjsGnVgN9ncDwjjs2ECdozj7SLbeutk/MQF/RikqDrkIRn058kuKfMF8wSQL8lI7IwVRliCrMjIUppGRShIy9GiJhoSmMSRX/LdmcwsSS57CIXe/iZyrFmM2YZWB8de/NPTLe/9+GSTxeOj+0vzCPmJTQxTBglJE6uqh5OfDoHE6GgpwO0MOksiFO1Key+KQRVKiy9jGljULiw85/65Q+R0fyX9EpANzpLP/OvkeF968nRbJTzfYsX9nfaqmDTweaDgXod+N9Aby1EQ0IWqhEDJmCopHQSYZAaEsRMELO0XFNdNGJmqhZWM98vrOnP7Dh49de/JkkrhvB/XvroE+y5jvqdPuPG3eaw9epRYOGCRqPo1GsyiP44M0LvDc2RgOraVimNCJRrluQxxm6puym1+5tfLGUV/+x2T+6NpmMRWTxp8LbL5c7zFwsCUogi3S0DSFcQ543jlAGHSPwtsNsj8ERVZ435eoQCQSAtYEO7l5WStgf51/9F/ua5p5+ZcoQWpAOXqurRh8AdB4aXDwgUqkus7rKe3JMcDJSByiN8B1Od36u8uU6BQLCUYqSaQtQgaaMFLrv93S68CJd/z+30++XpHNc92Zz1kM4tbTKx1j23YDdc112406lakFnz9wTOOHN12DwJCxhb0HydF4WjJtEWbKgOT1QPMqSKYSYKYJUVEgii5PEK0BeVErTXONqcz6j96DMv7ff978xVsPFU/bBPZ3OTj2zJMj3714KLThZcWDh/vrNtShsO8QNIdjkGQVmXSSk7RlPSiHOrqTRbwXLBiwzDDycj1W05qVCaQzs09/Z8str2xCDSYLJsqYhUqOu+Wvjkba5nW6BUHcFWNlwp7nbxiz8OkxFwEFE3299yqwBUlKNjRBLykCyyRhaxKMdByaSCgsHTZpuyJtE7TPqP2hSh9/+d+OefH6H2ffLsTdyu3WF5CN5Nr9RPcucOpcpld/9tVeFXdMuRpG8nBvSb+AYcsik3ROIZamYV86+nniTwU/G0ikkZ8bQjRSb2SaNqUKRh887eyqx2beB7T8koAFBzSy3cY5znqEDXr+Ev91Yv6wiTYTQxA1BIt6ipEt9ZAKi/nEhBGL8KmDDKe4dMHOPASjMM+g+Rf4vJIRWfttAkg+sv/bySe+eAMN/h+e3i/2zZU3Ar4ByB2QB4/fKYPysM1ltc/KjPGGPDlUmbwE7EwSvoCXG2hk+cIokH7josdnPfjNRfsuX0g7lqpIDk2hE3u3hbhdHr8do6MuPAlNYcwbitfOOhOwpig99gh6fflCa3McBaV9xOZwCxTZ4XF1oiNnXIpeGcJycxZEEcmWBgNNPywF8uS+Z97+9w1/OfMDLEAIU4b8A37v6aIeVFTFLwlc3UsFSXOIJjFEWgB5YU0GDSUwIwNRoaaTw/2rKDb8XtNuXLHAgOJbuP95f7nri5L/+ZgD9Ke28S5v2zjsWKrbPniie5veRQK32QpjSs/xt1+45atbLpILDxuuePyKTZhQ00CaYE40uSIxR7xKUGCnJfh8XsBO2PENP8QB9tJ5bM31zxCapxzGNgba9pTaH1e3Q1xglnTK9DGlHz/+57NaV399rlo8YpCk5YomU2HQ3JRIxRQNNmEnrQwk1YOg6oeRiCGVbLXN8JYMMq3zR5z9t+lLzjqfJOd3ZRKiuwtKuaSL792+gRb/yHx1e405DULTzVpJ78J02pI1PSDBm4N0jPpXHseeWIrn7SoN8dEv+kubciUyUAr10rZtJOJoWr9ozIUzKs5/YuIXlwl7nwusnyQX7llqMlmBqivtBtpB55Mf+w6tiayIMNMJesC216OIyWTCZtUrNqD3sHumfvjhG1NrnmnGoedzGsYOBtphTdp2Zed6yQ4PKlzOcvDMsYci/N3foBcMKxowSqmvDgt5pX3FWDIBAaRPmiWK7qis5pAV2IkY/LlBK7ZyST3AouIe4+fMWvL0a1ccf8vYTW/fdxly+pUKvjyvTC0tkcYSFVi2wIEPxLOsUSuCQPEkz8uLUowzO6h8zMdGbtC2apdWRbT+Q1+YcMHdTy47f9/V1UfAxNLtGGgnfcCfMFjfeZ+1t1kY04ee+v74Ff+66ip48vaVQ3k5TJCpzOcATeiXyLj6n0HgLJqiNRyKVLC4nd7y9Wr03f8e5I57FS9MTWAP97Dt/I3tsumdspSdm4CAu2q9+y58eczXz11xNfQRx3gL+kqC7BfjBKGQVXiCfiQTUSAVgxLMBae8TyQgSQZU2bRjGxaE5T4jnz/0xffv+vB/0dCh8rnzb+/mO7pjoNQNH3/9yqFf3j7uFiB0kN5nUG4qnhH03BIxlSBwtgjRp0MxqbVHhS8KUmjciX6nPxOw3gQz4ijpWWzULluUgJr3be99T5y/6eP7D4NatLeYWyjaRE0nylQKpaC1Xdk6myOSF+FjZRKv/GmqbBNnrdHSlECi9ZN9r674e/ldh6+7kDCIZbAwixtop1nH7SQv3VovJg1/ItVr2UUDbwRSJ+QOPsjfUh9TtUAxTGaLskxdlM4G6ggI01ieYhlgsmRnmmozCHjTiETWjDnp9B++f2/WEJjJvRHM00VJF1mWeJuTkmXdksUV7I1kFKIqwaNryKQyEA0LtqwSMMFOtq4zEK7ZMHTiBX8/6rF7535cgtalQhWj1gqVJLtqUrr8Z9lE/OfKQYUHGSu4uu8ZV2Y2fnqup/e+hSSpkUxSH4pCIKfAyI3TxTITqZlAGGYjAStaZyBZ86/fXffAfd/e8YeVJD7HE3lnlG3bl2ukHaoJ3XqawIxy5dmTZvQ6p6TPGdD8l4veolAwt4cUz9DUge0wsaXiQDICLRBEOpyBGghSQQW5Obrd3LCBsdaGtXtc8OBFSwcf8z2uaWMw294FbK9RvL372ioi2FGYO5Yxb5U85mRYP0xRe+y/hyB5AunWpBjo2R/RCCFq6OgjgWkKaUl2gcqYRCBNX02VTQJzE6Nhi+0Pells9UoToYIEYglDLCjy26alcEoEKuHxV2fv6VRu02YaGvVT0glbV0WWiDQJaG5ZjYH7PH3eR89XPHPctASWTqVzwsHdtoNMu0KG7fKG3JMx38LcMWUIL7oEBfv1A/MFBDEgqZIkpjUHuNEuf5jViHH5kbw64s1NRI1IxK8kjUYJl4l0Qg6VlgqtMRKqVUTCofL5YUZ8R0QrQ8RZpKomIpmM8LaNTIRp5D1lDaqqIZ0MW+lNX0fEoqL3R5/3j3/0nH76hjkVMDCZ9wpcM996G/CH396u2KW9swMLELCAeYbd8+7vlr91250QpKEQdZ+WkyumDYLyZdtzztJzNXQ+8M6gkfxqa52NeGNYGTb2BvXCy9+M99ivBZP4Vbb1sLeOytsfaxZT3E3rdLZHWTMLNl03c99PKm66GAgeGuw73E9t/XgyDUElCTjbmdIgmg5LRcAfQpS0TyQLPo9gR9YtSsi9Rj325qY3H//9JGxC5U55Vrt9fVnvmf2BHYsIMwFnfNIHLx4+BSgsU0qHlxqtScXfs79oJAwnR5IdeBuxujsSDIorvOTQ/+TlB9C86gfkDBxqh+vrE2hsFFFQIoGJsqjrToOOe88OBpqltqf2lCJy2T8qRonMtJPxiMEalzcAnq8Pvuajh5aPGvJd3TkuZUw7IViHKarO9titGdWt15MxxXfWLcPiL9w0Beh7lFgwrFgQA7qoaaIhEpSoo4CwKzzVplNqc/Y9I5O2CdqmUgUz2gItGEA6EoYWCIlcTc5y2fFpDfmS0DlDY30GJ8+miS7DoBaqCJ/XT71iO9mwOYPYqu8OvvDiB3tMuX3uSx5EMIKG3zg+tmvQr3NnzqI4NaddPrA6bTZ+Ev3tU/T7x8lHXwYzelGooLfUGo1r3P1TkYvWh7PzOyp0RLhmCCR0SUwVScQ3LI1CsOf976cbbnrgip6rsaCa8iN6O4mLuZfaMeds9zFuiaLb+z97fst3Mxa6Whh9NpC8TC8a1kuQvWLKdIh5RY8MUbFgpg0OEpZFmuFL8pEtnyZwwd/4ltVrjip/9HrMOPbDD4QdTYTs0rU5N5vNQZ2r3fEDGsuUfO8VxzV9dv9VUIaP8PcYEIhFkiKIb4mTdrknJDfM7C9n0oQLAFMv0HTkzD05OXa4mYq61G23oQSDxJjnhraugWZlBN3H4tEVpBIREGeipghWtL7aRDLeCpb38Ez29XPnno96PMMthF5bl+Tb3EXHNWoz0F3xHgLWtYTQf+QfAXYF1D55CJQEdX9ASjFi2XM5dTgsMasM5545Rgahgny01tdA9uu8Ck3iYj6fBy211QCxVnCP4kpYkI4K9VSpsUUyfKkI/Dl+2HYGmUwKuuaBZTE7GUsxxJqSkDJPfBBZcs9Rh5KEw7c2UEYI5u1aXtt+zxoox/n+hEOrfUmFExjzxM957rhPnrvlerWo78iMIUq5pT3R0tzIBwu2MlAi0xM0yKKHi4olIpstNC7eiD5jHrp+w9wXbxeE8FgGVHVkG+xwpjhbdvcMlD6DmJ0EFE8fh/oX/g5fyb5QvD5PICgm03Gil4HqFZGOJwBTg6B54dXISBM8CtJlZtevXmpBDr48/rw37/myopgEiogW5Wd5dS8Hbf+qNxnrc6IgTAby/uDvt29prCmqSIF8QizCFt0pqm0M1G0PmEkUlBahcct6QKUwmFZYhkgYX8tyQtgONX8nVHS+mwzclonNPQ0znbS9umQlNq2NQvEuLz7kirtO+uCCTyvGIYGqStulLnWsYiuund3JP7dabgWHXfY7fDLzGiA0CqE9e0i+gGLJScKouUbmVqC5fCPdlIMy4oNqqTgUn8ajAQ6oV2V4/V7EYpTD0z3T4eYYt4MWovTLhJ2JIZQf4mGuxedNg3ZrJMLAuViwdMRhh963779ufvspYUSa9DIZH13Y/qnbtSz9bmyrJUwtuPCukY1LXp2CdPKMQPEANZo0xFBREVKJKJGuuVj0bKWbUE9UidYJmohMw4oEkk3/xsQpN+H8w1bi5gkpTk3TIRri444dXnx6Jttx6lj02sXbEHI/ZMGWI2lCP/d0+HN65Jb0FCPxmKOc5dNgkoFChTeUx3VPzEwKQZ+OcFMtigpzjbrlb68aOOHKZ9bM3+MVsDN5k38Xr2G7b99Zm6XjejzBWO5FwtiDge8ugb7nOE9hb59lqyReJGaYq+FLcgR8k1GI5sDQaJUph8rEIzzUkWTZQQfxAWQJoF6nTqzw9AC2MiSHsYDQV5YBRRIJzEHyhwmrYUMUovfdGcuW3zP5b1iLyjbi7/ZL3qpy8LMZKNGClkwWPFMA6TiE9h8kef1eS7EcXtes6HEW3J89ZDQV6WgrCnqWoLF6C7QQ8TwJQDrtsBPyGM0dCHDRQRz0wAtvTvU2GPCipakBsC07lBtirdXVBky7CWJw9pymTx49QRM2YgEzMK7SZihrC2+7LBDtflsl6894J3/WKlb45xPOv7Bu+YdThGDvHpo/Tyzq0RsbV62CVpyPdIaGRShqcA4s3o4zHXIzmHEbDVUbUVj6Ys7KhfeEq6cl8MBUhop2lW9nF20V3nbeKG0JUefNvvMQqYxJ++a+Pv7rilOuVov2P0b15EhpUxQNS4Sq+2CaJi06iRHxNEx0R4sIkG2ZGduItsQRXfv16XcsuP8Vodd8XItfXNG6a4s+T5+17OnBk4Z7zwX8k6TC4T0F0c+Z3pOZsFPbIW1O+tVB/NcpFDk8RtmkUM0aMJP5BiVInNNH7CA8S5uWF5wcQV4a2xPtFMxUOGGHF/7gGzD+n5+u+ej9cQ4woSMz+raXv1WSsluhHIA53oF/sEaveXnSJUD+YWqvkfkZYq9WvBxYwNu/xEfLSH7W5tM/JmcryfLBkWdzck3KwNI8Z6fFc8WPuDpZlkqX/KAEljb5Z4cCfsQb60k4yg43bDbQsm75sHNuv+mg8//0VcUl0yJjJ5SyqgpizXfxtZ2bgx22dXZqxRm4bn/jLnpXhc1hyilvrNnv9efOnwbF3LuopESPpVIwSdCcBgcUyr0JOeSBkXGIQbVADiLV9dBChXa6ZlUGmcVvnzPtjsc++tOVX1f34H3sLtpk2ZS56/G4XeiDbrs/RrzH8pacf/gpaN5wq6egV66oBKVkRoCs+JBJpiETvSAR9nIAgwMJdFTSyEjTlt1c2wRvyYuzWt57bJJA8gXjdkv096d44DLGpNhDyHv32kMmIPnD5ULesP1lJUckrZQ0oxDG9SDcqLLhHd2Jk285NsSrQVDJy3KOHaXdQOnf+Uiea6T82HRyMlXWwTJp2OkorOYtzZCll/Y65ogZj7y5ZuME4c3shP0O8bZt97yzfHuni8Mk9F9XgHW/vwBYeZbcY3x/UwgomugRafghC1Qg58CVzzgtDBmcE/hw5XKiwaF7J5IvfqC5BprV9qR6INcokPjhREJJRErmUTU+P+n3ilbj8m/rUTrgleG3zHtw2eaahj1mHWEuXeqScvPv2Xndp81IXczjLhonl9qaVl41dGrFDRdIIbvcgukL5QbFlJGCoqkwiFmbKpBEMWSanBaGZFGMeBy6P2SLgsgSG1bV5A4rmv7l4o9mvww0TnVoOhyuKFc/p8158mCjgyftkDdvz0B37kGdxyJcPBcDHz1+1CNIpMYqpQNCpDWiQOXcs8Tp6oxROUU1J/91NzczYKQiFqLhjYV7HfhQwymPPIVrudjqr2ukdNKeU+edcXXx0Ml7DToDuj7Fk99Lp76uIZJcBEVjFL60G6jG78NpsrfpqPDN6IbBnQ00K6HHN6dj8NRTFSQNVjoJI1xvI7FmWd/fTZr+4DcPvnlSOzl2O6XJVvtym8L8zgtiOzVQR9tG9t+3n/nplTdCHTLO03t4gKIim+joqHxBvK4ODpEbKKmjZQ1UdQ00nc3XeaiffW9W35MOKsdA+Rpw9kIZRrQVuQUhqzVcY9r1i74qPv+Bf+Q8dfa3K0ZUprF0EisvL0eFw9iwUwPdhvPKBZ525/azkKmpY+H5Z+3vz63dsuEy5BQP8vq91NaEnU6D6RpMiwQ7GAxJgsKI8I3gmSKMeMz2BkIssXFTBKo6v/zRJ24ce96oNZMdvc8Oz7ITRUEHA+18/bvlQflN92M5SJ56GurenIKcfYb580rUWCQheoM5ME0KBphDK0Ic1wQTI49KvV2iuCSNy9aGDFprv+x93LXTNkl//AFvFrrogG4u6c/xtgVMwVgInnF/PilZVXmjUjx0mOYLSIZgg5NLW1R9do2UywECGZHCvSytIuVULgEWD+2yIS6hTNw5U/K23PPQ/0idi5gnBIg0wkUKYpn61w44b8ZDqVOPXVh1gkCRFD+gd5xWuY9vt71ndhGZcPRSlLy/R+FVgDJZ7z9GN2xFJMUzTsFCnXWi6qQiD4lWcAN1qEud+yd6TDdf58bpCvByT5s1TtqcjgdNpyzk5ReguXojcgtDRsuqH2LI6zlj5BPzntImorFqxLQklk7digokywS5TU62e9Xa7McJR93FvN88fO4+4Q2fXgUtZ4KvpI9X1XQpEo/DMkxoMnFCMwiKDJZOQQ/4kYq2QvWo8Ho0K7xmVQJGpEbZ88SHJj7zSGXl3lxfll47NdCuhsp330DpqQx6pzdWn3wpUHpiaOCevWOJjGTFk5Bz85yg2xX+dXcnz0epzM5IUZoZdjra1AhDeP2iu+f884mLb1gD9uwvAgPcgS0LWMKU8Z/UDPjyzyMuhlR4llxYFFB8XtHg0DQRNjdQZ8CatwoIAZRNEckr8HQ+2y/tkIO6ZFwasZdQ3ipy3kDuSOyMAck2LbNhS6NYOnb6mbfOfP25c/vXl7P11gyX6vjXNVDuonT/PlNOjH37r+koHNwToleiqrSmevgQNU23kCIaAaQURnxUWWjQ1hGEM0DhFIMcj5vNVZ12Dc9BMyby8nKo0GbHatbGkdmw4MAbXrnrs5FHVOH1aRFUTu2QF/Cn18al/DMZaHb/Ow+QQT7iH5lBH00/oByxTSfS/ftyCqVMxhANg5BPHq4vRdVrTVNhxWMQNCoOJmmI3TbMRMbaMn89MPKD/L+8V9F0eOlGHC1kSPUcCxxCMzcf2Nqvux70lzJQeqo6ht9wDJbdfjYw8HB/32E+I50RTc1LYnZc9NeJt9snRSiPUcwUdL+GTLzZTtZXNyJ/yO3vVb9beQxJufNS9O4WPnbRvS5nARx24uGoXjAV/rwhan6hwvVTKBQjikheBHIMlG7IwalSscj1oDzMcxS4+L22FYmcFiYVjejnaHKDmOusGMEiU1EkWj4/YMqrd5zw6J4LrxOmJRewqWycA4p3mMS2eXU6V382D+qmLV9g4KP79/0bFN/x8Ofl0uiU6glAlBRkTEeyUFKIBsWd5+TLsXUE4TxrN8LIMpFQRZe8Jwnw0r9bFpiZQk7Qmwqv/mALUPz8P9iKmX+bNq8OpYdamLxturMzD/oTaFzpYdkoY+LQg1C84pHJJ2DLN9cA6CHkFMiSoktmNAGN7t8TQDKRAolCU0jLWQ5jYYRyfYi0Nhis7vsWQF89/vqnpn9pnDAX70wz8Gw1w9gZDmroP2egU/WpC/7eb+o44TAAU/Sehwz0BXO1ljjV+9zNyveZG/bwBjKDIlOHwkKiqZaIoRmi0a8PPffWu+bOHPlvYDRpoOy8ItBdG3Sqejv6PK5S5blzS8/ktftdCpiniT369hEkktlQSbkeVKVNZ9ssfLjbDVs5G4CjjJZtxajcQG23iuueUJxvyKmCksKVFY3YaI2sQe6Qp86e9frLnx2B+vUjptlYOtVJ1Lcx0LaQ1j2JuwHG6O76dHjfiYwF3ux72rHY+OMNKCwdAtPWBNUHVfPxVq9oWcSDBdW1n3SHCEJzQ3znAHMNOPvsXaADCTRRFCEy2zbDDRT9hRFf8zH2vvgRXHfjEiztn0D1eqsrIq3tGaiz/8nBct2fXdk3CjCLQBA6Bl9+ElbPPRMh/UgougmvVxFlWbSjCYiBPIiCCjJW6u2zZBISqZQnw/D6VDvRuDmB+KqNKBj22jWrq567a//KTZQ/tz3HNl/duZOSHaXuWppi90NcZ+PLuI/58MorA/DVHybDO6xMzysKZEQv6Ti5AFKH+7R9sNuGqsnIRFqQkxtAJhFBYtP6GJj1+g2zNtx/2ySs4qNFP6eRdmezEpXfHn88HMtevhSB4fsL/pBXFD0ibUcKaqmVwCtutAd4FdcxPocu0il+cCOlgQzXg1Lfj7RPnP6fibRpEKcJIcUzaK6eN/j8f908+qmxyytHIOlOajhQMCcecq+6k3G2m2h37mqb92R74NupcCq9r1nVZ9Ndh92p9h54cCaaDNIArObP5XOtGVL5dqc3nGJZNsR3vKNzSNHh5ErYt62LM3PryDsy+HTFjjTVZBD+dj16n/DE9RtfeP52gROCdywUbkXvsr0qbsf2SlcG2pk5kt9/GRMFoup8GtrIZei/eOYJV6Ju2Yno0T8Xmk7jKIAsA/E0RH8OCa4BKQPe3AIkohFoROvjk5GMN9lmS20YzJzXd9KZj21YWrUQC95s5QATgYAm7qudg2Xrp/crhLi0exRcjxCePOYY1L9/g1Q4tq8lBRUoHlGUFepDOzA4orCUZNjERk9bnuYBU3GuHC0x007XNW6EHZj5Nfvq5X2Fis2MlaeFnxHEsNPdTHT+d27pufraQZMBdUrxnod46zbVKoovVxRFDzIm4VksyH4vTNKuzA4kcJt1Kr2OdIPbIyTghu6BSeK8zDmUbDMFjyZa0bVfN6FgzxcG/Wvu9NUHcdU0NoMxTHZCIbKCTiCHDle/a15iq9vuiFneFq/MhROoqudF6RGXonbFJf6+Q3qkMwKMNBNFjw+q7kUqHnWxqG35S4c+sQOH5DzDMhVTiNXOAnQ/SFPWzDgsCbkhHS3rllowG/91+I1zHmu4ee/vFk4bYWDqUtKaYTgCdlllGb/2ykpno3dloB0NsksP6iJzukCMUD6ivrwMPf9w0HHno2HNuejVrwCqVyLl+HS4HnJeHkSRMMZJB1NMBAUkGSnTiGAcHhV2orXGQnjxSpTsNfP6mq+eJUgf18qZsY0+aZfPk1Zwe15/9z1o+6PnJxK+fzcPq8+/AYJ0utprWKHiyUU8HBahKFAICJ1McqYz6ouSR7GpPJ2Mc7p/CvvMlmYDsm8VorUPPLl81TsXDhMaOd3cbmzInRpl5zcQZ49v/6ORWHwVxMKRyCsKSYpPsJkiqqIKQ1aduVeeU7pelJMuizCy3oL+xZVUkImvhmBvkgSPR7eTsSbAilkIL/shf/8LH2ma9sCbOHJcAuUzgArOUkcvZ9fjmHsAACAASURBVDB7R3Din7gmju4I0VxXbl/agDFlrwufmvDjU1NvkYuK95XkANIpWyzs1R/N4QgvxRNqrL344+ahHM7ncAqlSeaA2jIZG6Lmh0RA8lSag+VtM2VLYtqw6taGoRRMH377e7OXXZXTAIwwUbWUjR0LVFV2z0A7P74uNvs2bUMqkgsQlOVxVjBswMlliK25Wcop8uSX9hXqaxtE6BqQcrMsQeE0NZKk8UOY8dyZWCvD8Of40rH1KxugeV7d68ZHZ/z4xwkb0d8lkONhxjabq8u/+TUM1HXbTM0ff+ZBTV9VXoecPSfklfaXmpuaqIEGTzCEJEEAJZnrThILA3HAEqW/IEvwairiDQ0WIk1NwPJPA6MvfXn/KQ99/f5koWl7pNe7bHzd/IHD/pHs+8nf9j4LWHYWQnsTzMsLQxAzige+QAixxkbQ8dmGYe9goPwhQoBCokScZ0iGHY0R05vt92mI1m4wEK9PAPUv3PhF7IlbFmENSLadzbKJLZkxkOG4dCg7uOCfaKCOJyojI92h9sgMxkonC0OmQExP0Uv7hVJJJvjzS8REyoAiUPGvQ3WWO9LsULcDG6ZWFGePT5rw5RcTUSHS4Rb4cmluuNmy69dHwNZ/esi1lfdFrz1hUVWeYBDTwKwZsEkB3vGdZW3ec3sedAcGuoN+PlO+iyJn78CoMcDiiUDfMpQOzoHqg6R5RCudgiBbYATllDXImpeA/FAtm+cetpGGrop2sqkmgdiWucNOu/KJkyqv/2y6Qx7n1g+6enbbpse/lgdtu5pjZ60qfHfSXmVA0ZXI7dlD9XgUwtPYogRZ053ZUdK8pLCX4vIU8YfKCAQDhECy05GIgVi4Aca6eWW3vzWz8vr9vybZNEcfYZcKAN00xy4XUvcc8Njo5OfX/Q2+vgd680q8yYwgypoP3kAIrbW1gJeOl/YhE80mD+pICNKLRGYp96QUncJij6bbRiZpmLUb4kDDGvgGP/Zg7NN3LiMhJKCNrW+nBrobhrlLC8KY2vvkuw7Z9M4/b4elFCtFvQuMREbR8ouR5gLIzqCyIwfjjltxMWTHQImukyqeZmsCem4BjIwBKxFFXnHIbq5Zl0Lrt8uBnq88z9a+fBbWN0LoT2Ghxcr5AWWXldE37JaB0mV0NNJsyi0Gj0ZoWOD9Ed+8eswpgH5EwfDj+zTWN3mQTEvefv2RSiUddnjCS3NeZIFU5nhEoHs0m9kZ5tFEM7zy22rkjXjowsrPZj95uKueQB2I7b66qF/t4Hn+nCFu+yUxJgSOmzY4+s5LU4DkH8S8XgWSqsGwbTGQV4Boa4STQfP2CxWODGIxYCDqQipfM9OyiQ7NalyXQCb91vg7lt7z5bW+FW6ZuisNi13ad9198wHLWeDzYaMmA01T9NJhfdIWtQQDUD1ecIFW0vfcgYEKCgNRWWqyCktUbEUUWbKlAYjW1QKJtw6c+sYTn5285wrs1YFvyM2zuD7k9mBtv5aBAsKtjJX8tejAy9Cw9lyxaKiPSboXkibS8zSJ8S/LJ+xeKxXL6JCitgux9useauQnQe8nmhNiGPB4YSU3/tgItH5ecNG1d5zy+HWrKyqFJCmEc6kcAsV3LK50eGA7g/ptJ7zlzreNFGosU446YPnoDx76w1Vgm/aAb0APKZDjYaIsqB6PmIolEMzPQ6S+GoKuQdPoXjN86EPXKdRltpGOIt3c2ADDfnvk356/d/Hf9lvH5+04A3oW0tcND7qTZ/nLGChd19g5XqW1dk9j9TXXQO51uJqX67MEGZrPJxLHrBDMASMPSoPdxAhIJGOWMxxMRSQBGdtKNmT4XKCYM3P0/85+7odbey51JNp+JS9KuWivaydgy8OXIjjqKEi6V/KEeA5t0bVSDtrmQQDNdjZnmwfVRaSJ24fEDkXFptzFqNlgAM0/QBn99G2Zf82+wfWebUDq35aBAnOYF3+9aSwW3nU3QnuNKuk7TK5ds0HQe/YUU0kquLrqcG0GSmT8TrHMTltQfDkchZRJZyBLAoxM1Ea0NoXUuvXQgjMvTK1+6slsWDgDjLznJIFnyF3SgeyGgWZTMCV0+PsjW/899UyY3x8CYVB/T68+/mRLi+QpLEJubh6qly9HsFcvRMIt0D060pkUx497fTonjEu0NpCKfBKZ2k8Ouu7Zh9Tb/1jFBYSdQ4BQYFliyC4stIMH7cZB+8sZKIATFjDvnHEXlEFZNBkeaYTu93kzJINJaBxFc0NcYm32OJVdkpoj4VzS7bTTkKWUrYgGi65cUAvf4BfL//XNMxUPYyPmcC6SXel1dddpdnofk3A8euKtCWdAif8Jmq+3FMyXaJqRv4hq3sUW81YLD3Hb5QPJg9JFkuanZdiwM6aFZso9W2Z7j579YOC94SvquF4jfzlV2zawyVaUzFtfVzce7E+84S5+rExC+axcVIy6maRg1N6DvJlYShGCQZERkx03UDfEddnhHQOVYRsCNEmD5PHztoTu1ZBOtVisaUMMVuq9vNOPe6b55Xs/z9KwzmDA5A7DzF36nx2A5btsrzgMjzzixhIm7TezaeBX/5x0BiLflyklY/oYibSshkjpm6JXAYoiI9YahqZ7YZoWvF4PotEwx1zn5PoQbq6zEWlMwMgswsDhL/x51bxXHxIqqaXCjTN7zdtnQ/gthLgdVvaNBtbjpMKBF0FULpTz84pNJio5RaUIt0YhUuObuGYVYmGgiQbngXPCaCMFVbZ5HmDT5Etd3RYoweeOuP3tlz5qLV2Dqb+0J3XbDVOZrr99/4TUt3dfB6Xwd0pRT0JOi5w8jJSr3D6o0wslSkpqwDvQHtIzUT06FydIRyME6rXQvGUNUPP05Sxccb8gJMYyxqooNMpWbbObidO4b+f16xmocwIxJuUfeMeJTfMfvg0IDcwZPhrhlrDEe4T8erNADOdQIV5bLlkv+5CJp6H7/Ui1NsEb8iKVajHsupWbESx96KLaL157wis0YwbnGsoWx+y2ieWOt589vDoYqLPV+VQZF9Dmf9oWVeW0AGcxXCKh9JFTJ5wDVF+AUEkxMVH68vKVeDxGhL+QqHJr0/4jGlQGI5GEPyeEeDzCZRdDQY/dUL3WQDS8BgWFlc/WLaw8RxA2orw8gwqXhLrDIcsvr+2620x324f6q+egWy0sE/Z/JNXni0uHXwkYZTmD9ixMGxKSSUMUdB+YYULxe2kSgEpj8OXlIJWOw4rFEcwpQKS+DnpAh2HGLKuxpgbFvR/8y8ZPX7lTEJodKa4dSJTvthtxxNRfZCz/DGFYGZC5DIHcQb6S3mI8aULWPDzPUmj8iOONLRddQLUswmgKUAN+CMkwLJayzU0rE8DG9298duU9vc4evKhicoVxbctke1Il7xA7MUHWKbtV3K7ChF1Eyuz2KtAHvM1Y3+OE4puhF5YVDhyGSDSp0Xw60cJSLZtenEOY6tf8omUEAoVopZZMJoFgnp/I4+zEplWtEOzZ58z69MElp/ZaXYXJBiZVkLoXr9qSYptjdrv1ajcTRjnHiRqOfTMvtPkfx7YuevVywBpcsMdeaGyokaiOIFItgbynSUP09M0yB/MLqoZUIgZZshHwq2ipXWfDTjbA45l95etf3H9vOLYZc6YaaOHXv/0DNWuobRbb6d7+owbqXIuKsf87BlUzrwZCx/Xa+0C5sSVOwywiDTZ7/D4uXs0Z7mRqu8Q5jadgq1BNC1qIuGoSdrxhA0Mq+j1GjnjhsLffeOOTHp4GTmH4s2JROyyeq7KMo+DFj+eMRN1bN0IvPlQp7KnboteWmSRylS4yTtdnEpSPpmCYIEPSc2Bm0hBYDCzdkkbt0mrAfOK16saXFrVMryl9YCorpwn7zpuyg6FuQ4nx63nP7ELwo2L06yxn+bVnn5Za/81f5ZzCIomJuqUS9E9y9ayzJ0zWmxKgPIhUJAp/rhfMjNEvK7Fp2Spt6O8envL6W7PvH44mVI1jGOeIQbkGSv+XU2puWyTbBk2VPb/agHTuRbuenxdfgzMZC50/eMpx9urX/gTfHnt484ukjJURTarSKlScdHBifO6VE5yR8rkMWDTrKUGRbBjpqG2lmg2r5ce5Z9w6+8EXN/i+AioTQAXRsHcy0M6X1bGd7f5bN5/jbh5U3T7lKAjxoGDi0Wj64hr4eo4u6DdEibZExTTxtCmkVWI5km0U2RHTXU4ejKRJKoeccdLjkZBIhq3MhqUJaOoaFJU+f3HVt3MeLRLqAGJj2FFHv9vXub03CmObWbAqr8+lQOZ85PTtJQYKJALRc1rNNjfnzONKgjOZK3uCnBJFkNI2C1fHEVn4+cjTr3zsmZfv+YoYExaUg439rzBQfoPazZ9i8E0H978eYEcJRX1CkkJQTjJQidS7nLEz10sQwwAJuNHh5Q96bCMdFtONW1JIJucML3/gzt4zTljxATndbHgPiLPKgDLXE3Uh9NX+bJxd66x6O0oo+3fZPS3ivH7Ks0+vKz0ntO9xiHw7ERg6Ajm9cqHoki/oF+MJUg+g8RRqh7lwRU5QrnKGBKaIUCQgHm6Ax6ukkpuWrMjf78THJ3/5+OzbgCZUTGb4iIe2VNXibaFKfv3/fQbKo5ZDVrD8eeOOmojooptR1L9A8+QIMG1nYt+0IAWDXKotFYlA9wd4+KsqMqKRFl7sLSwIoaFhi2VVb0hAZLXikFHP2MOPm41XLyUR3V920JsxST7o9gnmZ3ddgfzBx0HyMVEnXSSal+S3xx25k4ORB3WIJTn7aLLJQvXyTUCk8oQZHz6zwePbtPCcvYz/AgNtD7gZQ1kEuZUFB50OY8UVas8R/ZmgcKy1LUhcKZzzELkvDkBxnp9tZGKiLBq2UbN4Y+HvTnmg4YgZL+EstGIJLExq11eZVQaxjJNzZ7PLjtGMs8Ztr3YL7ug96f/z3tcejClLx51Tgsi6iVg1/yig39hQ/9G5yTSTvP6gGI60ggBCqmAhTeUMzulCn+6MDNLTLMgLoKF2E2GmbUQjGwKjJzx2+gtP/uuJodiCcZMZBlTY3O1zJW4eAoizyhgmZf/c8XrbDvHfUBV3W3fEpB53oGf1dcPPAzadJfca088WVEHzBMRkwoBGql+aH+lYgov1UNhLlT8a/DYTMWiqCNmrwjLSdiYdZXbtqjWBvY99ep//eX72x+VP1QAXxlw6iV+kwnsVYwX3SPufCU/8OghaUMotUSCogijK3Eh5/YJzDlF5yOZgeT3ksVtXL8ogveobdZ8/PLrv3U/P++ygEa0zFnhY7rgq20HLdMq7fjsh7lYGip7woOejI/DtHddACx4n5ZeqxLBOrAicj6iNnNu5JT7qbRssHW8RYKUzaF3zyp9eb3rw8ZOxCjNA+iSE4mjL3dgsiChrN9CO+8cJebdroNm6EkfpE42NffuiAa/de3YZGpecAbFngafXYF8yKSi6HhRTqQxkr4eIU/lUI+MoeJ5QgyZtqEWUsU2wdMSGYBhoXFWDkjGv/bXmk0duFaoawMY6o5B0mND1Zs+OWWViGWahsjsG+hsLcWnjujLSTBl553cDF187diIglkEfNFjr0V+n7WxE0oDq50U3UZE5Vw+tl65rMEwTZiwKTSdMr4cqu3aCeoqp+BoUDXztqCv+OeeDIwcuQxUILvbLzJIyJg245KPxax89bjLQ+3Dk9w2Iul8XSMmYK0Jn+V0YYXVtUSVwPKzU2q9bgPjsi6ui9zxajWqc4Hh7KoiUdej3tW2/346BZm0k2/wBpiKEiokXoXr+JVLPvXoKki4RAQgnCssaKak6w4QqGSwTrhMEWcywpvqN6D3+rts2zpxzg0CDARUMrLx9iNlZEMdA+YbvfMY62mfbHvpuWc2pF4gnMGipS74d8uETF56CzJazkdcrICg+P7NkKSevhxiNZ2CFI8jp3w+RaBgKTRcRMwQ/K5wKtG0yG2aaIbLegC1tgRF+9+J3G59/dCGW41qBInmqbGfZ7V2eITfC3ipp7PCHzh70N2egzsq62QMjht9+AE4Hgmcgb1BfOVCsmDFDhOQD1ABfLFGk6f00+Gyl5oEkykgmk5xoi4afRY2AABHLrtu4CZ6C1w+69f2Z/67L3YjpQhqMEVX5z+5Jj2Ws8N2cw89F6ycHQNprP+QX58uKRxJorI4KC5yBATaz00yUU5KZiaTQuOJH9Bv/3BXr3njlPmFEDFjKn+Z/kYG220U5U4abbxy87KnLLkVR7yNF1a/Sg1BARGlZWlKKMjNQWRSZllobhlEHyfvR4X/9+t6PN2A1SMIB4wC2gCa6t4pc27oonR6dw0HR5cvZU+VMHjoD+oq9HhmMFc+cjXT9RASCvfJ69UNzYxj+UIFo2jJS8RSKe/VBXXU1V0KgyrsKgxf2eDGB2bwbZqfjFmKN65EKf/yHp7567uV3By7BLCpIun0lVmY7Awcdq7ddEdm2n22uBezSnvy1ikRdRLuMqMgHA7gO6sCjvb2HhBIJAaLsEwXBAytFg7E2l0VIRqgFA/jzS5BOpWFEIvDkBDmvEbOSiJP+RWtTDfJy3h99wU1PS3cct6TKId5yTruf80XoookfDcMbp90B6P2QO7S/rAUUmcbqBOL05pJdAMvAtMIwWmvDSNS/esKDH8/c8Ofhi7iMoDM1xfGmHS/uN+BB2/dDh0bF1s0fJu7/Gnp9ccERp8NuuEYOFeYwUee5KOOzv1kjTUO1Wmwz1ZKwm5YvQ+lJj9xd/cK7VwsVrWDlvKiCMje87ZBFtn0t529uf3VpoFkshzBWxvXzckqWV+5R+/b95chUH4XcAaGcwhIh3NAoSv4gZIpuaGhB8fD9Q/UOgh4yLttgEmUJaZ1yg82kkzZrrA3DXPPOnlPffGDT309Y08JJxKk8P4tXhCBUchErSlHaUU/dMFAO7Oi+4/gPGugsCeGygHbGlfuk333wAkgDjs0burcnmRKFZMIi5wh4iHDMdObxMgyMVNQklWN2WTIOPeRHKtYCWbFgGTGL1W+gOa7P1IOPr3xo3oz5D1SiaekkV9vk5zFSzrqAIxHE0tNORfW8S/Veew9Iba715Q3eQ8wYAMmU+HwBtIbr4fWaRmzjxxuVoac8cORtL7/zzimocwm6HS61rVY/Sye+9YX+h9ss2Ry0/aJ4KHl7wdCynPErZt9/IyRlj5z+Q9V40hSNhIncHn0Rbo1BYinYsc2G3fhjHaC/OvGZpkdn34AaVPPEj/A9HDm13ZiVBI3b+vxdbGhnuWRKnQ4EQp+NPOdwLPlkIjy5h8uB3JCsegVGBUhBhsCZCJ1fKhGGZ2sFFKVaBvwhP7y6jPq6LTbirQyyUovw4rkjz71l1uLKud/hkfdacB4xpfHUhF90mcuYTH/cykC3SZw7edD/HgPlbkbD76cVeFvDeyW+eP5PgHBAcNAhIcMQJcMyIcgmSH6Txn2IE4faGuSliLSJyIIJbeT1EXyMNFFI5jBjZxq2kBpzFYK9Xnxj0ZfvnHQMGrE0y6jW/ZNrh/bMmDDgjGcPWPtS+VVA6X45A4bnJxKm5PEGYZmiHdtSI/YaMcTYvPjLKOzN87H3/9w78eM7F82uXprAiBG0PThec9ui5LZZ1n/KQF12/iwSpuPVMpQz7369N/T9avrEKxGrP14qGZifW9BDiDZHxYykO/moEbORrI6j9Yevi/a55J/13ofnYh4SYxkIOUXL66xBp1fbmeWgNrroV7T9gFzMmCrd8EHv6mfunoCaFccD8gEo7BHyB3IFI5ER01SmJVhpduqGpDi4rg54WyUnlIN0gtQZ6qEFdOiaZLSu+bEFML7tO/HPMzf0GvslRvUMo3wsRT1OktohpOUq551D3P8zBurciDSWMa3qfoRwxf5jgeoLIfgPU0r7eD0+mhqRxGhzC9JUXfMEaPPzmUIe9ZP2Ce+bmlBkkmNPwky0cKIqQUKaRWprAqUH3xEquXLu5lF7V3N+G2qo7kJ4sSMjHb+E5X1/6umnGg1L7wqFinzNTRGxoKQ3FMWLmnUbESrKTbeum7cRwaEvXPDqN0/KRyJcMUIwsZT7D+rAdCwbuAi1HRvofwBB5Fxl9r/OglCBRJgKhGYe9Zcj1334+NWAf0zekL0EUl7MUO/alwsj2mAjsqEZVt3TC1jy8XECNmEsWPkCoIJj8/gaOGnZtvWgjobZVVORwksJB0zbA0vfPgUseTJEZSCYokD2MlH1CjQXRUJBvDfLycscgkAieCPqV/pqgpnSpAqzDDsTawHSsThSkXkI9X3x/vCXn15OEAS6X2oF8XQzO0TfaYgt61a3OW22Slp+Ugz3nwtx3WfjtkVkfM98waseGx/55C/lEHsd5e3dV83YXHFZFEjQVdFJlo7nOIqmO1MvhoF0OgnZQ8JF1IppRU5Bnq3KAuqX/ECTwmu1ojFv7Xn4FXPyDjly5fsfoQmVP1PIW8akCX1fGzf/wctvhqLtFyzs4Y8RSZoBFJT0shs3rTUQX/p+z7GXPHjfgnu+m+RwvNJma+dHbXtkHYLdTmocWQ/6HzBOvrXb9l7n7LSMKRN6f9xv/r1nXA5BKcvtNziUMSUhHk2KnmC+nUqE46xx/uL84cfc/frSdz4/qAJEA8K9EAfOdmivbBPnbtXj7GSrDHLB/SjAJ48PaZxz27lQtIOh6b39Rb0EVfcjEkuKZooGnmRoipd7TLC0Kzth8OKiwXmZRRixBDwer01hbrJ6Yxysel7u2LJZ0xbM/JDP6JbPYOCSE/zFh1ypdpB9bG3KEvwSuwoH/vsNtMOpwgTcjXzcecYE1L81GQjugWBxMbwhKRDKEU2LIRklsRoZsjtpAMOENxREOpnk4AC/34vWliaweAwFPUrsxk3rGBLEzoAPgN7P4MD5i/FvhN3piZ90onX8oYvWsuIn9hlxNhq3/E+w75A+acOWLCbZgUC+0bJyUSMQeviG5xY/V90LLc8cygsi2cmVTt+9cwNt3xQ/U5jevbvfFg7QDv8WpzKm3VIy7gyrbtXF8OSNLO41SLCYjJbWOLMaVtQAycq/vvbjQ7eeOqCBV9WdMFFkrAyCUElb2oF4bM+DtqOEuHFQtKXegR5f3nbMIYhuPhqSdYhW3CNH9wXFRMqyDdMSNY+Pjy+mm1qh+XJceXoSLCAZDwqgLM7CQhL1fo/fjrVGjPSWJWFA+rHwd6c+fdt7D3/xp1HjmlFd5Q67ti2Uq7nYvnBZlW/HNv/vGmjHQoSAy1kI9++/L7CxHHroUPhzAqrPLxInaypFpArUf5EBGk3TvWCq5mjs2lQmF2AaBp+K0RQJBXlBe/OiBRmgpRVQFwGZN3Dqk5/g1eM3QeAyE7ujqEauQBn25+lDl8989DKY5vHQPbmC4rVZfQ0pGH8w5OBL/vniSzdWjevRhnLiwfm2tvGbNdD2/rVz0VsVVssZk9efd/2eH7zwzBmwrLMgewNQvCKSBLzdMr/3vmWP3vrVM5+fQyN1VC5tD+Qdb9TRQNsrQu3fkzVQxsRywF8x7vJhWPLpiTAzxyLgH6nqQUHRfKKkeJBIZ2DG4tQ058JXBpGUWTRtQ7zFNJ2YQkZIcWVvio9p4igdjpLadBTJ5McoPfzlWdUV/55UiVZMInotWJUdJOpdr9/RSNsq8E7j5f+ugW4TuRcz5q0rOPpINK26CIGCCTDTPiE3VyQmA1oVWVa5KhbN66VaY9C8AQ4SyERjyC0shtfjxZblS6H5VPjzgrYgWqxxw0oDqS218BR9pY4e/+pBd95f9dGBebUdQs6f0o7hRjr26un7VN1z3aEAhrlSaPUYcP57B1701Ndn5SM8ebJA0040iJxF1f+3eFAXvrPdUE2ZU82U+c++M3D6TX8+GZm1A5z7Vzei58Xzp86/7/Op/YU4b+q7Ia07zLy1gW7fm4tYB8X7yV/y2BPf75tcsm4iUtaRkH0FgYLekqT4EG6is1eHPzcPqUwGJg2RqwpX2MvEaL6YISMR2XQSAv1OqS+zwAxmmZtr6Yc/CIy77JWrvr10/tQRiBHtaRmDXenccjvKqR1f69JotNfN/n8zUFoY6ZAVLHfe0AMPBJr+CCT3U3v3KVF1rxSLJgBLgOjxc74bUZRhpAnzqUHXNEQamziut6h3LzQ21vOQhs+W8lJexsrUrk/AzixG/yHv/+neJ+e81BLbEjt/TBxVFQbG8XxjVw2VGynOeCAXSdGPRkP6/YNXplvHoPnzyUihwpn1bJuy7+LzHYa5tiC27R3/qQpuJ3vZGrfexeTQDMaUyYXTCmEWKL+/8BK88/C78uXvHduUOARxKghlB5mzW9r9nfgonKmVrl/yHxkLrbzl7oFVT71wMNavOQZSYG+loKfP7y8UaYyYJFhJN0dUPRCI74rZsFQZAhcyIkpllXtQQcpQhR+ikCA9IK6sZ27atAlIvy+NunzOta9M++G2PUCpkMX1FLOdkSwzglMZ41eZVcDepn/dVZibBTV0L53o8l3/6SLRji5duIqx/HuEUXsDDX+A6j0GnmChJ6cUEHUkm1tFaD7kFZcgHo8jHQ7DE/KTEgySiRh03YuMQcvqHHhE088xAsywbStpWY11jTBrF8A3svKt2JffLQaqr3OoN36Kkbr3wUPVLFERz65cesduGf22be6tf2wHaJrd2AJtEyFdX2NnYocdjva1heruGjhxq2OQHapM9BeVEIVJW3sp9yZkXA312rtY/r3jJh9lrFpyBKJ145GXU+z350qGZVFtkHMdJWIJKLqPFxA57TKFV6TAZppIGw4RgD83iFhLPcCSCAY1O7JxlYFkXQ0Qe/34qV+++Nac1EYEDomVzYXhhLTtdbHtckJ1sdrbNaTd7Br8pg2USrY9GFOqh5eNxvJ3L4DeY6IQKPR4vPkSRF0yLIhGOAI5FEIg4EdruImjQjy6iniEGBv8roEyCCLtNDJUR36dWWnLqF4fAZqXA/L7GHjKOyOuf3zdkouyEohE6vrLFWS6p26+rc38IkbqRwuTdgAAHl9JREFU5Ho/g4Fu1wlwg806GdJ0poNsVhloWJ2Y/ERiVCCRoasWsNCTZ1wwIvbNqlPMNVsmwlNYDEWWvDk5oqrpyGQMpIhRkOaIvT4kEikwcqUEROD8VtT3lGDIvBIEu7EWQm4QeSGv1bRppYnWDRsA4aW8Iy75oDn3gmVYMiA5awkswkRnuVHakuBuaJHu7GTc3er7b9lAnYjCScA9Jz6xeNCbU04rg1l9AqRe/YX8nnIop1ANNzSTpDe8Xj+sRILnHx5dR7i5EaKHGN6z+45K+/SBVCSgKIaKCIadScYyICFhRBd5ek+Y+8gLb33S/0BsPFQYlwCqsrqO3fKAO3tY2/57Vwrn2UfSOcB1fvoXMdAdX7iDnuJf/pMOrE5RRbZd4ZIFjWUiqo5Wxzz5fujgnomSx/9yxmHxhZ/+HqJ/T6mwfzAQyJcImBKJxmFy0nMVVKklMnCi9UylCK9N4B+F5xFGmgpCgiO0m4rDE/Ai2dJgoWlzDEh8C7HfezjknE/v/fiS9VcuRaxsD1gcCcSXvWMvaTswp118yP8/GKizOxhTR9y3tM+SWy86DU1f7gf0GAexMJA7aJgnlbKFZDwlev0hrvtBClTegIaE0QImZoumzuLTfynwJIdhppPIzQlClQSroabasFsIKqgslAoOfePp95+fn7s3Gu4BYvMcXO8vaKRt4iZbPf6uvvDXNlBHpvQnHwwd4kX+IQTUdV/lmMFm0Ki+ml+NAdMu+cehVbP/eTQgDfP1G5hv26JqmEz06DQ4ISGZynDiCpKOoN8zyZTDZKHIUHXVkf3JJJEmiQ0atKYJqGQCmqZY6U2rG4G13wAHvHXx3PnzVh+Cmg+cfIcReTfXUOm08tvrnOyKfe6ucW59ZOzKN/9K722Dm7Ubh4Kp3xXilekHYdkrBwGYgPyD+iiK38uYJgQDuWI0moIRTyNUkoOYUQeb25bgTMQ4TMuc2ItMIi83B42bNwOZDPRg0LYNGo9qMSGqGyEoVTlDenycKM1fMOzdR9cspEZax+b6L2Kw26JzOy/1rsq57+6j6q6Bus9qex3NduOcUQ6UzxDK5yE4967KXh573V4Lv377ZCRaDlXzizy6Jyik0oaoe72I1Nc4NiSpULwB6JoPRDFE85xmMg1BVaAQ5YFgwjBT/HdFFSFKpJ9j2lY6ZZm11I+OzUbRpXPQ+7JlqBpA0pYkrtaWb249kcK9gesTdm/1/s8bqLs87TGf40lpVC1UWv5an5qKayYB1gmAZyBye0uKEoCq+ERF8SCaCEPwxGGLpMbpGCfhvLmRkn4I6ZZaNrG9c7JsK2NAFgRIgmTHIq2W2bQiCiQi0JWv8kaPnzfwmNO//faksvUYjbiDJ/wZPWqW+6jTftge8eZOjXRHOWWXe85lMOzi37proJ0Age09COczec55HmPSqYD/3v+d3Te88vMDvv/onSMgiePgCQYKintrkXhKyiQNLhZMXLR5JTmIx6PIZGi5FXCWT5Ps1QO/PwDTpImhNGyWhiTbkBWaQUgjHm2x0dKUQTqyFDDnoPeUuWPuuW359x8h7oy6ZXmgOhau2s8Wp/jqIJ525/X/g4FubZzZ1aINPRlyz9gzI7a8eMsxgHA81JxhUH0hX6gEPm9IbG2ugx2wYYv0VGmui7yoyHG9jtisCFlSILms9pZAojg2ZJNmA0nnyLTBwoZRuz4OGC3ILf6hdJ+DPzzuypu+E48qra647qMEph9JXEi7A3bobJJb1QS6MtCdGufu7Kif/rPbq2WImMHEs8uh1j61ImRUvTdg0w9Ve69ZuvhAhBvGAFpxqP9QLWWIQjqeEb15RfAFctESjsKMEAyW8B6AqHmgSDpMA7DSFgRJ5Rhaw0jBSpB2igXNR1rlUVixJoZ4pBbWloUA3rn2afbZgPOwcTInlmZuj5Nfbvuon2uIbTfRYdftqon+HEbZ8TH81otEO9kyM5Qj3ygv/PDcYw5HuKoM0EdA71UCxavofr/AVEu0uAIZyRBQ4NLBi0KEpCgcfcT1BkyLzwdyIXdZhiRaSKYaYaaiHKvJGe2NTA10faVc0Pvfvv6jv77oyVvW3LMWzXiW9zt/SnvG6UN0eO2suvtrG2jH69nBd3feR1znfs/3mRL4AaHkgoeHrfnunfGtm1aOh5EZhUBOqaz5BX8wX0xGU0gLGhTN56B/uEyhF6pHgZEJQ6Zw1RZgccUiYtjTeE5Kg9U+n263tjZAFAymepgZb9ycQXQ9hbRvA30/OPO19T++cOrCFmCvjEM6xMNaFyThMCJsfSJua467YqA/t3H+5nPQbh3ojAl7At6Fe125NxY+NxGQJsJTVFrQu7+SNiFG40komoeX5GOJBOxkCprXD8nvR4aQJ65uJ1UB6RkSTSbZjKbRBknBzqQgKRJ8Xh3JeMw2mhszMM0WmMY6/9CR3/Xc48D5f338msXz8rH5KS5LkQ2hOjnDbt3Mb/JNbTfigi06h68djxd5KoP0w/3Qe1lNJS8/eevoaOPyfdPNG8dDYsMhiprk9UuEnZUUnVdmBUWHrJKygEoDl5AsG2mSAeGD1Em+9iIkrixGxqkpHliGRdo4hBBDXp5PjEQabKNuRQqo+RHQX8NRF/+7V+Xd6za/jxg+AnOVuree7+uMW2y7iw4muQuF3F/COP9vGKiTzgtjGTw1V7/Vp/qFe/ZD7ffHAMmDxcL98qlfSrISFC7KqopAKAemzRCuq4fm1ZG23GEFEVyvNE3eknytKPNBcTOdgSAK0DXKgQyk4zHqqdo+r56JbVibgKzWQJB+1HsPXTrmoJOWHX7+WStyDkLD1VVIYCwnuvsVFdl+ZgNvYyzo6nPbcEEinoby4DEIpL9B6b9ff2Hk27OfGG6HNw4D9JEQ1QIAPsHn13Sfj0tPkv3RMCgN3pNDIxxVO9WJo8QuSQK8uoRoYz2QMSEHQpAEGemWiA1ZEnOIUUNidhPxUsVrCHgwFxj2AcpvrRow47DGtQCJMVmobCcl4ztlO0vUZSjpKDnt8PVLGWb2S3/jIa5T1XSRKDtbKwpVpSErkbvygGGj0Bg/Gmg5AcrQPoEePXR/MISWSKuYao2C5LV8oRDiMZKTk6CR1iWxw1M7TCRuM0bEUZAkOq2dAV9bppOcuIwzkIwMSTvBoyswjZQtpJOZdCwSgdlcB8grUTz+y7JLbv3uwAMGbfbnIpI/BqnngXTlryn8tDu2yqtpHbiCOByLqD44B6GI9ZCwBcqFo6AcFIT/w9ujfd99/roDmpa+8zsgPgLwF4j+Uo+i+FRRUkVFJX1NjSfryVQaGcr7aaDT6+E4IyJL4Vqj9Axo/WWJbBBiKgGmyNBUlVfdjTShwCwoxAAlCaxl7aIIEF4GyHPhP2ruAY+/sOLz/dCM/lUWwIWQt2F775aBtp09OzdQ7uV+Wn+4W0/oN26gbVUh9zq72ShnTMUJU0vw1vuHAs1lgHEovEElVNJTIA2YtGGKCeqjiRIUXeeNbY4fo6NAJV4hhgwHeirQaCrfnROTJcmt+GZgpFPwemlGNQlRZFymJJmI2XYyxsDEFtjGRr0g70dJllcXD/zd4sFHXLd8/HXBxmeA1PrJsIiQnA9ubxPHtEeU3XqCP/1N2+S/zke5rR4HI+kYJCAUA4onDKW+At7EB+/4+5ey4g2fPXOIHW8ahUTrWNh2qeDxizmhfBFMFFKpjOjVg44Ehm0jY1lI0aQRY5xSlYAE6VSCf7qgkI4oWYUFxTaRcfvUlF4omgJREGCk07YkgumqjFQyZhq168NA7RtA4Vyc8sXXR73Wv/GD9gPQbmPd+3/tXWuMXOdZfs53vnPOzOxl9mY768R3N27tlkRZB9qkgQJtaEUToUq7UFCRQGLViCIubUKFipKIixT1R/jBHxtEIUGAdoUaSCWUyMUkOGmKvE5qp27txq7r28brXe9trueKnvc7Z2Z2vTdfEhLhkUZ7mTMzZ775nvPen2fRDl8VoIsyAqtbhVsAvd4t6HzyVFI4sGt4D8Lnfg4o/zzQfY/Vd3tnV886K4JSpQqb6AsQCgYSCrFe6qZKI3Uf9TARfQ7FjG8UkQYdrmaVRyFk7TTnoVYrI/Rr0I4ShocoCmO/XkVcmQtQK3GCu4TYvwSUTgHT3wM6jusH/vTKV772aOl745gp7kT1g/ejlgOip48ivPTNsQQj3wrx8c9yWBjAdrLHraz7sdYVanQEsa1uTGNsr+mvwz5gYAC7n4H1+7uhT70Ce+oinI8OwnnjEIo/PPjShrkzRzePnzi+6fzxo5sxfX4noHYh19EGnXOsfJunHU8liW1xblcaY2kuEzYBaaNcx+4e7nbbhm+zdkl+rlAASnJ+ApRTJnHMkkrMhF3siqxARMWxOImDpHT2rTrCs9OA/10A/1383L5D635t+NyHBlF+3joeAnuSwSSJTWdQiswlIualG0CW8n9XtqDvpOV8n7i4C3beMlf8lUMEMoyTnmNo/cN7cfn5zwD5TyG3pT+3vr8tsbSylIcoUVCByfaG7OcUxYIIrpUg1IxFFaKQSmvsCGN5RgtI/csTsHu60VXsRBjWUSnPQ4V1BNx0bAVVjK3iOE7CJCQz99TELDBHAqWA+habH/iNw+Uwuljs6Z3o2rB+umt972zhji0z93/hF+Z2dML/x1cmw2R7nz/Qj/BVIGCn8dgZxNiaqRSNpQC7ag1omVOmrNRGD44AI4MYBvTYfjhjB75jHxz5mKqWYOkanPnLcE68gfzJwwe6Lp492ucnYe+xEz/cOH3l0vb44skdQGkLgHZYHbZatyHf1t7hzJeqlsyj2FpZXDdmV5no0Q5yDgmBA+GPYgaduKXQkjDuh8zWBpIDEK8liVFnkTPi8bJuMbVELRWqMKghjoMoLM9PYfbsSeDyq8COl/+8+tbxkRwmjw4dDzAqPE/ytaUq7Mbyt7AfNFZoQSo6fVr6Y1FO1/gTS0Sh7wYw328AbXVUru5WWb2rx8KDzxTwk6PrceKFu4CpX4aV+0zblg/0hbG2w4hXcEfFiYLDNjLO2zvce3U4COFbtKFKRptcmei0ENIqUCiPxTlOTjCGcm3Yrhb5RJkrZzZEGoAbG5CmJbG1ZdkqTvyzpwBtVZBzZ+HYU/D0DJSegqsngWh23bq+8TgMZgpdvbOFnnVz3es3lns2bK/suuuj1bvv3eEz6i0A6CtQPBhJXxuwpU92aUyxw1lAczznQgXW5Jvwzp34Ue7E2KHO86d/UJy5NNV26vvHim2dhW4riYpx4PfEYa23Vq32wa/0AboL+Z4inHxemNpsZTmOqwg+0yhpocBMeBCJ+xrJWkQAJSSVEkVtjv1x3cI4RsK1sF3oXF5CC8nWxhymZmcXQRpJ9w8lV1kMcxxYUVJHfW7Kx9zUODD9X8h/8D83/tmzR/q/vHV8jEm4vZmVHMAItsdkdc+oVIQ4cJlW5+YGunaAvpvgvCr6Waun9H9w3OJYeaVQYqWwwcLBxMPv/NEOvPXSfUJS1r1lh5PvKCrFhIW2giBRzOB6OQ/VYB5JSD4bBZsdR5ZGEFAqIIFla3GH62R5iCPYHi1IIrOnVMWCcuDZhjtJBKEIYGFuZIbYsE6yYT+OOZ9Yo3wWn0ywc6KYWaoQUUiOjjKUnoWjK0isKhKbtAA1BGE119lX9v16EEdhKJYHmicvFt+ytVUpl/NwNYW7lONqL/RjFwiKSa1eRJx02rZdiKLYRRCQvtJmn5ylteVoV0cc0FN5pb02uJ4H22apI0YYRWJVuMhBLZVLYt3Yy0mCjYwWvDnaQRxEpneWqxCQwd2SdeP6sb1SSpOa66hFI85KQpa5iFQ79itUErsIzBwCth7sf+zY/+x5qv3iAetMHdhGQttsCLzBcmDU0ZpKOculYBu4XdAptJT9vNqC3gLou4H+AThb/zZpO/N7X9qFQ9+8F5j+NNA9gOLtxWJPvxNFUKWZWZDw2NLcuwkiWgcmkxxb/k6CAJbnIGHDNq0ot6Hnwcuzw8VHXKrBU20IEw6VW1C2uLtIElqbQH66Hl/bxF0R/WepyBhBWpuJkflSKP4h60KkMwzDGCFVOUltyDf1tHRbNG6t1yYilvG0+NkEAoNBQDuWCZZBwuHMJVVa24rjVvQKoiiyoiiCSHulpXzLVvI4gcpkGX+vlMvGYvIYx4HNTJnJaoIUNXFCIaLU7eWMJmvMpKZxHSlbceyvUp6Fm/jUK4LPppDZmQrC+XFg/DXg9oPAhddv+62Xxx/+iwfK+zc+GQJCvLYwQyuiRYMYHBxp6KQsJxSxpqRPup7m2OYz3m1wvp8s6GqwXSk+Xf6xgWEH+/YV8NW/3owj//ogrpx8kFKYcNe392za5lSDyI5iJYFnRHClK6bI6Mip/dkZWB0dcKnM7Puoy5yiUd62mP2tAzoxm1l4k9kMI68j2hBGuZAAZSJELvyJ4CmgFeS8o5uThjShoaeb2NBV599JHKTWSg5ftJnoKFq2J/3HpqeUwLegLFtARitbr1Mhg9acLj7v5liCkxcNM3tC9zML0tLqS3ps9n++nvQzh6FktxlfCm+UpjtLkBoMu54LV5PTuIZ6eRYe2S7sEGFQjTA9UUV06QpQpsX89ocf+4fvnv7238w89tDO2ccffzxomdWMk1R9vOV/amRkBIODUgZqzHXeYCvtVVmjWwBdDYbv1ON073rQjunHtgHfuQ+YexB596dRnQRyvUV0tDuul7M4LIx6XbHYrgod0ByWUrbEX2JhJc/ENjT6fyEspanTnFoVc/IEqcyoUiuWcVcSw2WSRJ5kODYYlwUEtcRw6XVcPDoDsmxjNjdo+tyW9WGVRLUANCW9MiClCyzJmtAAP5NPTDOfPNbmbKXrSlLHsLGzHmxaa8gry9KHXDhoyDMBY6ljcjw3J51bs7NzYlX5GXmc1rZYzsivo1qeg9ZWHJanE8y/fQaYOAhsfwkfe/II/v3zF9CHGvaOYXD7U/HoqJDSZq5sZj2brq1YT7m3UGK2ND9c976RmdW1GN3rfofVnvheroO2NsovF4OulDxa7bMvepymBhp/hTb84de3Aq/eCbx0D1C4H/C3obChu3vjHY5281a15qtqLYClXEOkLckgLQJP2s1JY74ThvDjQOhAM7fPWFJzJxDDKBD4NQFqECwApWVjVtNoGjZmiVtBSoA1XbDF+0hJbkfyL3TJs4bwFoDT6yVAGVu2niN/jxioOXnR/mwM7qT1SUM0QUYh89PhTKZjgEirbPk+fCtGoeBCa56HBb9eQ3lmOgYHqtkOWalMIjl3Cth0FB/+pVd6f/aRY1N3feAyXJTgIwIT1CmfU/pFqfQzELBqaMjwpaRWU34ODQ3RurZ8rze6vW8BdDkQrWVlU8ep8RKLdujyI1QrI1dSCPoLCVzndRT/7p5P7wZeYMHwPqBnD9DRa/X2F3p6N1iliq9crx3aLSCKFcrlOiI2e9sOXOnlbQ67NCyfWDDbJFQY58kEXQKpvopKtSWjcIQuM8iWCPxkVjRlIUgtsXDwLJMJYQxGtzW9Piw5OuU4jrizvGcWlvGy8RM1gigrnaQXDsUMdyrThwSFQl4GpOMokMw17+bzkFomRL02E4dBhWUpvnwpunJlGsHEZaB2Gigdxc6vHsHGXzw38OgnL499FhUMIcRpJBhj/Xc/RpJE5BlTTyEljjZrQYBm4Ez/lqvMzQRoyhJ6jRf6m3v4WoBwc99xba92Ped1g67IkoC2sDtxuh5FYeZrT67Hhb/cDagHgd4HkCtuEjVhO0cmbUu7BVNTjU1BPmBupkVpgoCUK4oMU5h+0yw2bI6VcSTOIDUgEKI6lGR60/gzjScz15YAW+lG7tfsttRsI2NH1nszgBKcYt3FgtoIhFdWpy63YTzJaGMIQs9zUauWEfkclmZmmxlkcaFjbUdJFMxU/elLCWqlEjDxBpCwweBN4OFTTyT/dvEJYE6uMEOwkPJcpqXHeBCDGLUa8n5XkUa3gHEBl9BCkF7PNmqu6C2ALr+71rKyq1jQtV0J1niUhYFEP3QYzqHfHO+bfvaRLcCl7cBr9wJ9dwFt26C7uq3uPtfLFahciiCMY7EkWgK+liQMPcM0PsrAKoBltpPurjkjWioqikt9MHNRU8BlADVJomypFi6ZVCpZDpKw1iSpzK3p7mYuLkEql4XU/ZXYkudI2krGsgLaBIqSgKS2ZNcG08BhkGhbxe3teVvbKqlUy2Flbi5BtRKiNjkDnHkTwFEg9yZ2fenYlw9+/eJEP6rP7kGA440268ZXcDgZwADGMDYG7KXPYlzYmAmg1htj0hbreQuga9zE2WHX0/Gz+C3WClDZV42dd40n2nL4Wl+DyNIDgHPq7+HO/MuRPrzw5CbgW3cC8Z2AdRfQtRXoLCLX5aC921Wu5zApY2st4STvbIcjENkAYYCZNf2wEcJcd+qp0E9GLChJGh6fPi/LuC4LUL5MFEr8ZwBtrF8Dpkks2VxWNAlQA07zqLHY6fRklphi+x3H8cIgJkl8EoeJ57lhvVIOUJonrXsAjL8NVE8BeAvACeBn3to5/NvjT+8bnn1iDJWxvZyZHU6A/WmnU+N0FlhINh8elgvVPgwNHciSRFd9u1lMml1cmp8tc6bWso2W3zTvdwu6OEGz1Gq0WrnFLuiNrd6qYJS3XsrtNcJ2afC2eEB67QPRklSSwWQcQB7PPF/Es3/cB/xgB4CPAPgQsGkXVMcGJDFpBx3V0eloNgMI4bYt5GYeibdlHM64vqLjzhjVVggC1mls6WXNrKaptBiLLJ1MjF8Tyum1Logpuue0LUPoBnBp7CjZV/O3dqR3WFxcmS6R+iaF40zWlQLUKj2WpaEoCuIoCqPY92uo1aoIKhGSCfYZHwNwDLj9BLZ85SL2fHEKJ2cqwz+6rbx/DIExh2NZhtX8NF9CA5gDAOUdMEx0prf9A8AYmxMBTE9Py/Na405a0tHRhX3KNzMGNRe1928Wd7HVuRaAvsPgbOyANcWlS4F0Aan4ChfZRpYmIzwa3WPjseMFdH+uiNePbgV+vBmIqCT+EWDLTnT29MPWnfB9aioKjWQuV1C27YhDGoWxxUHyhKDJe/QvxcJKS1zqKqfchCaGJZWLfMqmq8vjWEdVlENpEfqkAaa7yjqi1kpVKmUBpcNOHkXAm3hUssORDxVWY1GNC4MkrtdDVEqzwMQpoE5A0lKOA7mf4FMnT+PFTbO4HQkuEoyHjUgvGYRGLYWnoCQr29JgkDHtUwiX1UupYA6ngN1vxsSYpzWqfwuztdnXcQugZiVuNpgWx46r2sJlDljldZa1oEu+XAbSpSzr2q1q+tKS9TmsgQELL8LBi8jj7OUuvPZ0P849cxtwgUPMfYC9A+jZAtibgGob23wAL0C+z1HFLlJgmbZ7gt/WynY9aUPMXGXDgpbV+/g1pXVSgaf0tyaMG9nUEAd+HDFmjUJ4OccLWf5QKmajRBiFCarVGHRXabaplRBVS8DkeaB0BsB5AG8D7ZeA7Rdw359c/vw3fnX2SA/qJ/pkbqWFiCvbMemqjUI1UJYCcXTUTJ3J/3kXdC76nX+O8uHswSZIWU5ZbtP8f7SgNwLQpUB0rQBdKqBYrti1+Ng1WdDlv+yrL06yGKsz+i30LgikIdmQNjZC4z9g/cpPwXmu+5+KKL/ai+DlDcC5fqDWD1i8bwDa18PK98Bx24HYE/oBk1QiQY+SEZxCuzkTCRgJ5PR6KnFtDNSrMYRh3U6IQukR5sG28CcF8KuckmbrzyTgUx/hEhCfA4qTwIVLuPsPxjGlruDjj5Q+Mbyt7n4CwYtUK98t72REbYca22Np0CQy6k0ryccbLi3B14RehtwMqCPA6FA8OjioYLqD5LYSMLNjbiZA34sx6FIJnhsB6PVax8Z6L3qB5ZI5y533akBe+fxaafUIhSYFyNWvu3SEvdwFwnyOZlGdTXX8n8av/7gD//y77cBEERjrAsCBlc70J+sq5HouCLMWSgWg3TOZJjetuaTFSI6SgPIVIVWEKkBQEWZnOD7gVoD5EhBx2KUC9M/De2gWD++bxgiFNJeyiK1L1dgSCsMDamTf4XgQo7CspmVrEYtqlTgRgIrb2grQoYz5oLFcRtslzUBfyya6WQB9L4Bzsfu6GIjXaumuZR2v5djrt6BLpogMNNZ0AksBtLlKzddoCs2udq7NtxUu3Cxf1XI2ZGZOWQwG0m7Y84DVdhqaguz5Sdhn35xX89+/ZKHG7oAORYhhvmphkspvdSWYno5inHtOA20h2jZH6N8U447bEtzdHz78RUS37UI0DuB5AuE44oZVNIVac0s1PReuVXNbEIRDFtRoUzBqoWbmgo9l/uDy0aKKBR0EBkcBSwBqlo4xafY0qa4Mtipar24rbhZA5VxX95TWtI1u5KD/BTTgpOLruHXKAAAAAElFTkSuQmCC';let imageData = Data.fromBase64String(qwe);let dimg=Image.fromData(imageData);const butimg =await this.getImageByUrl('https://gitlab.com/j8468/ggfv/-/raw/main/img/but1.PNG');const headimg =await this.getImageByUrl('https://gitlab.com/j8468/ggfv/-/raw/main/img/vw_logo.png');this.checkUpdate('fvw-version',false);const aa=new UITable();aa.dismissOnSelect = true;let topRow = new UITableRow();topRow.height = 60;let leftText = topRow.addButton('版本:'+this.version);leftText.widthWeight = 0.3;leftText.onTap = async () => {await Safari.open('');};let centerRow = topRow.addImage(dimg);centerRow.widthWeight = 0.4;centerRow.centerAligned();let rightText = topRow.addButton('使用教程');rightText.widthWeight = 0.3;rightText.rightAligned();rightText.onTap = async () => {await Safari.openInApp('https://docs.qq.com/doc/DQkRxdG91Rnd1UlFM',false);};aa.addRow(topRow);const header = new UITableRow();const heading = header.addText('一汽大众小组件');heading.titleFont = Font.mediumSystemFont(17);heading.centerAligned();aa.addRow(header);if (this.settings['isLogin']){var menuList = [{name: 'actionAccountLogin',text: '账户登陆',icon: 'square.and.pencil',color:'#FD2953'},{name: 'actionPreferenceSettings',text: '个性设置',icon: 'tag',color:'#30c758'},{name: 'actionUIRenderSettings',text: '界面设置',icon: 'wand.and.stars',color:'#6a6358'},{name: 'actionRefreshData',text: '刷新数据',icon: 'gobackward',color:'#66cd00'},{name: 'actionOperations',text: '操作车辆',icon: 'car',color:'#2e8b57'},{name: 'actionTriggerPreview',text: '预览组件',icon: 'eyes',color:'#ff4800'},{name: 'setpic',text: '车辆图片',icon: 'car',color:'#4cc9f0'},{name: 'setqd',text: '签到KEY',icon: 'key',color:'#6c00ff'},{name: 'actionLogOut',text: '退出登录',icon: 'gobackward.minus',color:'#4cc9f0'},{name: 'checkUpdate(\'fvw-version\')',text: '检查更新',icon: 'arrow.up.square',color:'#27e1c1'}];}else{var menuList = [{name: 'actionAccountLogin',text: '账户登陆',icon: 'square.and.pencil',color:'#FD2953'},{name: 'checkUpdate(\'fvw-version\')',text: '检查更新',icon: 'arrow.up.square',color:'#27e1c1'}];}for(let item of menuList){let row2 = new UITableRow();row2.dismissOnSelect = false;let img2 = row2.addImage(await this.htt(item.icon,item.color));img2.widthWeight = 100;let rowtext2 = row2.addText(item.text);rowtext2.widthWeight = 400;switch(item.text){case'账户登陆':var tt = this.settings['username']?this.settings['username']:'未登录';if (tt!='未登录')tt = tt.substring(0,8) + "***";break;case'检查更新':var tt = this.version;break;default:var tt = '>';};let valText2 = row2.addText(tt);valText2.titleColor =Color.blue();valText2.widthWeight = 500;valText2.rightAligned();row2.onSelect = async () => {eval('this.'+item.name+'()')};aa.addRow(row2);}let butrow = new UITableRow();let centerRow1 = butrow.addImage(butimg);butrow.height = 160;butrow.onSelect = async () => {await Safari.open('weixin://scanqrcode');};centerRow1.centerAligned();aa.addRow(butrow);aa.present();}async handleLoginRequest(debug = false) {const options = {url: 'https://one-app-h5.faw-vw.com/prod-api/mobile/one-app/account/login?appkey=9709918063',method: 'POST',headers: this.requestHeader(),body: JSON.stringify({password: this.settings['password'].trim(),account: this.settings['username'].trim(),scope: 'openid profile mbb'})};try {const response = await this.http(options);if (debug) {console.log('登录接口返回数据：');console.log(response);}if(this.settings['username'].trim()== '44444'){await this.notify('用户正确', this.settings['username'].trim());}else{if (response.returnStatus === 'SUCCEED') {await this.notify('登录成功', '正在从服务器获取车辆数据，请耐心等待！');const tokenInfo = response.data;this.settings['userIDToken'] = tokenInfo.idToken;this.settings['aid'] = response.data.userInfo.aid;await this.saveSettings(false);console.log('账户登录成功，存储用户 idToken 密钥信息，准备交换验证密钥数据和获取个人基础信息');const ee=JSON.stringify(tokenInfo);console.log(ee);await this.getTokenRequest(debug);  Safari.open('scriptable:///run?scriptName=' + encodeURIComponent(Script.name()));} else {console.error('账户登录失败：' + response.description);await this.notify('账户登录失败', '账户登录失败：' + response.description);}}} catch (error) {console.error(error);}}async getTokenRequest(debug = false) {const requestParams = `grant_type=${encodeURIComponent('id_token')}&token=${encodeURIComponent(this.settings['userIDToken'])}&scope=${encodeURIComponent('t2_fawvw:fal')}`;const options = {url: 'https://mbboauth-1d.prd.cn.vwg-connect.cn/mbbcoauth/mobile/oauth2/v1/token',method: 'POST',headers: {'X-Client-Id': this.settings['clientID']},body: requestParams};try {const response = await this.http(options);if (debug) {console.log('密钥接口返回数据：');console.log(response);console.warn('请注意不要公开此密钥信息，否则会有被丢车、被盗窃等的风险！');}if (response.error) {switch (response.error) {case 'invalid_grant':if (/expired/g.test(response.error_description)) {console.warn('IDToken 数据过期，正在重新获取数据中，请耐心等待...');await this.getTokenRequest(debug);} else {console.error('Token 授权无效，请联系开发者：');console.error(`${response.error_description} - ${response.error_description}`);}break;case 'invalid_request':console.warn('无效 Token，正在重新登录中，请耐心等待...');await this.handleLoginRequest(debug);break;default:console.error('交换 Token 请求失败：' + response.error + ' - ' + response.error_description);}} else {this.settings['authToken'] = response.access_token;await this.saveSettings(false);console.log('authToken 密钥数据获取成功并且存储到本地');console.log(response);await this.getVehiclesVIN(debug);}} catch (error) {console.error(error);}}async getVehiclesVIN(debug = false) {const options = {url: 'https://mal-1a.prd.cn.vwg-connect.cn/api/usermanagement/users/v1/vehicles',method: 'GET',headers: {'Accept': 'application/json','Authorization': 'Bearer ' + this.settings['authToken']}};try {const response = await this.http(options);if (debug) {console.log('车架号接口返回数据：');console.log(response);}if (response?.userVehicles?.vehicle) {this.settings['carVIN'] = response.userVehicles.vehicle[0];this.settings['seriesName'] = '一汽大众';this.settings['carModelName'] = '这里可以自行设置功率排量等数据';await this.saveSettings(false);await this.getApiBaseURI(debug);} else {console.log('获取车架号失败');}} catch (error) {console.error(error);}}async actionAccountLogin() {const message = `温馨提示：由于一汽大众应用支持单点登录，即不支持多终端应用登录，建议在一汽大众应用「爱车 - 用户授权」进行添加用户，这样 VW组件和应用独立执行。`;const present = await this.actionStatementSettings(message);if (present !== -1) {const alert = new Alert(); alert.title = 'VW 登录';alert.message = '使用一汽大众账号登录进行展示数据';alert.addTextField('一汽大众账号', this.settings['username']);alert.addSecureTextField('一汽大众密码', this.settings['password']);alert.addAction('确定');alert.addCancelAction('取消');const id = await alert.presentAlert();if (id === -1) return;this.settings['username'] = alert.textFieldValue(0);this.settings['password'] = alert.textFieldValue(1);console.log('您已经同意协议，并且已经储存账户信息，开始进行获取设备编码');await this.saveSettings(false);await this.getDeviceId();}}async actionCheckUpdate() {await this.checkUpdate('fvw-version');}async actionTriggerPreview() {await this.actionPreview(Widget);}async showPlate() {const alert = new Alert();alert.title = '设置车牌';alert.message = '请设置您的车辆牌照信息，不填牌照默认关闭牌照展示';alert.addTextField('车牌信息', this.settings['carPlateNo']);alert.addAction('确定');alert.addCancelAction('取消');const id = await alert.presentAlert();if (id === -1) return ;this.settings['carPlateNo'] = alert.textFieldValue(0);await this.saveSettings();}requestHeader() {return {Accept: 'application/json',OS: 'iOS','Content-Type': 'application/json','User-Agent': 'NewAfterMarket-ios/3.17.1 CFNetwork/1329 Darwin/21.3.0','Did': `VW_APP_iPhone_${this.settings['clientID'].replace(/-/g, '')}_15.1_2.7.0`,'X-Client-Id': this.settings['clientID'],'deviceId': this.settings['clientID']}}}await Running(Widget)


